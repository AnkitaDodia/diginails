package com.bopo.dialogs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bopo.R;

/**
 * Created by Nick Yaro on 13/4/2016.
 */
public class ThreeImageChoiceDialogFragment extends DialogFragment {
    int mTitleResId, mMessageResId, mImageOneResId, mImageTwoResId, mImageThreeResId, mImageOneTitle, mImageTwoTitle, mImageThreeTitle;
    View.OnClickListener mImageOneOnClickListener, mImageTwoOnClickListener, mImageThreeOnClickListener;

    public static ThreeImageChoiceDialogFragment newInstance(int titleResId, int messageResId,
                                                             int imageOneTitle, int imageOneResId, View.OnClickListener imageOneOnClickListener,
                                                             int imageTwoTitle, int imageTwoResId, View.OnClickListener imageTwoOnClicklistener,
                                                             int imageThreeTitle, int imageThreeResId, View.OnClickListener imageThreeOnClickListener) {
        ThreeImageChoiceDialogFragment twoImageChoiceDialogFragment = new ThreeImageChoiceDialogFragment();

        twoImageChoiceDialogFragment.mTitleResId = titleResId;
        twoImageChoiceDialogFragment.mMessageResId = messageResId;
        twoImageChoiceDialogFragment.mImageOneResId = imageOneResId;
        twoImageChoiceDialogFragment.mImageTwoResId = imageTwoResId;
        twoImageChoiceDialogFragment.mImageThreeResId = imageThreeResId;
        twoImageChoiceDialogFragment.mImageOneTitle = imageOneTitle;
        twoImageChoiceDialogFragment.mImageTwoTitle = imageTwoTitle;
        twoImageChoiceDialogFragment.mImageThreeTitle = imageThreeTitle;
        twoImageChoiceDialogFragment.mImageOneOnClickListener = imageOneOnClickListener;
        twoImageChoiceDialogFragment.mImageTwoOnClickListener = imageTwoOnClicklistener;
        twoImageChoiceDialogFragment.mImageThreeOnClickListener = imageThreeOnClickListener;

        return twoImageChoiceDialogFragment;
    }

    public ThreeImageChoiceDialogFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(mTitleResId);

        View rootView = inflater.inflate(R.layout.dialog_three_image_choice, container, false);

        ImageView imageOne = (ImageView)rootView.findViewById(R.id.dialog_two_image_choice_image1);
        ImageView imageTwo = (ImageView)rootView.findViewById(R.id.dialog_two_image_choice_image2);
        ImageView imageThree = (ImageView)rootView.findViewById(R.id.dialog_two_image_choice_image3);

        if (mImageOneResId > 0) imageOne.setImageResource(mImageOneResId);
        if (mImageTwoResId > 0) imageTwo.setImageResource(mImageTwoResId);
        if (mImageThreeResId > 0) imageThree.setImageResource(mImageThreeResId);

        ((TextView)rootView.findViewById(R.id.dialog_two_image_choice_message)).setText(Html.fromHtml(getString(mMessageResId)));
        ((TextView)rootView.findViewById(R.id.dialog_two_image_choice_image1_text)).setText(Html.fromHtml(getString(mImageOneTitle)));
        ((TextView)rootView.findViewById(R.id.dialog_two_image_choice_image2_text)).setText(Html.fromHtml(getString(mImageTwoTitle)));
        ((TextView)rootView.findViewById(R.id.dialog_two_image_choice_image3_text)).setText(Html.fromHtml(getString(mImageThreeTitle)));

        View.OnClickListener optionOne = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImageOneOnClickListener.onClick(v);
                dismiss();
            }
        };
        View.OnClickListener optionTwo = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImageTwoOnClickListener.onClick(v);
                dismiss();
            }
        };
        View.OnClickListener optionThree = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImageThreeOnClickListener.onClick(v);
                dismiss();
            }
        };

        rootView.findViewById(R.id.dialog_two_image_choice_image1).setOnClickListener(optionOne);
        rootView.findViewById(R.id.dialog_two_image_choice_image1_text).setOnClickListener(optionOne);

        rootView.findViewById(R.id.dialog_two_image_choice_image2).setOnClickListener(optionTwo);
        rootView.findViewById(R.id.dialog_two_image_choice_image2_text).setOnClickListener(optionTwo);

        rootView.findViewById(R.id.dialog_two_image_choice_image3).setOnClickListener(optionThree);
        rootView.findViewById(R.id.dialog_two_image_choice_image3_text).setOnClickListener(optionThree);
        
        return rootView;
    }
}
