package com.bopo.dialogs;

import android.app.ProgressDialog;
import android.support.v4.app.DialogFragment;

import com.bopo.R;

/**
 * Created by Nick Yaro on 1/3/2017.
 */

public abstract class BaseDialogFragment extends DialogFragment {
    ProgressDialog mProgressDialog;

    public void setProgressVisibility(boolean state) {
        setProgressVisibility(state, 0, R.string.please_wait);
    }

    public void setProgressVisibility(boolean state, int titleId, int msgStrId) {
        if (state) {
            if (mProgressDialog == null || !mProgressDialog.isShowing()) {

                String titleStr = "";
                if (titleId > 0) {
                    titleStr = getString(titleId);
                }

                String msgStr = getString(R.string.please_wait);
                if (msgStrId > 0) {
                    msgStr = getString(msgStrId);
                }

                mProgressDialog = ProgressDialog.show(this.getActivity(), titleStr, msgStr, true, false);
            }
        }
        else {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }
    }

    public void showPopup(final String message) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setProgressVisibility(false);
                final GeneralDialog generalDialog = GeneralDialog.newInstance(message);
                generalDialog.show(getActivity().getSupportFragmentManager(), GeneralDialog.class.getName());
            }
        });
    }
}
