package com.bopo.dialogs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.bopo.activities.BaseActivity;
import com.bopo.fragments.FragmentPrintReady;
import com.bopo.R;
import com.bopo.utils.Analytics;
import com.bopo.views.AnimationOnTouchListener;
import com.bopo.utils.GenericDialogHelper;

public class PrintOptionsDialog extends DialogFragment {
	private View rootView;
	TextView txtNotYet, txtYes, txtPrintFromComputer;

	public static PrintOptionsDialog newInstance() {
		PrintOptionsDialog dialog = new PrintOptionsDialog();
		return dialog;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		rootView = inflater.inflate(R.layout.dialog_print, container, false);

		txtYes = (TextView) rootView.findViewById(R.id.txtYes);
		txtPrintFromComputer = (TextView) rootView.findViewById(R.id.dialog_print_computer);

		txtYes.setOnTouchListener(new AnimationOnTouchListener(1.25f));
		txtPrintFromComputer.setOnTouchListener(new AnimationOnTouchListener(1.25f));

		txtYes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();

				((BaseActivity)getActivity()).loadFragment(FragmentPrintReady.newInstance());

				Analytics.log(Analytics.ACTION_PRINT_DEVICE);
			}
		});

		txtPrintFromComputer.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getDialog().dismiss();
				GenericDialogHelper.showPrintFromWeb(getActivity());
				Analytics.log(Analytics.ACTION_PRINT_WEB);
			}
		});

		return rootView;
	}



}
