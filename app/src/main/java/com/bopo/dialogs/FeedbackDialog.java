package com.bopo.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.bopo.R;

/**
 * Created by admin on 2/20/2016.
 */
public class FeedbackDialog {
    Context mContext;
    View mView;
    AlertDialog mDialog;

    float mRating = 0;
    String mComment;
    boolean mSubmitted;

    public FeedbackDialog(Context context) {
        mContext = context;
    }

    public AlertDialog show() {
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_feedback, null);

        ((RatingBar)mView.findViewById(R.id.feedbackRatingBar)).setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mRating = rating;
                mView.findViewById(R.id.feedback_section1).setVisibility(View.GONE);
                mView.findViewById(R.id.feedback_section2).setVisibility(View.VISIBLE);

                if (mRating < 4) {
                    ((EditText) mView.findViewById(R.id.feedbackEditText)).setHint(R.string.feedback_input_how_can_we_improve);
                }

                mView.findViewById(R.id.feedback_submit).setOnClickListener(new OnOkClickListener());
            }
        });

        mDialog = new AlertDialog.Builder(mContext)
                .setView(mView)
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (mSubmitted) {
                            Toast.makeText(mContext, mContext.getString(R.string.feedback_thanks), Toast.LENGTH_LONG).show();
                        }
                    }
                })
            .show();

        return mDialog;
    }

    private void submitData() {
        //TODO - async
    }

    class OnOkClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            mComment = ((EditText)mView.findViewById(R.id.feedbackEditText)).getText().toString();

            submitData();
            mSubmitted = true;
            mDialog.dismiss();
        }
    };
}
