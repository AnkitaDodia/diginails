package com.bopo.dialogs;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.view.View;

import com.bopo.R;
import com.bopo.fragments.FragmentPrintReady;
import com.bopo.print.PrintUtil;
import com.bopo.utils.Analytics;
import com.bopo.utils.Consts;

/**
 * Created by Nick Yaro on 27/1/2017.
 */

public class PaperSizeOptionsDialogFragment extends ThreeImageChoiceDialogFragment {

    public static ThreeImageChoiceDialogFragment newInstance(Context c, final Fragment targetFragment) {
        final SharedPreferences.Editor editor = c.getSharedPreferences(Consts.PREFS_KEY, Context.MODE_PRIVATE).edit();

        return ThreeImageChoiceDialogFragment.newInstance(
                R.string.select_paper_size,
                R.string.unsupported_paper_size_message,
                R.string.paper_size_a5,
                R.drawable.print_a5,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editor.putInt(Consts.PARAM_PRINT_MODE, PrintUtil.PAGE_FORMAT_A5_GRID).apply();
                        editor.putBoolean(Consts.PARAM_PRINT_HELP_DIALOG_SHOWN, false).apply();
                        editor.commit();

                        if (targetFragment != null && targetFragment instanceof FragmentPrintReady) {
                            ((FragmentPrintReady)targetFragment).onPrintParamsChanged();
                        }
                        Analytics.log(Analytics.ACTION_PAPER_SIZE_SAVE, Analytics.PARAM_PAPER_SIZE, "A5");
                    }
                },
                R.string.paper_size_a4,
                R.drawable.print_a4,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editor.putInt(Consts.PARAM_PRINT_MODE, PrintUtil.PAGE_FORMAT_A4_GRID).apply();
                        editor.putBoolean(Consts.PARAM_PRINT_HELP_DIALOG_SHOWN, false).apply();
                        editor.commit();

                        if (targetFragment != null && targetFragment instanceof FragmentPrintReady) {
                            ((FragmentPrintReady)targetFragment).onPrintParamsChanged();
                        }
                        Analytics.log(Analytics.ACTION_PAPER_SIZE_SAVE, Analytics.PARAM_PAPER_SIZE, "A4");
                    }
                },
                R.string.paper_size_a4_old,
                R.drawable.print_a4,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editor.putInt(Consts.PARAM_PRINT_MODE, PrintUtil.PAGE_FORMAT_A4_8_ROW).apply();
                        editor.putBoolean(Consts.PARAM_PRINT_HELP_DIALOG_SHOWN, false).apply();
                        editor.commit();

                        if (targetFragment != null && targetFragment instanceof FragmentPrintReady) {
                            ((FragmentPrintReady)targetFragment).onPrintParamsChanged();
                        }
                        Analytics.log(Analytics.ACTION_PAPER_SIZE_SAVE, Analytics.PARAM_PAPER_SIZE, "A4 - Old format");
                    }

                }
        );
    }
}
