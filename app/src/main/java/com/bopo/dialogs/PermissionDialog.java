package com.bopo.dialogs;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.bopo.App;
import com.bopo.activities.BaseActivity;
import com.bopo.R;

public class PermissionDialog extends DialogFragment {
	Context mContext;
	private View rootView;
	public BaseActivity instance;
	TextView txtNot, txtAgree;
	App app;
	String url;
	PermissionDialogListener onClickListener;

	public static PermissionDialog newInstance(PermissionDialogListener onClickListener) {
		PermissionDialog dialog = new PermissionDialog();
		dialog.onClickListener = onClickListener;
		return dialog;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		instance = (BaseActivity) getActivity();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		getDialog().getWindow().setBackgroundDrawable(
				new ColorDrawable(Color.TRANSPARENT));
		getDialog().setCanceledOnTouchOutside(false);

		rootView = inflater.inflate(R.layout.dialog_permission, container,
				false);
		txtNot = (TextView) rootView.findViewById(R.id.txtNot);
		txtAgree = (TextView) rootView.findViewById(R.id.txtAgree);

		txtAgree.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				onClickListener.onAgree();
			}
		});
		txtNot.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});

		return rootView;
	}

}
