package com.bopo.dialogs;

import android.support.v4.app.DialogFragment;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bopo.R;
import com.bopo.activities.BaseActivity;
import com.bopo.api.RequestApi;
import com.bopo.bean.GalleryDesignItem;
import com.bopo.fragments.FragmentDesigner;
import com.bopo.fragments.FragmentGalleryDesigns;
import com.bopo.fragments.FragmentUserDesigns;
import com.bopo.utils.Consts;
import com.bopo.utils.managers.UserManager;
import com.bopo.views.AnimationOnTouchListener;
import com.bopo.utils.managers.DesignBitmapManager;
import com.bopo.utils.managers.DrawingManager;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.techery.properratingbar.ProperRatingBar;
import io.techery.properratingbar.RatingListener;
import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by Nick Yaro on 15/3/2016.
 */
public class DialogGalleryItem extends DialogFragment {
    static private final int mResId = R.layout.dialog_gallery_item;
    static private List<Integer> mRatedDesigns = new ArrayList<>();

    public GalleryDesignItem galleryDesignItem;
    public ImageView itemImage, userImage;
    public LinearLayout userInfoContainer;
    public TextView downloadCount, username, editBtn, printBtn;
    public ProperRatingBar ratingBar;
    public ProgressBar progressBar;

    public int selectedDesignId;
    private boolean printImmedeately;

    public static DialogGalleryItem newInstance(GalleryDesignItem galleryDesignItem) {
        DialogGalleryItem dialogGalleryItem = new DialogGalleryItem();
        dialogGalleryItem.galleryDesignItem = galleryDesignItem;
        return dialogGalleryItem;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View rootView = inflater.inflate(mResId, container, false);

        itemImage = (ImageView) rootView.findViewById(R.id.gallery_item_image);
        userImage = (ImageView) rootView.findViewById(R.id.gallery_item_user_image);
        downloadCount = (TextView) rootView.findViewById(R.id.gallery_item_downloads);
        userInfoContainer = (LinearLayout)rootView.findViewById(R.id.gallery_item_user_container);
        username = (TextView) rootView.findViewById(R.id.gallery_item_username);
        ratingBar = (ProperRatingBar) rootView.findViewById(R.id.gallery_item_rating_bar);
        progressBar = (ProgressBar) rootView.findViewById(R.id.gallery_progress_bar);
        editBtn = (TextView) rootView.findViewById(R.id.gallery_item_button_edit);
        printBtn = (TextView) rootView.findViewById(R.id.gallery_item_button_print);

        itemImage.setOnTouchListener(new AnimationOnTouchListener(0.9f));
        editBtn.setOnTouchListener(new AnimationOnTouchListener(0.9f));
        printBtn.setOnTouchListener(new AnimationOnTouchListener(0.9f));

        itemImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDesignSelected(galleryDesignItem, false);
            }
        });
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDesignSelected(galleryDesignItem, false);
            }
        });
        printBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDesignSelected(galleryDesignItem, true);
            }
        });

        ratingBar.setListener(new RatingListener() {
            @Override
            public void onRatePicked(ProperRatingBar properRatingBar) {
                if (!UserManager.getInstance(getActivity()).isSignedIn()) {
                    Toast.makeText(getActivity(), "Please login or register to submit ratings", Toast.LENGTH_LONG).show();
                    properRatingBar.setRating((int) galleryDesignItem.getDesignRating());
                } else if (mRatedDesigns.contains(galleryDesignItem.getId())) {
                    Toast.makeText(getActivity(), R.string.youve_already_rated_this_design, Toast.LENGTH_SHORT).show();
                    properRatingBar.setRating((int) galleryDesignItem.getDesignRating());
                } else {
                    mRatedDesigns.add(galleryDesignItem.getId());
                    galleryDesignItem.setDesignRating(properRatingBar.getRating());
                    String userId = UserManager.getInstance(getActivity()).getValue(UserManager.PARAM_ID);
                    RequestApi.getInstance(getActivity()).rateDesign(postRatingCallback, galleryDesignItem.getId(), properRatingBar.getRating(), userId);
                }

            }
        });

        String imagePath = Consts.ROOT_URL + "resize.php?img=" + galleryDesignItem.getPrintImgPath() + "&width=200&h=360&zc=1";

        progressBar.setVisibility(View.VISIBLE);
        Picasso.with(getActivity())
                .load(imagePath)
                .error(R.drawable.no_image)
                .into(itemImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        progressBar.setVisibility(View.GONE);
                    }
                });

        Picasso.with(getActivity()).load(Consts.ROOT_URL + "data/avatars/" + galleryDesignItem.getUserId() + ".jpg").error(R.drawable.no_image).into(userImage);
        Picasso.with(getActivity()).load(galleryDesignItem.getPrintImgPath()).fetch();

        if (galleryDesignItem.getDownloads() > 0) {
            downloadCount.setText(
                    String.format(
                            getActivity().getString(R.string.downloads_),
                            String.valueOf(galleryDesignItem.getDownloads())
                    )
            );
        } else {
            downloadCount.setVisibility(View.INVISIBLE);
        }
        username.setText(galleryDesignItem.getUsername());
        ratingBar.setRating((int) Math.round(galleryDesignItem.getDesignRating()));

        userInfoContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity)getActivity()).loadFragment(
                        FragmentUserDesigns.newInstance(
                                galleryDesignItem.getUserId(),
                                galleryDesignItem.getUsername()
                        )
                );
            }
        });

        return rootView;
    }


    final public okhttp3.Callback postRatingCallback = new okhttp3.Callback() {
        @Override
        public void onResponse(Call call, Response response) throws IOException {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getActivity(), R.string.thanks_for_rating, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onFailure(Call call, IOException e) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.error_network_generic), Toast.LENGTH_LONG).show();
        }
    };


    public void onDesignSelected(GalleryDesignItem galleryDesignItem, boolean print) {
        ((BaseActivity)getActivity()).setProgressVisibility(true);
        printImmedeately = print;
        selectedDesignId = galleryDesignItem.getId();
        Picasso.with(getActivity()).load(galleryDesignItem.getPrintImgPath()).into(imgLoadTarget);
    }

    Target imgLoadTarget = new Target() {

        @Override
        public void onPrepareLoad(Drawable arg0) {
        }

        @Override
        public void onBitmapLoaded(final Bitmap bmap, Picasso.LoadedFrom arg1) {
            ((BaseActivity)getActivity()).setProgressVisibility(false);

            getDialog().dismiss();

            DesignBitmapManager.getInstance().setNailDesignId(selectedDesignId);
            DesignBitmapManager.getInstance().setNailDesignBitmap(bmap);

            if (printImmedeately) {
                PrintOptionsDialog.newInstance().show(getFragmentManager(), "select_message");
            } else {
                DrawingManager.getInstance().history.clear();
                ((BaseActivity)getActivity()).loadFragment(FragmentDesigner.newInstance());
            }
        }

        @Override
        public void onBitmapFailed(Drawable arg0) {
            Toast.makeText(getActivity(), R.string.error_network_generic, Toast.LENGTH_LONG).show();
        }
    };
}
