package com.bopo.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bopo.App;
import com.bopo.activities.BaseActivity;
import com.bopo.fonts.CustomFontTextView;
import com.bopo.intefaces.PaintFontDialogListener;
import com.bopo.R;

import java.util.ArrayList;
import java.util.List;

public class ChooseFontDialog extends DialogFragment {
	private View rootView;

	PaintFontDialogListener onSelect;

	public static ChooseFontDialog newInstance(PaintFontDialogListener fragmentPaint) {
		ChooseFontDialog chooseFontDialog = new ChooseFontDialog();
		chooseFontDialog.onSelect = fragmentPaint;
		return chooseFontDialog;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		// getDialog().setTitle("Loading Global Category List...");

		rootView = inflater.inflate(R.layout.dialog_font_design, container,
				false);

		LinearLayout fontsLl = (LinearLayout)rootView.findViewById(R.id.dialog_font_design_fonts);

		View.OnClickListener onClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (v instanceof CustomFontTextView) {
					onSelect.onSelectFontCompleted(((CustomFontTextView)v).getFontFaceName());
					dismiss();
				}
			}
		};

		int childCount = fontsLl.getChildCount();
		for (int i=0; i < childCount; i++){
			View v = fontsLl.getChildAt(i);
			v.setOnClickListener(onClickListener);
		}

		return rootView;
	}
}
