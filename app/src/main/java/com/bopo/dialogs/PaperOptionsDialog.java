package com.bopo.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bopo.R;
import com.bopo.views.AnimationOnTouchListener;

import org.w3c.dom.Text;

/**
 * Created by Nick Yaro on 30/4/2017.
 */

public class PaperOptionsDialog extends DialogFragment {
    View rootView;
    @StringRes int title;
    @DrawableRes int resource1;
    @DrawableRes int resource2;
    View.OnClickListener onClickListener;

    public static PaperOptionsDialog newInstance(Context c,
                                                 @StringRes int title, @DrawableRes int resource1, @DrawableRes int resource2,
                                                 View.OnClickListener onClickListener) {
        PaperOptionsDialog paperOptionsDialog = new PaperOptionsDialog();
        paperOptionsDialog.title = title;
        paperOptionsDialog.resource1 = resource1;
        paperOptionsDialog.resource2 = resource2;
        paperOptionsDialog.onClickListener = onClickListener;

        return paperOptionsDialog;
    }

    public PaperOptionsDialog() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        rootView = inflater.inflate(R.layout.dialog_paper_options, container, false);

        ((TextView)rootView.findViewById(R.id.dialog_paper_option_title)).setText(title);

        ImageView option1 = (ImageView)rootView.findViewById(R.id.dialog_paper_option_1);
        ImageView option2 = (ImageView)rootView.findViewById(R.id.dialog_paper_option_2);

        option1.setImageResource(resource1);
        option2.setImageResource(resource2);

        option1.setOnTouchListener(new AnimationOnTouchListener());
        option2.setOnTouchListener(new AnimationOnTouchListener());

        option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                onClickListener.onClick(v);
            }
        });
        option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                onClickListener.onClick(v);
            }
        });

        return rootView;
    }
}


