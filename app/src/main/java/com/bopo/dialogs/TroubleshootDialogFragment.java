package com.bopo.dialogs;

import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.bopo.R;
import com.bopo.fragments.FragmentPrintReady;
import com.bopo.utils.Analytics;
import com.bopo.utils.GenericDialogHelper;
import com.bopo.views.AnimationOnTouchListener;

import java.lang.ref.WeakReference;

/**
 * Created by Nick Yaro on 27/1/2017.
 */

public class TroubleshootDialogFragment extends DialogFragment {
    private WeakReference<FragmentPrintReady> mFragmentPrintReady;

    public static TroubleshootDialogFragment newInstance() {
        TroubleshootDialogFragment troubleshootDialogFragment = new TroubleshootDialogFragment();
        troubleshootDialogFragment.mFragmentPrintReady = new WeakReference<>(null);
        return troubleshootDialogFragment;
    }
    public static TroubleshootDialogFragment newInstance(FragmentPrintReady fragmentPrintReady) {
        TroubleshootDialogFragment troubleshootDialogFragment = new TroubleshootDialogFragment();
        troubleshootDialogFragment.mFragmentPrintReady = new WeakReference<>(fragmentPrintReady);
        return troubleshootDialogFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View rootView = inflater.inflate(R.layout.dialog_troubleshoot, container, false);

        ImageView btnMisalignment = (ImageView)rootView.findViewById(R.id.dialog_troubleshoot_misalignment);
        ImageView btnSpacing = (ImageView)rootView.findViewById(R.id.dialog_troubleshoot_spacing);
        ImageView btnAltPrinting = (ImageView)rootView.findViewById(R.id.dialog_troubleshoot_alt_printing);
        ImageView btnQuestions = (ImageView)rootView.findViewById(R.id.dialog_troubleshoot_questions);

        btnMisalignment.setOnTouchListener(new AnimationOnTouchListener());
        btnSpacing.setOnTouchListener(new AnimationOnTouchListener());
        btnAltPrinting.setOnTouchListener(new AnimationOnTouchListener());
        btnQuestions.setOnTouchListener(new AnimationOnTouchListener());

        btnMisalignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrintAlignmentDialog
                        .newInstance()
                        .show(getFragmentManager(), PrintAlignmentDialog.class.getName());

                Analytics.log(Analytics.ACTION_PRINT_TROUBLE, Analytics.PARAM_TROUBLE_ITEM, "Misalignment");
                TroubleshootDialogFragment.this.dismiss();
            }
        });
        btnSpacing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrintAlignmentDialog
                        .newInstance(R.string.alignment_stretching_message)
                        .show(getFragmentManager(), PrintAlignmentDialog.class.getName());
                Analytics.log(Analytics.ACTION_PRINT_TROUBLE, Analytics.PARAM_TROUBLE_ITEM, "Spacing");
                TroubleshootDialogFragment.this.dismiss();
            }
        });
        btnAltPrinting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GenericDialogHelper.showPaperOptionsFlow(getActivity(), getFragmentManager(), mFragmentPrintReady.get());
                Analytics.log(Analytics.ACTION_PRINT_TROUBLE, Analytics.PARAM_TROUBLE_ITEM, "AltPrinting");
                TroubleshootDialogFragment.this.dismiss();
            }
        });
        btnQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GenericDialogHelper.showUnknownIssue(getActivity());
                Analytics.log(Analytics.ACTION_PRINT_TROUBLE, Analytics.PARAM_TROUBLE_ITEM, "Questions");
                TroubleshootDialogFragment.this.dismiss();
            }
        });

        return rootView;
    }
}
