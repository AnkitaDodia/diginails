package com.bopo.dialogs;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.bopo.R;
import com.bopo.views.AnimationOnTouchListener;

public class GeneralDialog extends DialogFragment {
	Spannable mSpannable;
	String mTitle, mMessage;
	View.OnClickListener mOnPositiveClickListener, mOnNeutralClickListener;
	String mPositiveButtonText, mNeutralButtonText;

	public static GeneralDialog newInstance(String title, Spannable spannable) {
		GeneralDialog generalDialog = new GeneralDialog();
		generalDialog.mTitle = title;
		generalDialog.mSpannable = spannable;

		return generalDialog;
	}

	public static GeneralDialog newInstance(Spannable spannable) {
		GeneralDialog generalDialog = new GeneralDialog();
		generalDialog.mSpannable = spannable;

		return generalDialog;
	}

	public static GeneralDialog newInstance(String message) {
		GeneralDialog generalDialog = new GeneralDialog();
		generalDialog.mMessage = message;

		return generalDialog;
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	public void setOnPositiveClickListener(String text, View.OnClickListener onOkClickListener) {
		mOnPositiveClickListener = onOkClickListener;
		mPositiveButtonText = text;
	}
	public void setOnNeutralClickListener(String text, View.OnClickListener onOkClickListener) {
		mOnNeutralClickListener = onOkClickListener;
		mNeutralButtonText = text;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

		View rootView = inflater.inflate(R.layout.dialog_general, container, false);

		TextView messageTv = (TextView) rootView.findViewById(R.id.textMessage);
		messageTv.setMovementMethod(LinkMovementMethod.getInstance());
		messageTv.setText(mSpannable != null ? mSpannable : Html.fromHtml(mMessage));

		if (mTitle != null && !mTitle.isEmpty()) {
			TextView title = (TextView)rootView.findViewById(R.id.dialog_general_title);
			title.setVisibility(View.VISIBLE);
			title.setText(mTitle);
		}

		final TextView positiveButton = (TextView) rootView.findViewById(R.id.textOk);
		if (mPositiveButtonText != null) {
			positiveButton.setText(mPositiveButtonText);
			positiveButton.setOnTouchListener(new AnimationOnTouchListener(1.25f));
		}
		positiveButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mOnPositiveClickListener != null) {
					mOnPositiveClickListener.onClick(v);
				}
				if (mPositiveButtonText != null) {
					positiveButton.setText(mPositiveButtonText);
				}
				dismiss();
			}
		});

		final TextView neutralButton = (TextView) rootView.findViewById(R.id.textNeutral);
		if (mNeutralButtonText != null) {
			neutralButton.setText(mNeutralButtonText);
			neutralButton.setVisibility(View.VISIBLE);
			neutralButton.setOnTouchListener(new AnimationOnTouchListener(1.25f));
		}
		neutralButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mOnNeutralClickListener != null) {
					mOnNeutralClickListener.onClick(v);
				}

				dismiss();
			}
		});

		return rootView;
	}

}
