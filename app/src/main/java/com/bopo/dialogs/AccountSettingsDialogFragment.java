package com.bopo.dialogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bopo.R;
import com.bopo.api.RequestApi;
import com.bopo.utils.Consts;
import com.bopo.utils.managers.UserManager;
import com.bopo.views.AnimationOnTouchListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Nick Yaro on 1/3/2017.
 */

public class AccountSettingsDialogFragment extends BaseDialogFragment {
    EditText editFirstName, editLastName, editUsername, editEmail, editPassword;

    public static AccountSettingsDialogFragment newInstance() {
        AccountSettingsDialogFragment accountSettingsDialogFragment = new AccountSettingsDialogFragment();
        return accountSettingsDialogFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View rootView = inflater.inflate(R.layout.dialog_account_settings, container, false);

        editFirstName = (EditText)rootView.findViewById(R.id.dialog_account_settings_first_name);
        editLastName = (EditText)rootView.findViewById(R.id.dialog_account_settings_last_name);
        editUsername = (EditText)rootView.findViewById(R.id.dialog_account_settings_username);
        editEmail = (EditText)rootView.findViewById(R.id.dialog_account_settings_email);
        editPassword = (EditText)rootView.findViewById(R.id.dialog_account_password_change_edittext);

        final TextView buttonUpdateInfo = (TextView)rootView.findViewById(R.id.dialog_account_settings_save);
        final TextView buttonUpdatePassword = (TextView)rootView.findViewById(R.id.txtPositive);

        editFirstName.setText(UserManager.getInstance(getActivity()).getValue(UserManager.PARAM_FIRST_NAME));
        editLastName.setText(UserManager.getInstance(getActivity()).getValue(UserManager.PARAM_LAST_NAME));
        editUsername.setText(UserManager.getInstance(getActivity()).getValue(UserManager.PARAM_USERNAME));
        editEmail.setText(UserManager.getInstance(getActivity()).getValue(UserManager.PARAM_EMAIL));

        buttonUpdateInfo.setOnTouchListener(new AnimationOnTouchListener());
        buttonUpdatePassword.setOnTouchListener(new AnimationOnTouchListener());

        buttonUpdateInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInfoSectionValid()) {
                    requestInfoUpdate(
                            new FormBody.Builder()
                                    .add("fname", editFirstName.getText().toString())
                                    .add("lname", editLastName.getText().toString())
                                    .add("email", editEmail.getText().toString())
                                    .add("username", editUsername.getText().toString())
                                    .build()
                    );
                }
            }
        });

        buttonUpdatePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPasswordSectionValid()) {
                    requestInfoUpdate(
                            new FormBody.Builder()
                            .add("password", editPassword.getText().toString())
                            .build()
                    );
                }
            }
        });

        return rootView;
    }

    private boolean isInfoSectionValid() {
        boolean isValid = true;

        if (editFirstName.getText().toString().isEmpty()) {
            editFirstName.setError(getString(R.string.field_cannot_be_empty));
            isValid = false;
        }
        if (editLastName.getText().toString().isEmpty()) {
            editLastName.setError(getString(R.string.field_cannot_be_empty));
            isValid = false;
        }
        if (editUsername.getText().toString().isEmpty()) {
            editUsername.setError(getString(R.string.field_cannot_be_empty));
            isValid = false;
        }

        if (
                editUsername.getText().toString().contains("@") ||
                        editUsername.getText().toString().contains(".com") ||
                        editUsername.getText().toString().contains(".ca")) {
            editUsername.setError(getString(R.string.username_error_email));
            isValid = false;
        }

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(
                editEmail.getText().toString()).matches()) {
            editEmail.setError(getString(R.string.doesnt_seem_right));
            isValid = false;
        }

        return isValid;
    }

    private boolean isPasswordSectionValid() {
        boolean isValid = true;

        if (editPassword.getText().toString().isEmpty() || editPassword.getText().toString().length() < 6) {
            editPassword.setError(getString(R.string.error_password_length));
            isValid = false;
        }

        return isValid;
    }

    private void requestInfoUpdate(RequestBody body) {
        setProgressVisibility(true);

        RequestApi.getInstance(getActivity()).post(Consts.API_URL + "users/secure/update", body, new Callback() {
            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setProgressVisibility(false);
                        try {
                            final String responseStr = response.body().string();
                            String error = RequestApi.getError(responseStr);
                            if (!error.isEmpty()) {
                                showPopup(error);
                            } else {
                                showPopup(getString(R.string.account_settings_updated));
                                dismiss();
                                UserManager.getInstance(getActivity()).setUser(new JSONObject(responseStr));
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }

            @Override
            public void onFailure(Call call, IOException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setProgressVisibility(false);
                        Toast.makeText(getActivity(), R.string.error_network_generic, Toast.LENGTH_LONG).show();
                    }
                });
            }

        });
    }
}