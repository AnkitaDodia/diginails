package com.bopo.dialogs;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bopo.R;
import com.bopo.utils.Analytics;
import com.bopo.views.AnimationOnTouchListener;
import com.bopo.utils.managers.DesignBitmapManager;

/**
 * Created by Nick Yaro on 30/3/2016.
 */
public class PrintAlignmentDialog extends DialogFragment {
    public final static String PRINT_OFFSET_VERTICAL_MM = "offset_vertical";
    public final static String PRINT_OFFSET_HORIZONTAL_MM = "offset_horizontal";

    public final static String PRINT_STRETCH_LEFT = "stretch_left";
    public final static String PRINT_STRETCH_RIGHT = "stretch_right";

    private Bitmap mScaledDownNailDesign;

	SharedPreferences mSharedPreferences;
	
    float mDefaultX, mDefaultY, mMaxX, mMaxY;
	int offsetLeft, offsetTop, stretchLeft, stretchRight;

    @StringRes int titleResId;

    RelativeLayout mAlignmentGrid;
    LinearLayout mMovableLayout;
    ImageView mMovableImageViewOne, mMovableImageViewTwo, mMovableImageViewThree;
    TextView mXposText, mYposText, mStretchLeftTextView, mStretchRightTextView;

    public static PrintAlignmentDialog newInstance() {
        PrintAlignmentDialog printAlignmentDialog = new PrintAlignmentDialog();
        return printAlignmentDialog;
    }

    public static PrintAlignmentDialog newInstance(@StringRes int titleResId) {
        PrintAlignmentDialog printAlignmentDialog = new PrintAlignmentDialog();
        printAlignmentDialog.titleResId = titleResId;
        return printAlignmentDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        final View rootView = inflater.inflate(R.layout.dialog_print_alignment, container, false);
        final OnCanvasTouchListener onCanvasTouchListener = new OnCanvasTouchListener();

        TextView smallDesc = (TextView)rootView.findViewById(R.id.dialog_print_alignment_msg_small);
        smallDesc.setText(Html.fromHtml(getString(R.string.misalignment_message_2_short)));
        smallDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TextView)v).setText(getString(R.string.misalignment_message_2));
            }
        });

        mAlignmentGrid = (RelativeLayout) rootView.findViewById(R.id.dialog_print_alignment_grid);
        mMovableLayout = (LinearLayout) rootView.findViewById(R.id.dialog_print_alignment_movable);
        mXposText = (TextView)rootView.findViewById(R.id.dialog_print_x_pos_text);
        mYposText = (TextView)rootView.findViewById(R.id.dialog_print_y_pos_text);

        mStretchLeftTextView = (TextView)rootView.findViewById(R.id.dialog_print_alignment_stretch_left_count);
        mStretchRightTextView = (TextView)rootView.findViewById(R.id.dialog_print_alignment_stretch_right_count);

        mMovableImageViewOne = (ImageView) rootView.findViewById(R.id.dialog_print_alignment_movable_1);
        mMovableImageViewTwo = (ImageView) rootView.findViewById(R.id.dialog_print_alignment_movable_2);
        mMovableImageViewThree = (ImageView) rootView.findViewById(R.id.dialog_print_alignment_movable_3);

        if (DesignBitmapManager.getInstance().getNailDesignBitmap() != null) {
            mScaledDownNailDesign = Bitmap.createScaledBitmap(DesignBitmapManager.getInstance().getNailDesignBitmap(), 110, 210, true);
            mMovableImageViewOne.setImageBitmap(mScaledDownNailDesign);
            mMovableImageViewTwo.setImageBitmap(mScaledDownNailDesign);
            mMovableImageViewThree.setImageBitmap(mScaledDownNailDesign);
        }
        mMovableLayout.setOnTouchListener(onCanvasTouchListener);
		
        rootView.findViewById(R.id.dialog_print_alignment_reset).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetPosition();
            }
        });

        rootView.findViewById(R.id.dialog_print_alignment_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
                dismiss();
            }
        });

        rootView.findViewById(R.id.dialog_print_alignment_stretch_left_plus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stretchLeft >= 10) return;
                stretchLeft++;
                updateStretching();
            }
        });

        rootView.findViewById(R.id.dialog_print_alignment_stretch_left_minus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stretchLeft <= -10) return;
                stretchLeft--;
                updateStretching();
            }
        });

        rootView.findViewById(R.id.dialog_print_alignment_stretch_right_plus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stretchRight >= 10) return;
                stretchRight++;
                updateStretching();
            }
        });

        rootView.findViewById(R.id.dialog_print_alignment_stretch_right_minus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stretchRight <= -10) return;
                stretchRight--;
                updateStretching();
            }
        });

        rootView.findViewById(R.id.dialog_print_alignment_stretch_left_plus).setOnTouchListener(new AnimationOnTouchListener(0.9f));
        rootView.findViewById(R.id.dialog_print_alignment_stretch_left_minus).setOnTouchListener(new AnimationOnTouchListener(0.9f));
        rootView.findViewById(R.id.dialog_print_alignment_stretch_right_plus).setOnTouchListener(new AnimationOnTouchListener(0.9f));
        rootView.findViewById(R.id.dialog_print_alignment_stretch_right_minus).setOnTouchListener(new AnimationOnTouchListener(0.9f));

        if (titleResId > 0) {
            ((TextView)rootView.findViewById(R.id.dialog_print_alignment_msg_large)).setText(titleResId);
        }

		mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        loadData(mMovableLayout);

        refreshDefaultXY();

        return rootView;
    }

    private void refreshDefaultXY() {
        mDefaultX = Math.round((mAlignmentGrid.getWidth() - mMovableLayout.getWidth()) / 2 / 10) * 10;
        mDefaultY = Math.round((mAlignmentGrid.getHeight() - mMovableLayout.getHeight()) / 2 / 10) * 10;

        if (mMaxX == 0 || mMaxY == 0) {
            mMaxX = mDefaultX * 2;
            mMaxY = mDefaultY * 2 + 250;
        }
    }

    private void updateAlignmentLabels(float x, float y) {
        if (mDefaultX == 0 || mDefaultY == 0) { return; }

        String xText;
        String yText;

        if (x == mDefaultX) {
            offsetLeft = 0;
            xText = getActivity().getString(R.string.centered_horizontally);
            mXposText.setTextColor(Color.GRAY);
        } else {
            offsetLeft = Math.round((x - mDefaultX) / 20);
            xText = String.format("%d mm\n%s", removeSign(offsetLeft), x < mDefaultX ? "Left" : "Right");
            mXposText.setTextColor(Color.RED);
        }

        if (y == mDefaultY) {
            offsetTop = 0;
            yText = getActivity().getString(R.string.centered_vertically);
            mYposText.setTextColor(Color.GRAY);
        } else {
            offsetTop = Math.round((y - mDefaultY) / 20);
            yText = String.format("%d mm\n%s", removeSign(offsetTop), y < mDefaultY ? "Up" : "Down");
            mYposText.setTextColor(Color.RED);
        }

        mXposText.setText(xText);
        mYposText.setText(yText);
    }

    private void resetPosition() {
        if (mDefaultX == 0 || mDefaultY == 0) { return; }

        updateAlignmentLabels(mDefaultX, mDefaultY);

        stretchLeft = 0;
        stretchRight = 0;
        updateStretching();

        mMovableLayout.animate()
                .x(mDefaultX)
                .y(mDefaultY)
                .setDuration(0)
                .start();
    }

    private void updateStretching() {
        mStretchLeftTextView.setText(String.valueOf(stretchLeft));
        mStretchRightTextView.setText(String.valueOf(stretchRight));

        int widthFactor = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics()));
        int width = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60 + (stretchLeft + stretchRight), getResources().getDisplayMetrics()));

        LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) mMovableImageViewOne.getLayoutParams();
        params1.width = width + widthFactor;
        mMovableImageViewOne.setLayoutParams(params1);

        LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) mMovableImageViewTwo.getLayoutParams();
        params2.width = width;
        mMovableImageViewTwo.setLayoutParams(params2);

        LinearLayout.LayoutParams params3 = (LinearLayout.LayoutParams) mMovableImageViewThree.getLayoutParams();
        params3.width = width - widthFactor;
        mMovableImageViewThree.setLayoutParams(params3);

        /*int paddingRight = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, stretchLeft * 2, getResources().getDisplayMetrics()));
        int paddingLeft = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, stretchRight * 2, getResources().getDisplayMetrics()));

        if (paddingRight < 0) {
            paddingLeft += paddingRight * -1;
            paddingRight = 0;
        }

        if (paddingLeft < 0) {
            paddingRight += paddingLeft * -1;
            paddingLeft = 0;
        }

        mMovableLayout.setPadding(paddingLeft, 0, paddingRight, 0);*/

        refreshDefaultXY();
    }

    class OnCanvasTouchListener implements View.OnTouchListener {
        float dX, dY;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            refreshDefaultXY();

            switch (event.getActionMasked()) {

                case MotionEvent.ACTION_DOWN:

                    dX = v.getX() - event.getRawX();
                    dY = v.getY() - event.getRawY();

                    break;

                case MotionEvent.ACTION_MOVE:
                    float x = event.getRawX() + dX - (event.getRawX() + dX) % 20;
                    if (x < 0) {
                        x = 0;
                    }
                    if (x > mMaxX) {
                        x = mMaxX;
                    }

                    float y = event.getRawY() + dY - (event.getRawY() + dY) % 20;
                    if (y < -250) {
                        y = -250;
                    }
                    if (y > mMaxY) {
                        y = mMaxY;
                    }

                    v.animate()
                            .x(x)
                            .y(y)
                            .setDuration(0)
                            .start();

                    updateAlignmentLabels(x, y);
                    break;
                default:
                    return false;
            }
            return true;
        }
    }

    public void saveData() {
        SharedPreferences.Editor sharedPreferencesEditor = mSharedPreferences.edit();

        sharedPreferencesEditor.putInt(PRINT_OFFSET_VERTICAL_MM, offsetTop);
        sharedPreferencesEditor.putInt(PRINT_OFFSET_HORIZONTAL_MM, offsetLeft);

        sharedPreferencesEditor.putInt(PRINT_STRETCH_LEFT, stretchLeft);
        sharedPreferencesEditor.putInt(PRINT_STRETCH_RIGHT, stretchRight);

        sharedPreferencesEditor.commit();

        Analytics.log(
                Analytics.ACTION_ALIGNMENT_SAVE,
                Analytics.PARAM_ALIGNMENT_VALUE,
                String.format("OT: %s, OL: %s, SL: %s, SR: %s", offsetTop, offsetLeft, stretchLeft, stretchRight));
    }

    public void loadData(View v) {
        refreshDefaultXY();
        
		int top = mSharedPreferences.getInt(PRINT_OFFSET_VERTICAL_MM, 0);
		int left = mSharedPreferences.getInt(PRINT_OFFSET_HORIZONTAL_MM, 0);

		int x = Math.round((left * 20));
		int y = Math.round((top * 20));
		
		v.animate()
				.x(x)
				.y(y)
				.setDuration(0)
				.start();

        stretchLeft = mSharedPreferences.getInt(PRINT_STRETCH_LEFT, 0);
        stretchRight = mSharedPreferences.getInt(PRINT_STRETCH_RIGHT, 0);

		updateAlignmentLabels(mDefaultX + x, mDefaultY + y);
        updateStretching();
    }

    long removeSign(int num){
        return num < 0 ? num * -1 : num;
    }
}
