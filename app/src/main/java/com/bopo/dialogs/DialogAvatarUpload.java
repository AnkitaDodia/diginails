package com.bopo.dialogs;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bopo.R;
import com.bopo.api.RequestApi;
import com.bopo.fonts.KelsonBoldFontTextView;
import com.bopo.fragments.FragmentAccount;
import com.bopo.utils.MediaUtils;
import com.bopo.utils.managers.DrawingManager;
import com.bopo.utils.managers.UserManager;
import com.bopo.views.AnimationOnTouchListener;
import com.bopo.views.RoundImageView;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Nick Yaro on 28/2/2017.
 */

public class DialogAvatarUpload extends BaseDialogFragment {
    RoundImageView avatarImageView;
    LinearLayout uploadCameraGallery, uploadSaveRetry;
    KelsonBoldFontTextView cameraButton, galleryButton, saveButton, retryButton, cancelButton;

    public static DialogAvatarUpload newInstance() {
        DialogAvatarUpload dialogAvatarUpload = new DialogAvatarUpload();
        return dialogAvatarUpload;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View rootView = inflater.inflate(R.layout.dialog_avatar_upload, container, false);
        avatarImageView = (RoundImageView)rootView.findViewById(R.id.dialog_avatar_upload_image);
        uploadCameraGallery = (LinearLayout)rootView.findViewById(R.id.dialog_avatar_upload_camera_gallery);
        uploadSaveRetry = (LinearLayout)rootView.findViewById(R.id.dialog_avatar_upload_save_retry);

        cameraButton = (KelsonBoldFontTextView)rootView.findViewById(R.id.dialog_avatar_upload_camera);
        galleryButton = (KelsonBoldFontTextView)rootView.findViewById(R.id.dialog_avatar_upload_gallery);
        saveButton = (KelsonBoldFontTextView)rootView.findViewById(R.id.dialog_avatar_upload_save);
        retryButton = (KelsonBoldFontTextView)rootView.findViewById(R.id.dialog_avatar_upload_retry);
        cancelButton = (KelsonBoldFontTextView)rootView.findViewById(R.id.dialog_avatar_upload_cancel);

        cameraButton.setOnTouchListener(new AnimationOnTouchListener());
        galleryButton.setOnTouchListener(new AnimationOnTouchListener());
        saveButton.setOnTouchListener(new AnimationOnTouchListener());
        retryButton.setOnTouchListener(new AnimationOnTouchListener());
        cancelButton.setOnTouchListener(new AnimationOnTouchListener());

        DrawingManager.getInstance().tempBitmap = null;

        loadImage();

        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaUtils.openCamera(getActivity());
            }
        });

        galleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaUtils.openGallery(getActivity());
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveImage();
            }
        });

        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadCameraGallery.setVisibility(View.VISIBLE);
                uploadSaveRetry.setVisibility(View.GONE);
                loadImage();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return rootView;
    }

    public void onResume() {
        super.onResume();

        if (DrawingManager.getInstance().tempBitmap != null && DrawingManager.getInstance().tempBitmap.bitmap != null) {
            avatarImageView.setImageBitmap(DrawingManager.getInstance().tempBitmap.bitmap);

            uploadCameraGallery.setVisibility(View.GONE);
            uploadSaveRetry.setVisibility(View.VISIBLE);
        }
    }

    public void loadImage() {
        String imageUrl = String.format(RequestApi.URL_AVATARS, UserManager.getInstance(getActivity()).getValue(UserManager.PARAM_ID));
        Picasso.with(getActivity()).load(imageUrl).into(avatarImageView);
    }

    public void saveImage() {
        setProgressVisibility(true, 0, R.string.saving_your_avatar);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DrawingManager.getInstance().tempBitmap.bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] bitmapByteArray = stream.toByteArray();

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("avatar", "avatar-upload.png", RequestBody.create(RequestApi.PNG, bitmapByteArray))
                .build();

        RequestApi.getInstance(getActivity()).post(RequestApi.POST_AVATAR_UPLOAD, requestBody, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                setProgressVisibility(false);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), R.string.error_network_generic, Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                setProgressVisibility(false);
                String res = response.body().string();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (getTargetFragment() != null && getTargetFragment() instanceof FragmentAccount) {
                            ((FragmentAccount) getTargetFragment()).onAvatarUploaded();
                        }
                        showPopup(getString(R.string.avatar_updated));
                        dismiss();
                    }
                });
            }
        });
    }
}
