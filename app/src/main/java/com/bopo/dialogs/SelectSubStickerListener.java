package com.bopo.dialogs;

import android.graphics.Bitmap;

public interface SelectSubStickerListener {

	void onSelectSubSticker(Bitmap bmp);
}
