package com.bopo.dialogs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bopo.api.RequestApi;
import com.bopo.bean.SubStickerItem;
import com.bopo.fragments.FragmentSticker;
import com.bopo.utils.Logger;
import com.bopo.R;
import com.bopo.adapters.StickersCategoryAdapter;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class SelectSubStickerDialogFragment extends DialogFragment {
	FragmentSticker messageFragment;
	private View rootView;
	List<SubStickerItem> listSubStickerItems;
	GridView gridStickerSub;
	ImageView imgClose;
	String paramStickerID;
	TextView textViewTitleStrick;
	String stickerName;

	public static SelectSubStickerDialogFragment newInstance(FragmentSticker messageFragment,
															 String paramStickerID, String stickerName) {
		SelectSubStickerDialogFragment dialog = new SelectSubStickerDialogFragment();
		dialog.messageFragment = messageFragment;
		dialog.paramStickerID = paramStickerID;
		dialog.stickerName = stickerName;
		return dialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		getDialog().getWindow().setBackgroundDrawable(
				new ColorDrawable(Color.TRANSPARENT));
		getDialog().setCanceledOnTouchOutside(false);
		rootView = inflater.inflate(R.layout.dialog_sub_sticker, container,
				false);
		gridStickerSub = (GridView) rootView.findViewById(R.id.gridStickerSub);
		textViewTitleStrick = (TextView) rootView
				.findViewById(R.id.textViewTitleStrick);
		textViewTitleStrick.setText(stickerName);
		imgClose = (ImageView) rootView.findViewById(R.id.imgClose);
		imgClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
		gridStickerSub.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Logger.e("listSubStickerItems.get(position).imgPath::"
						+ listSubStickerItems.get(position).imgPath);
				try {
					String url = listSubStickerItems.get(position).imgPath;
					Logger.e("Pattern url ::" + url);
					Picasso.with(getActivity()).load(url).into(target);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		getSubStickers();
		return rootView;
	}

	Target target = new Target() {

		@Override
		public void onPrepareLoad(Drawable arg0) {
			Logger.e("onPrepareLoad");

		}

		@Override
		public void onBitmapLoaded(final Bitmap bmap, LoadedFrom arg1) {
			messageFragment.onSelectSubSticker(bmap);
			dismiss();
		}

		@Override
		public void onBitmapFailed(Drawable arg0) {
			Logger.e("onBitmapFailed");

		}
	};

	public void getSubStickers() {
		RequestApi.getInstance(getActivity()).get(
				RequestApi.GET_STICKERS_BY_CATEGORY + paramStickerID,
				new Callback() {
					@Override
					public void onFailure(Call call, IOException e) {
						dismiss();
						messageFragment.showPopup(getString(R.string.error_network_generic));
					}
					@Override
					public void onResponse(Call call, Response response) throws IOException {
						String responseBody = response.body().string();
						JSONObject jsonObject;

						if (responseBody.isEmpty() || !response.isSuccessful()) {
							dismiss();
							messageFragment.showPopup(getString(R.string.error_network_generic));
							return;
						}

						try {
							jsonObject = new JSONObject(responseBody);
							if (jsonObject.has("error")) {
								dismiss();
								messageFragment.showPopup(jsonObject.getString("error"));
								return;
							}

							listSubStickerItems = new ArrayList<SubStickerItem>();

							JSONArray jsonArray = jsonObject.getJSONArray("stickers");

							for (int i = 0; i < jsonArray.length(); i++) {
								JSONObject objData = jsonArray.getJSONObject(i);
								SubStickerItem st = new SubStickerItem();
								st.id = objData.getString("id");
								st.name = objData.getString("name");
								st.imgPath = objData.getString("img_path");

								listSubStickerItems.add(st);

							}
						} catch (JSONException e) {
							dismiss();
							messageFragment.showPopup(getString(R.string.error_network_generic));
							return;
						}

						if (listSubStickerItems.size() > 0) {
							final StickersCategoryAdapter stickersCategoryBaseAdapter = new StickersCategoryAdapter(
									getActivity(), listSubStickerItems);
							getActivity().runOnUiThread(new Runnable() {
								@Override
								public void run() {
									gridStickerSub.setAdapter(stickersCategoryBaseAdapter);
								}
							});

						} else {
							dismiss();
							messageFragment.showPopup(getString(R.string.error_network_generic));
						}
					}
				}
		);
	}

}
