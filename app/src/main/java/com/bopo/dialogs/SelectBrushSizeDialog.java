package com.bopo.dialogs;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.bopo.R;
import com.bopo.fragments.FragmentDesigner;
import com.bopo.views.DrawingView;

public class SelectBrushSizeDialog extends DialogFragment implements View.OnClickListener {
	View rootView;
	ImageView imgClose;
	DrawingView drawingView;
	FragmentDesigner fragmentDesigner;

	public static SelectBrushSizeDialog newInstance(FragmentDesigner fragmentDesigner, DrawingView drawingView) {
		SelectBrushSizeDialog selectBrushSizeDialog = new SelectBrushSizeDialog();
		selectBrushSizeDialog.fragmentDesigner = fragmentDesigner;
		selectBrushSizeDialog.drawingView = drawingView;
		return selectBrushSizeDialog;
	}

	public SelectBrushSizeDialog() {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		getDialog().getWindow().setBackgroundDrawable(
				new ColorDrawable(Color.TRANSPARENT));

		rootView = inflater.inflate(R.layout.dialog_brush_size, container,
				false);

		rootView.findViewById(R.id.size_1).setOnClickListener(this);
		rootView.findViewById(R.id.size_2).setOnClickListener(this);
		rootView.findViewById(R.id.size_3).setOnClickListener(this);
		rootView.findViewById(R.id.size_4).setOnClickListener(this);

		return rootView;
	}

	@Override
	public void onClick(View v) {
		int size = getResources().getInteger(R.integer.brush_size_2);

		switch (v.getId()) {
			case R.id.size_1:
				size = getResources().getInteger(R.integer.brush_size_1);
				break;
			case R.id.size_2:
				size = getResources().getInteger(R.integer.brush_size_2);
				break;
			case R.id.size_3:
				size = getResources().getInteger(R.integer.brush_size_3);
				break;
			case R.id.size_4:
				size = getResources().getInteger(R.integer.brush_size_4);
				break;
		}
		drawingView.setBrushSize(size);
		fragmentDesigner.openColorPicker();

		dismiss();
	}
}
