package com.bopo.enums;

/**
 * Created by Nick Yaro on 3/3/2016.
 */
public enum AnimationType {
    NONE,
    IN,
    OUT
}
