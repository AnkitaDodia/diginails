package com.bopo.enums;

/**
 * Created by Nick Yaro on 4/3/2016.
 */

//Library Class
public enum DrawingEventType {
    DRAW,
    TEXT,
    PATTERN,
    BITMAP_MOVABLE,
    BITMAP_STATIC,
}
