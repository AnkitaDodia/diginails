package com.bopo.enums;

/**
 * Created by Nick Yaro on 3/3/2016.
 */
public enum Fragments {
    DesignerFragment,
    PrintReadyFragment,
    PrintCompleteFragment,
    PatternsFragment,
    StickersFragment,
    PictureFromWebFragment,
    LoginFragment,
    RegisterFragment,
    ForgotPasswordFragment,
    GalleryFragment,
    AccountFragment
}
