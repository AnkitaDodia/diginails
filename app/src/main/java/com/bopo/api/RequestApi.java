package com.bopo.api;

import android.content.Context;
import android.graphics.Bitmap;

import com.bopo.utils.Consts;
import com.bopo.utils.managers.UserManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Nick Yaro on 8/3/2016.
 */
public class RequestApi {
    public final static String GET_DESIGNS_MOST_DOWNLOADED = Consts.API_URL + "user-designs-all/99/%d/most_downloaded";
    public final static String GET_DESIGNS_HIGHEST_RATED = Consts.API_URL + "user-designs-all/99/%d/highest_rated";
    public final static String GET_DESIGNS_MOST_RECENT = Consts.API_URL + "user-designs-all/99/%d/most_recent";
    public final static String POST_RATE_DESIGN = Consts.API_URL + "rate-design";

    public final static String POST_LOGIN = Consts.API_URL + "users/secure/login";
    public final static String POST_REGISTER = Consts.API_URL + "users/secure/register";
    public final static String GET_REGISTER_CHECK_USERNAME = Consts.API_URL + "users-check-username/"; // + username
    public final static String POST_USER_UPDATE = Consts.API_URL + "users/secure/update/%s"; // token
    public final static String GET_RESET_PASSWORD = Consts.API_URL + "users/secure/reset?email=%s"; // + email
    public final static String POST_AVATAR_UPLOAD = Consts.API_URL + "users/secure/avatar";

    public final static String GET_PATTERNS = Consts.API_URL + "patterns";
    public final static String GET_STICKER_CATEGORIES = Consts.API_URL + "sticker-cats";
    public final static String GET_STICKERS_BY_CATEGORY = Consts.API_URL + "stickers-by-cat/"; // + category id

    public final static String GET_USER_DESIGNS_PUBLIC = Consts.API_URL + "user-designs/%s"; // user id
    public final static String GET_USER_DESIGNS = Consts.API_URL + "user-designs/%s/all"; // user id
    public final static String PUT_USER_DESIGNS = Consts.API_URL + "user-designs";
    public final static String DELETE_USER_DESIGNS = Consts.API_URL + "user-designs/%s"; // design id
    public final static String POST_UPLOAD_DESIGN = Consts.API_URL + "uploader.php";
    public final static String POST_DESIGN_UPLOAD = Consts.API_URL + "designs/upload";

    public final static String URL_UPLOADED_DESIGNS = Consts.API_URL + "designs/";
    public final static String URL_AVATARS = Consts.ROOT_URL + "data/avatars/%s.jpg"; //id


    private static RequestApi mInstance;

    private WeakReference<Context> weakContext;
    private OkHttpClient client;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public static final MediaType PNG = MediaType.parse("image/png");
    private static final String KEY_TOKEN = "token";

    private RequestApi(Context c) {
        int cacheSize = 30 * 1024 * 1024; // 30 MiB
        Cache cache = new Cache(c.getCacheDir(), cacheSize);

        client = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .cache(cache)
                .build();
    }

    public static RequestApi getInstance(Context c) {
        if (mInstance == null) {
            mInstance = new RequestApi(c);
        }
        mInstance.weakContext = new WeakReference<>(c);
        return mInstance;
    }

    public void get(String url, Callback callback) {
        Request request = new Request.Builder().url(url).build();
        client.newCall(request).enqueue(callback);
    }

    public void post(String url, JSONObject params, Callback callback) {
        RequestBody body = RequestBody.create(JSON, params.toString());
        Request request = new Request.Builder()
                .header(KEY_TOKEN, UserManager.getInstance(weakContext.get()).getValue(UserManager.PARAM_TOKEN))
                .url(url)
                .post(body)
                .build();
        client.newCall(request).enqueue(callback);
    }

    public void post(String url, RequestBody body, Callback callback) {
        Request request = new Request.Builder()
                .header(KEY_TOKEN, UserManager.getInstance(weakContext.get()).getValue(UserManager.PARAM_TOKEN))
                .url(url)
                .post(body)
                .build();
        client.newCall(request).enqueue(callback);
    }

    public void delete(String url, Callback callback) {
        Request request = new Request.Builder()
                .url(url)
                .delete()
                .build();
        client.newCall(request).enqueue(callback);
    }

    public void rateDesign(Callback callback, int designId, int rateStars, String userId) {
        RequestBody body = new FormBody.Builder()
                .add("design_id", String.valueOf(designId))
                .add("rate_stars", String.valueOf(rateStars))
                .add("user_id", userId)
                .build();

        post(POST_RATE_DESIGN, body, callback);
    }

    public void saveDesign(final String imageName, final Bitmap nailBitmap, final Bitmap printBitmap, final String userId, final boolean isPublic, final Callback callback) {
        uploadDesignImage(nailBitmap, imageName, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.onFailure(call, e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String responseStr = response.body().string();
                uploadDesignImage(printBitmap, "print-" + imageName, new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        callback.onFailure(call, e);
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        String filePath = URL_UPLOADED_DESIGNS + imageName;
                        postUserDesign(filePath, userId, isPublic, callback);
                    }
                });
            }
        });
    }

    private void uploadDesignImage(Bitmap bitmap, String filename, Callback callback) {
        //String filename = imagePath.substring(imagePath.lastIndexOf("/") + 1);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] bitmapByteArray = stream.toByteArray();

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("uploadedfile", filename, RequestBody.create(PNG, bitmapByteArray))
                .build();

        Request request = new Request.Builder()
                .url(POST_UPLOAD_DESIGN)
                .post(requestBody)
                .build();

        client.newCall(request).enqueue(callback);
    }

    private void postUserDesign(String filePath, String userId, boolean isPublic, Callback callback) {
        JSONObject params = new JSONObject();
        try {
            params.put("img_path", filePath);
            params.put("users_id", userId);
            params.put("device_id", "Android");
            params.put("public_img", isPublic ? "1" : "0");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        post(PUT_USER_DESIGNS, params, callback);
    }

    public void postDesignN(Bitmap bitmap, boolean isPublic, Callback callback) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] bitmapByteArray = stream.toByteArray();

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("design", "design-image.png", RequestBody.create(PNG, bitmapByteArray))
                .addFormDataPart("device_id", "Android")
                .addFormDataPart("is_public", isPublic ? "1" : "0")
                .build();

        Request request = new Request.Builder()
                .header(KEY_TOKEN, UserManager.getInstance(weakContext.get()).getValue(UserManager.PARAM_TOKEN))
                .url(POST_DESIGN_UPLOAD)
                .post(requestBody)
                .build();

        client.newCall(request).enqueue(callback);
    }

    public static class RequestBuilder {
        JSONObject params = new JSONObject();

        public RequestBuilder() {}

        public RequestBuilder put(String key, String value) {
            try {
                params.put(key, value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return this;
        }

        public JSONObject build() {
            return params;
        }
    }

    public static String getError(String str) {
        try {
            JSONObject obj = new JSONObject(str);
            JSONObject errorObj = obj.optJSONObject("error");
            if (errorObj != null && !errorObj.optString("text").isEmpty()) {
                return errorObj.optString("text");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }
}
