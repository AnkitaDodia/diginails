package com.bopo.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.bopo.fragments.FragmentLogin;
import com.bopo.utils.Analytics;
import com.bopo.utils.AnimationUtils;
import com.bopo.R;
import com.bopo.utils.Consts;
import com.bopo.utils.managers.UserManager;

public class SplashActivity extends Activity {
	private static final String TAG = SplashActivity.class.getSimpleName();
    private static final String KEY_PREFERENCES = "spash_prefs";
    private static final String KEY_SPLASH_VIDEO_SHOWN = "splash_video_shown";
	public static final int SPLASH_TIMEOUT = 1500;

	public final static String KEY_TOKEN = "user_token";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash);

		if (Build.VERSION.SDK_INT >= 21) {
			getWindow().setNavigationBarColor(getResources().getColor(R.color.blue_koi));
			//getWindow().setStatusBarColor(getResources().getColor(R.color.md_red_700));
		}

		try {
			String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;

			String version = String.format(getString(R.string.version), versionName);
			((TextView)findViewById(R.id.version_view)).setText(version);
		} catch (Exception e) {
			e.printStackTrace();
		}

		AnimationUtils.scaleView(findViewById(R.id.logo), 0f, 1f, 1000);

        attemptUpgradePrefs();

		if (wasVideoShown()) {
			Handler handler = new Handler();
            handler.postDelayed(
					new Runnable() {
						public void run() {
							startMainScreen();
						}
					},
					SPLASH_TIMEOUT);
		} else {
            showVideo();
		}
	}

	private void startMainScreen() {
		Intent intent = new Intent(SplashActivity.this, BaseActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);

		finish();
	}

	private void showVideo() {
        findViewById(R.id.version_view).setVisibility(View.GONE);
        findViewById(R.id.logo).setVisibility(View.GONE);
        findViewById(R.id.logo1).setVisibility(View.VISIBLE);

        final VideoView videoView = (VideoView) findViewById(R.id.splashVideoView);
        final RelativeLayout videoSkipButton = (RelativeLayout) findViewById(R.id.buttonSplashSkipVideo);

        videoView.setVisibility(View.VISIBLE);
        videoSkipButton.setVisibility(View.VISIBLE);

        videoSkipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoView.stopPlayback();
                startMainScreen();
				Analytics.log(Analytics.ACTION_SPLASH_SKIP);
            }
        });

		try {
            videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.dns_video));

		} catch (Exception e) {
			e.printStackTrace();
		}

        videoView.requestFocus();

		//Sets media volume at 20%
		AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
				audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) / 5,
				AudioManager.FLAG_SHOW_UI);

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
			public void onPrepared(MediaPlayer mediaPlayer) {
                videoView.start();
			}
		});

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                startMainScreen();
            }
        });
	}

	@Override
	public void onPause() {
		super.onPause();
		startMainScreen();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		startMainScreen();
	}

    private boolean wasVideoShown() {
        SharedPreferences sharedPref = getSharedPreferences(KEY_PREFERENCES, Context.MODE_PRIVATE);
        boolean wasVideoShown = sharedPref.getBoolean(KEY_SPLASH_VIDEO_SHOWN, false);

        if (!wasVideoShown) {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean(KEY_SPLASH_VIDEO_SHOWN, true);
            editor.apply();
        }

        return wasVideoShown;
    }

	public void attemptUpgradePrefs() {
		SharedPreferences prefs = this.getSharedPreferences(Consts.BOPO, Context.MODE_PRIVATE);

		if (!prefs.getString(KEY_TOKEN, "").isEmpty()) {
			UserManager.getInstance(this).setValue(UserManager.PARAM_TOKEN, prefs.getString(KEY_TOKEN, ""));
			UserManager.getInstance(this).setValue(UserManager.PARAM_ID, prefs.getString("user_id", ""));
			UserManager.getInstance(this).setValue(UserManager.PARAM_USERNAME, prefs.getString("username", ""));
			UserManager.getInstance(this).setValue(UserManager.PARAM_FIRST_NAME, prefs.getString("fname", ""));

			SharedPreferences.Editor editor = prefs.edit();
			editor.putString(KEY_TOKEN, "");
			editor.putString("user_id", "");
			editor.putString("username", "");
			editor.commit();
		}
	}

}
