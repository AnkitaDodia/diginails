package com.bopo.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bopo.BuildConfig;
import com.bopo.api.RequestApi;
import com.bopo.fragments.FragmentAccount;
import com.bopo.fragments.FragmentForgotPassword;
import com.bopo.fragments.FragmentMyDesigns;
import com.bopo.fragments.FragmentPrintComplete;
import com.bopo.fragments.FragmentPrintReady;
import com.bopo.fragments.FragmentRegister;
import com.bopo.fragments.FragmentGallery;
import com.bopo.intefaces.OnBackPressedListener;
import com.bopo.intefaces.OnDispatchKeyEventListener;
import com.bopo.utils.Analytics;
import com.bopo.enums.AnimationType;
import com.bopo.utils.MediaUtils;
import com.bopo.utils.managers.DrawingManager;
import com.bopo.utils.managers.DesignBitmapManager;
import com.bopo.enums.Fragments;
import com.bopo.R;
import com.bopo.fragments.FragmentLogin;
import com.bopo.fragments.FragmentDesigner;
import com.bopo.fragments.FragmentPatterns;
import com.bopo.fragments.FragmentPictureFromWeb;
import com.bopo.fragments.FragmentSticker;
import com.bopo.utils.managers.UserManager;
import com.bopo.views.AnimationOnTouchListener;
import com.bopo.views.MultipleOrientationSlidingDrawer;
import com.bopo.views.MultipleOrientationSlidingDrawer.OnDrawerCloseListener;
import com.bopo.views.MultipleOrientationSlidingDrawer.OnDrawerOpenListener;
import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

public class BaseActivity extends FragmentActivity implements OnDrawerOpenListener, OnDrawerCloseListener, OnClickListener {
	private final static int BACK_BUTTON_QUIT_THRESHOLD = 4000;

	private ProgressDialog mProgressDialog;

	private ImageView imgLogo, imgLoginLogout, menuUserImage;
	private LinearLayout linGoBack;
	private TextView menuUserName, textLoginLogout;
	private MultipleOrientationSlidingDrawer mSlidingDrawer;
	private long mLastBackButtonTouchTime;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.activity_main);

		showChooseLan();
		imgLogo = (ImageView) findViewById(R.id.imgLogo);
		mSlidingDrawer = (MultipleOrientationSlidingDrawer)findViewById(R.id.drawer);

		LinearLayout linCreateNewDesign = (LinearLayout) findViewById(R.id.linCreateNewDesign);
		LinearLayout linRecentCreations = (LinearLayout) findViewById(R.id.linRecentCreations);
		LinearLayout linDesignGallery = (LinearLayout) findViewById(R.id.linDesignGallery);
		LinearLayout linLoginLogout = (LinearLayout) findViewById(R.id.linLoginLogout);
		LinearLayout linFeedback = (LinearLayout) findViewById(R.id.linOrder);

        linCreateNewDesign.setOnTouchListener(new AnimationOnTouchListener());
        linRecentCreations.setOnTouchListener(new AnimationOnTouchListener());
        linDesignGallery.setOnTouchListener(new AnimationOnTouchListener());
        linLoginLogout.setOnTouchListener(new AnimationOnTouchListener());
        linFeedback.setOnTouchListener(new AnimationOnTouchListener());

		textLoginLogout = (TextView) findViewById(R.id.textLoginLogout);
		imgLoginLogout = (ImageView) findViewById(R.id.imgLoginLogout);
		linGoBack = (LinearLayout) findViewById(R.id.linGoBack);
		menuUserName = (TextView)findViewById(R.id.menuUserName);
		menuUserImage = (ImageView)findViewById(R.id.menuUserImage);

		linCreateNewDesign.setOnClickListener(this);
		linRecentCreations.setOnClickListener(this);
		linDesignGallery.setOnClickListener(this);
		linLoginLogout.setOnClickListener(this);
		linFeedback.setOnClickListener(this);
		linGoBack.setOnClickListener(this);
		linGoBack.setOnClickListener(this);

		loadFragment(Fragments.GalleryFragment);

		refreshMenuAccount();

		if (!BuildConfig.DEBUG) {
			Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
				@Override
				public void uncaughtException(Thread thread, Throwable e) {
					Crashlytics.logException(e);

					Intent i = new Intent(getApplicationContext(), BaseActivity.class);
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
					i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					i.putExtra("fatal-error", true);
					startActivity(i);

					System.exit(1);
				}
			});

			final boolean fatalError = getIntent().getBooleanExtra("fatal-error", false);
			if (fatalError) {
				Toast.makeText(getApplicationContext(), R.string.crash_message, Toast.LENGTH_LONG).show();
			}
		}
	}

	private void showChooseLan() {


	}

	@Override
	public void onDrawerClosed() {
		imgLogo.setVisibility(View.VISIBLE);
	}

	@Override
	public void onDrawerOpened() {
		if (UserManager.getInstance(this).isSignedIn()) {
			textLoginLogout.setText(getString(R.string.logout));
			imgLoginLogout.setBackgroundResource(R.drawable.menu_icon_logout6);
		} else {
			textLoginLogout.setText(getString(R.string.login));
			imgLoginLogout.setBackgroundResource(R.drawable.menu_icon_login6);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.linLoginLogout:
			if (UserManager.getInstance(this).isSignedIn()) {
				loadFragment(Fragments.AccountFragment);
			} else {
				loadFragment(Fragments.LoginFragment);
			}
			refreshMenuAccount();
			break;

		case R.id.linCreateNewDesign:
            DesignBitmapManager.getInstance().reset();
			DrawingManager.getInstance().reset();
            loadFragment(Fragments.DesignerFragment, AnimationType.OUT);
			break;

		case R.id.linRecentCreations:
			loadFragment(FragmentMyDesigns.newInstance());
			break;

		case R.id.linDesignGallery:
			loadFragment(Fragments.GalleryFragment);
			break;
		case R.id.linGoBack:
			onBackPressed();
			break;
		case R.id.linOrder:
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.amazon_url)));
			startActivity(browserIntent);
			Analytics.log(Analytics.ACTION_MENU_ORDER_ONLINE);
			break;
		default:
			break;
		}

		if (mSlidingDrawer.isOpened()) {
			mSlidingDrawer.toggle();
		}
	}

	@Override
	public void onBackPressed() {
		FragmentManager manager = getSupportFragmentManager();
		Fragment fragment = manager.findFragmentById(R.id.content_frame);

		if (!(fragment instanceof OnBackPressedListener) || !((OnBackPressedListener) fragment).onBackPressed()) {
			if (manager.getBackStackEntryCount() >= 1) {
				manager.popBackStack();
			} else if (manager.getBackStackEntryCount() < 1) {
				if (fragment instanceof FragmentGallery) {
					if (System.currentTimeMillis() - mLastBackButtonTouchTime > BACK_BUTTON_QUIT_THRESHOLD) {
						Toast.makeText(BaseActivity.this, R.string.back_button_notice, Toast.LENGTH_SHORT).show();
						mLastBackButtonTouchTime = System.currentTimeMillis();
					} else {
						finish();
					}
				} else {
					//clearBackStack();
					loadFragment(Fragments.GalleryFragment);
				}
			}
		}
	}

	@SuppressLint("NewApi")
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK &&
				(requestCode == MediaUtils.PICK_IMAGE ||
				requestCode == MediaUtils.CAMERA_CAPTURE
//                        ||
//				requestCode == MediaUtils.GALLERY_KITKAT_INTENT_CALLED ||
//				requestCode == MediaUtils.GALLERY_KITKAT_S4_INTENT_CALLED
                )) {
			MediaUtils.processActivityMediaResponse(this, requestCode, data);
		}
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent KEvent) {
		int index = getSupportFragmentManager().getBackStackEntryCount() - 1;
		if (index >=0 && getSupportFragmentManager().getBackStackEntryCount() >= 0) {
			FragmentManager.BackStackEntry backEntry = getSupportFragmentManager().getBackStackEntryAt(index);
			String tag = backEntry.getName();
			Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(tag);

			if (currentFragment != null && currentFragment instanceof OnDispatchKeyEventListener &&
					!(KEvent.getKeyCode() == KeyEvent.KEYCODE_BACK)) {
				return ((OnDispatchKeyEventListener) currentFragment).dispatchKeyEvent(KEvent);
			}
		}

		return super.dispatchKeyEvent(KEvent);
	}

	public void loadFragment(Fragments fragmentType) {
		loadFragment(fragmentType, AnimationType.IN);
	}

	public void loadFragment(Fragments fragmentType, AnimationType animationType) {
		Fragment f;
		switch (fragmentType) {
			case DesignerFragment:
				f = new FragmentDesigner();
				break;
			case PrintReadyFragment:
				f = new FragmentPrintReady();
				break;
			case PatternsFragment:
				f = new FragmentPatterns();
				break;
			case StickersFragment:
				f = new FragmentSticker();
				break;
			case PictureFromWebFragment:
				f = new FragmentPictureFromWeb();
				break;
			case LoginFragment:
				f = new FragmentLogin();
				break;
			case RegisterFragment:
				f = new FragmentRegister();
				break;
			case ForgotPasswordFragment:
				f = new FragmentForgotPassword();
				break;
			case PrintCompleteFragment:
				f = new FragmentPrintComplete();
				break;
			case AccountFragment:
				f = new FragmentAccount();
				break;
			case GalleryFragment:
			default:
				f = new FragmentGallery();
		}

		loadFragment(f, animationType);

		//manager.executePendingTransactions();
	}

	public void loadFragment(Fragment f) {
		loadFragment(f, AnimationType.IN);
	}

	public void loadFragment (final Fragment f, final AnimationType animationType) {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				FragmentManager manager = getSupportFragmentManager();
				FragmentTransaction txn = manager.beginTransaction();

				if (animationType == AnimationType.IN) {
					txn.setCustomAnimations(
							R.anim.fade_in,
							R.anim.fade_out,
							R.anim.fade_in,
							R.anim.fade_out
					);
				} else if (animationType == AnimationType.OUT) {
					txn.setCustomAnimations(
							R.anim.fade_in,
							R.anim.fade_out,
							R.anim.fade_in,
							R.anim.fade_out
					);
				}
				txn.replace(R.id.content_frame, f, f.getClass().getName());

				if (!(f instanceof FragmentGallery)) {
					txn.addToBackStack(f.getClass().getName());
				}
				txn.commit();

				manager.executePendingTransactions();
			}
		});
	}

	public void clearBackStack() {
		FragmentManager manager = getSupportFragmentManager();
		FragmentTransaction ft = manager.beginTransaction();
		manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		//ft.addToBackStack(null);
		ft.commit();
	}

	public void removeFragment() {
		FragmentManager manager = getSupportFragmentManager();

		if (manager.getBackStackEntryCount() > 1) {
			manager.popBackStack();
		}
	}

	public void refreshMenuAccount() {
		boolean isLoggedIn = UserManager.getInstance(this).isSignedIn();

		menuUserName.setVisibility(isLoggedIn ? View.VISIBLE : View.GONE);
		menuUserImage.setVisibility(isLoggedIn ? View.VISIBLE : View.GONE);

		if (isLoggedIn) {
			String name = UserManager.getInstance(this).getValue(UserManager.PARAM_FIRST_NAME);
			String imageUrl = String.format(RequestApi.URL_AVATARS, UserManager.getInstance(this).getValue(UserManager.PARAM_ID));

			menuUserName.setText(name.length() < 9 ? name : name.substring(0, 9) + "...");
			Picasso.with(this)
					.load(imageUrl)
					.into(menuUserImage);
		}

		if (UserManager.getInstance(this).isSignedIn()) {
			textLoginLogout.setText(R.string.my_account);
			imgLoginLogout.setBackgroundResource(R.drawable.menu_icon_logout6);
		} else {
			textLoginLogout.setText(R.string.login_register);
			imgLoginLogout.setBackgroundResource(R.drawable.menu_icon_login6);
		}
	}

	public void refreshAvatar() {
		String imageUrl = String.format(RequestApi.URL_AVATARS, UserManager.getInstance(this).getValue(UserManager.PARAM_ID));
		Picasso.with(this)
				.load(imageUrl)
				.memoryPolicy(MemoryPolicy.NO_CACHE)
				.networkPolicy(NetworkPolicy.NO_CACHE)
				.into(menuUserImage);
	}

	public void setProgressVisibility(boolean state) {
		setProgressVisibility(state, 0, R.string.please_wait);
	}

	public void setProgressVisibility(boolean state, int titleId, int msgStrId) {
		if (state) {
			if (mProgressDialog == null || !mProgressDialog.isShowing()) {

				String titleStr = "";
				if (titleId > 0) {
					titleStr = getString(titleId);
				}

				String msgStr = getString(R.string.please_wait);
				if (msgStrId > 0) {
					msgStr = getString(msgStrId);
				}

				mProgressDialog = ProgressDialog.show(this, titleStr, msgStr, true, false);
			}
		}
		else {
			if (mProgressDialog != null && mProgressDialog.isShowing()) {
				mProgressDialog.dismiss();
			}
		}
	}

	public void setTitle(String str) {
		TextView title = (TextView)findViewById(R.id.title);

		if (title != null && str != null && !str.isEmpty()) {
			title.setText(str);
			setTitleVisibility(true);
		}
	}

	public void setTitle(int resId) {
		TextView title = (TextView)findViewById(R.id.title);

		if (title != null) {
			if (resId > 0) {
				title.setText(getString(resId));
				setTitleVisibility(true);
			} else {
				title.setText("");
			}
		}
	}

	public void setTitleVisibility(boolean visible) {
		findViewById(R.id.title).setVisibility(visible ? View.VISIBLE : View.GONE);
	}

	public void setBackButtonVisibility(boolean visible) {
		linGoBack.setVisibility(visible ? View.VISIBLE : View.GONE);

		TextView title = (TextView)findViewById(R.id.title);
		if (visible && title.getVisibility() == View.GONE) {
			title.setVisibility(View.INVISIBLE);
		}
	}
}
