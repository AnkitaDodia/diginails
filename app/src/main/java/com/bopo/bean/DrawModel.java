package com.bopo.bean;

import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Path;

import com.bopo.enums.DrawingEventType;
import com.bopo.utils.drawing.MultiTouchEntity;

//Library Class
public class DrawModel {

	public DrawModel(DrawingEventType type) {
		this.type = type;
	}

	public DrawingEventType type; //all
	public Integer color; //all
	public Path drawPath; //DRAW
	public Integer brushSize; //DRAW
	public String textValue; //TEXT
	public Bitmap bitmap; //PATTERN, BITMAP_STATIC
	public MultiTouchEntity mte; //BITMAP_MOVABLE, TEXT
}
