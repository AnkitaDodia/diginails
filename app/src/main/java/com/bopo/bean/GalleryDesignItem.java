package com.bopo.bean;

import com.bopo.utils.managers.StringUtils;

/**
 * Created by admin on 3/8/2016.
 */
public class GalleryDesignItem {
    private int id;
    private String imgPath;
    private int downloads;
    private double designRating;
    private int userId;
    private String username;
    private boolean isPublic;

    public GalleryDesignItem(int id, String imgPath, int downloads, double designRating, int userId, String username, boolean isPublic) {
        this.id = id;
        this.imgPath = imgPath;
        this.downloads = downloads;
        this.designRating = designRating;
        this.userId = userId;
        this.username = username;
        this.isPublic = isPublic;
    }

    public int getId() {
        return id;
    }
    public String getImgPath() {
        return imgPath;
    }

    public String getPrintImgPath() {
        return StringUtils.getDesignPrintPath(imgPath);
    }

    public int getDownloads() {
        return downloads;
    }

    public double getDesignRating() {
        return designRating;
    }

    public void setDesignRating(int designRating) {
        this.designRating = designRating;
    }

    public int getUserId() { return userId; }

    public String getUsername() {
        return username;
    }

    public boolean isPublic() {
        return isPublic;
    }
}
