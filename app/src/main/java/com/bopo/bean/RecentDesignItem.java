package com.bopo.bean;

public class RecentDesignItem {
	public String recentDesignID;
	public String recentDesignUsersId;
	public String recentDesignPrintCode;
	public String recentDesignImagePath;
	public String recentDesignDeviceId;
	public String recentDesignDownloads;
	public String recentDesignKeywords;
	public String recentDesignDateCreated;
	public String recentDesignDesignRating;
	public String recentDesignUsername;

}
