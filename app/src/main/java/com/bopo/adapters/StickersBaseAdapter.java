package com.bopo.adapters;

import java.util.List;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bopo.R;
import com.bopo.bean.StickerItem;
import com.squareup.picasso.Picasso;

public class StickersBaseAdapter extends ArrayAdapter<StickerItem> {
	private Context mContext;
	private List<StickerItem> mItems;

	public StickersBaseAdapter(Context context, List<StickerItem> items) {
		super(context, R.layout.fragment_stickers_item, items);

		mContext = context;
		mItems = items;
	}

	@NonNull
	public View getView(int position, View convertView, @NonNull ViewGroup parent) {
		ProductViewHolder holder;

		if (convertView == null) {
			String inflater = Context.LAYOUT_INFLATER_SERVICE;
			LayoutInflater vi = (LayoutInflater) getContext().getSystemService(inflater);
			convertView = vi.inflate(R.layout.fragment_stickers_item, parent, false);

			holder = new ProductViewHolder();
            holder.container = (LinearLayout) convertView.findViewById(R.id.fragment_stickers_item_container);
			holder.imgSticker = (ImageView) convertView.findViewById(R.id.fragment_stickers_item_image);
			holder.imgStickerName = (TextView) convertView.findViewById(R.id.fragment_stickers_item_text);
			convertView.setTag(holder);
		} else {
			holder = (ProductViewHolder) convertView.getTag();
		}

		holder.imgStickerName.setText(mItems.get(position).name.trim());

		Picasso.with(mContext).load(mItems.get(position).stickerThumb).into(holder.imgSticker);

		return convertView;
	}

	private static class ProductViewHolder {
        public LinearLayout container;
		public ImageView imgSticker;
		public TextView imgStickerName;

	}
}