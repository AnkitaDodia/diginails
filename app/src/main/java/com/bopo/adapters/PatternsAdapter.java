package com.bopo.adapters;

import java.util.List;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.bopo.R;
import com.bopo.bean.PatternItem;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

public class PatternsAdapter extends ArrayAdapter<PatternItem> {
	private Context mContext;
	private List<PatternItem> mItems;

	public PatternsAdapter(Context context, List<PatternItem> items) {
		super(context, R.layout.fragment_patterns_item, items);

		mContext = context;
		mItems = items;
	}

	@NonNull
	public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
		final ViewHolder holder;

		if (convertView == null) {
			String inflater = Context.LAYOUT_INFLATER_SERVICE;
			LayoutInflater vi = (LayoutInflater) getContext().getSystemService(inflater);
			convertView = vi.inflate(R.layout.fragment_patterns_item, parent, false);

			holder = new ViewHolder();
			holder.pattern = (ImageView) convertView.findViewById(R.id.imgPatterns);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		Picasso.with(mContext)
				.load(mItems.get(position).patternsThumb)
				.networkPolicy(NetworkPolicy.NO_CACHE)
				.into(holder.pattern);

		return convertView;
	}

	private static class ViewHolder {
		ImageView pattern;
	}
}