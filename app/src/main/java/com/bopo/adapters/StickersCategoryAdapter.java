package com.bopo.adapters;

import java.util.List;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.bopo.R;
import com.bopo.bean.SubStickerItem;
import com.squareup.picasso.Picasso;

public class StickersCategoryAdapter extends ArrayAdapter<SubStickerItem> {
	private Context mContext;
	private List<SubStickerItem> mItems;

	public StickersCategoryAdapter(Context context, List<SubStickerItem> listSubStickerItem) {
		super(context, R.layout.fragment_stickers_item, listSubStickerItem);

		mContext = context;
		this.mItems = listSubStickerItem;
	}

	@NonNull
	public View getView(int position, View convertView, @NonNull ViewGroup parent) {
		ProductViewHolder holder;

		if (convertView == null) {
			String inflater = Context.LAYOUT_INFLATER_SERVICE;
			LayoutInflater vi = (LayoutInflater) getContext().getSystemService(inflater);
			convertView = vi.inflate(R.layout.fragment_stickers_sub_item, parent, false);

			holder = new ProductViewHolder();
			holder.imgSubSticker = (ImageView) convertView.findViewById(R.id.imgSubSticker);
			convertView.setTag(holder);
		} else {
			holder = (ProductViewHolder) convertView.getTag();
		}

		Picasso.with(mContext)
				.load(mItems.get(position).imgPath)
				.into(holder.imgSubSticker);

		return convertView;
	}

	private static class ProductViewHolder {
		ImageView imgSubSticker;
	}
}