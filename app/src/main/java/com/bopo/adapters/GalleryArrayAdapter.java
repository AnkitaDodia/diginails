package com.bopo.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bopo.R;
import com.bopo.activities.BaseActivity;
import com.bopo.bean.GalleryDesignItem;
import com.bopo.dialogs.DialogGalleryItem;
import com.bopo.fragments.FragmentBaseDesigns;
import com.bopo.fragments.FragmentGallery;
import com.bopo.utils.Consts;
import com.bopo.views.AnimationOnTouchListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by admin on 3/8/2016.
 */
public class GalleryArrayAdapter extends ArrayAdapter<GalleryDesignItem> {
    private List<GalleryDesignItem> mItems;
    private FragmentBaseDesigns mFragmentGalleryDesigns;
    private boolean mIsOwnDesign;

    private static final int layoutResId = R.layout.fragment_gallery_item;

    public GalleryArrayAdapter(BaseActivity context, FragmentBaseDesigns fragmentGalleryDesigns, List<GalleryDesignItem> items, boolean isOwnDesign) {
        super(context, layoutResId, items);

        mFragmentGalleryDesigns = fragmentGalleryDesigns;
        mItems = items;
        mIsOwnDesign = isOwnDesign;
    }

    @NonNull
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        ItemViewHolder mHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(getLayoutResId(), parent, false);

            mHolder = new ItemViewHolder();
            mHolder.rootView = (RelativeLayout)convertView;
            mHolder.itemImage = (ImageView) convertView.findViewById(R.id.gallery_item_image);
            mHolder.progressBar = (ProgressBar) convertView.findViewById(R.id.gallery_progress_bar);
            mHolder.itemDelete = (ImageView) convertView.findViewById(R.id.gallery_item_delete);
            mHolder.itemPublic = (ImageView) convertView.findViewById(R.id.gallery_item_public);


            if (mIsOwnDesign) {
                mHolder.itemDelete.setVisibility(View.VISIBLE);
                mHolder.itemDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mFragmentGalleryDesigns.onDesignDeleteClick(position);
                    }
                });

                if (mItems.get(position).isPublic()) {
                    mHolder.itemPublic.setVisibility(View.VISIBLE);
                    mHolder.itemPublic.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mFragmentGalleryDesigns.showPopup(getContext().getString(R.string.design_is_public));
                        }
                    });
                }
            }

            convertView.setTag(mHolder);
        } else {
            mHolder = (ItemViewHolder) convertView.getTag();
        }

        mHolder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogGalleryItem dialogGalleryItem = DialogGalleryItem.newInstance(mItems.get(position));
                dialogGalleryItem.show(mFragmentGalleryDesigns.getChildFragmentManager(), FragmentGallery.class.getName());
            }
        });

        mHolder.rootView.setOnTouchListener(new AnimationOnTouchListener(1.25f));

        String imagePath = Consts.ROOT_URL + "resize.php?img=" + mItems.get(position).getPrintImgPath() + "&width=200&h=360&zc=1";

        mHolder.progressBar.setVisibility(View.VISIBLE);
        Picasso.with(getContext())
                .load(imagePath)
                .error(R.drawable.no_image)
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .into(mHolder.itemImage, new CustomCallback(mHolder.progressBar));

        return convertView;
    }

    private int getLayoutResId() {
        return layoutResId;
    }

    private class ItemViewHolder {
        RelativeLayout rootView;
        ImageView itemImage, itemDelete, itemPublic;
        ProgressBar progressBar;
    }

    private class CustomCallback implements Callback {
        ProgressBar mProgressBar;

        public CustomCallback(ProgressBar progressBar) {
            mProgressBar = progressBar;
        }

        @Override
        public void onSuccess() {
            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onError() {
            mProgressBar.setVisibility(View.GONE);
        }
    }
}
