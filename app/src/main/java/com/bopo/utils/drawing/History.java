package com.bopo.utils.drawing;

import com.bopo.bean.DrawModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nick Yaro on 11/2/2017.
 */

//Library Class
public class History {
    private final Object mObject = new Object();
    private ArrayList<DrawModel> mItems = new ArrayList<DrawModel>();

    public void add(DrawModel obj) {
        synchronized (mObject) {
            mItems.add(obj);
        }
    }
    public void addAll(List<DrawModel> obj) {
        synchronized (mObject) {
            mItems.addAll(obj);
        }
    }
    public void set( int i, DrawModel d) {
        synchronized (mObject) {
            mItems.set(i, d);
        }
    }
    public DrawModel get(int i) {
        synchronized (mObject) {
            return mItems.get(i);
        }
    }
    public List<DrawModel> subList(int s, int e) {
        synchronized (mObject) {
            return mItems.subList(s, e);
        }
    }
    public void removeLast() {
        synchronized (mObject) {
            mItems.remove(mItems.size() - 1);
        }
    }
    public void clear() {
        synchronized (mObject) {
            mItems.clear();
        }
    }
    public int size() {
        synchronized (mObject) {
            return mItems.size();
        }
    }
}