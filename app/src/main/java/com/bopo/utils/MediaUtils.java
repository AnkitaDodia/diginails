package com.bopo.utils;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.bopo.utils.managers.DrawingManager;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Nick Yaro on 13/2/2017.
 */

public class MediaUtils {
//    public static final int GALLERY_IMAGE = 222;
//    public static final int GALLERY_KITKAT_INTENT_CALLED = 333;
//    public static final int GALLERY_KITKAT_S4_INTENT_CALLED = 444;
//    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;

    public static final int PICK_IMAGE = 201;
    public static final int CAMERA_CAPTURE = 202;

    private static final int MEDIA_TYPE_IMAGE = 1;
    private static final String IMAGE_DIRECTORY_NAME = "DigiNailStudio";

    public static String selectedImagePath, filemanagerstring;
    private static Uri selectedImageUri;
    public static String imageStoragePath;

    public static void openGallery(Activity context) {

        requestStoragePermission(context);

       /* if (Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.KITKAT) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            context.startActivityForResult(intent, GALLERY_IMAGE);

        } else {
            String manufactures = android.os.Build.MANUFACTURER;
            Logger.v("manufactures-->" + manufactures);

            // Galaxy s4 with kitkat
            if (manufactures.equalsIgnoreCase("samsung")) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                context.startActivityForResult(i, GALLERY_KITKAT_S4_INTENT_CALLED);
            }
            // other device with kitkat
            else {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                context.startActivityForResult(intent, GALLERY_KITKAT_INTENT_CALLED);
            }

        }*/
    }

    private static void requestStoragePermission(final Activity context) {

        Dexter.withActivity(context)
                .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // permission is granted
                        OpenGallery(context);
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            showSettingsDialog(context);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private static void OpenGallery(Activity context) {

        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        context.startActivityForResult(intent, PICK_IMAGE);
    }

    private static void showSettingsDialog(final Activity context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings(context);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    // navigating user to app settings
    private static void openSettings(Activity context) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", context.getPackageName(), null);
        intent.setData(uri);
        context.startActivityForResult(intent, 101);
    }

    public static void openCamera(Activity context) {
//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        Uri fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
//
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//        context.startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

        if (CameraUtils.checkPermissions(context)) {
//            layout_include.setVisibility(View.GONE);
            captureImage(context);
        } else {
            requestCameraPermission(context, CameraUtils.MEDIA_TYPE_IMAGE);
        }
    }

    /**
     * Requesting permissions using Dexter library
     */
    private static void requestCameraPermission(final Activity context, final int type) {
        Dexter.withActivity(context)
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            if (type == CameraUtils.MEDIA_TYPE_IMAGE) {
                                // capture picture
                                captureImage(context);
                            }

                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert(context);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    //********************* Coding for camera **************************//
    /**
     * Capturing Camera Image will launch camera app requested image capture
     */
    private static void captureImage(Activity context) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = CameraUtils.getOutputMediaFile(CameraUtils.MEDIA_TYPE_IMAGE);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }

        Uri fileUri = CameraUtils.getOutputMediaFileUri(context, file);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        context.startActivityForResult(intent, CAMERA_CAPTURE);
    }

    /**
     * Alert dialog to navigate to app settings
     * to enable necessary permissions
     */
    private static void showPermissionsAlert(final Activity context) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setTitle("Permissions required!")
                .setMessage("Camera needs few permissions to work properly. Grant them in settings.")
                .setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CameraUtils.openSettings(context);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }


    public static void processActivityMediaResponse(Activity activity, int requestCode, Intent data) {
        if (requestCode == PICK_IMAGE) {

            Log.e("PICK_IMAGE", "In MediaResponse : ");

                selectedImageUri = data.getData();
                String filePath = null;

                try {
                    // OI FILE Manager
                    filemanagerstring = selectedImageUri.getPath();

                    // MEDIA GALLERY
                    selectedImagePath = getPath(activity, selectedImageUri);
                    Log.e("selectedImagePath", "path : " + selectedImagePath);

                    if (selectedImagePath != null) {
                        filePath = selectedImagePath;
                    } else if (filemanagerstring != null) {
                        filePath = filemanagerstring;
                    } else {
                        Toast.makeText(activity, "Unknown path", Toast.LENGTH_LONG).show();
                        Log.e("Bitmap", "Unknown path");
                    }

                    if (filePath != null) {

                        DrawingManager.getInstance().tempBitmap = new DrawingManager.TempBitmap(lessResolution(filePath), true);

                    } else {
//                        bitmap = null;
                    }

                } catch (Exception e) {
                    Toast.makeText(activity, "Internal Error", Toast.LENGTH_LONG).show();
                    Log.e(e.getClass().getName(), e.getMessage(), e);
                }
//            }
        } else if(requestCode == CAMERA_CAPTURE){

//            if (requestCode == Activity.RESULT_OK) {
                // Refreshing the gallery
                try {

                    CameraUtils.refreshGallery(activity.getApplicationContext(), imageStoragePath);

                    selectedImagePath = imageStoragePath;

                    Log.e("selectedImagePath", "Camera Path : "+selectedImagePath);

                    if (selectedImagePath != null) {

                       /* bitmap = lessResolution(selectedImagePath, 200, 200);
                        img_profile_pic.setImageBitmap(bitmap);
                        BaseActivity.BmpCropped = bitmap;

                        Intent i = new Intent(mContext, CropActivity.class);
                        startActivityForResult(i, CROP_IMAGE);

                        BitmapUtils.createBitmap(selectedImagePath);*/
                        DrawingManager.getInstance().tempBitmap = new DrawingManager.TempBitmap(lessResolution(selectedImagePath), true);

                    } else {
//                        bitmap = null;
                    }

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
//            } else if (requestCode == Activity.RESULT_CANCELED) {
//                // user cancelled Image capture
//                Toast.makeText(activity.getApplicationContext(),
//                        "You cancelled image capture", Toast.LENGTH_SHORT)
//                        .show();
//            } else {
//                // failed to capture image
//                Toast.makeText(activity.getApplicationContext(),
//                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
//                        .show();
//            }
        }
//        else if (requestCode == GALLERY_IMAGE) {
//            Uri selectedImageUri = data.getData();
//            selectedImagePath = AppUtils.getPath(activity, selectedImageUri);
//            BitmapUtils.createBitmap(selectedImagePath);
//        } else if (requestCode == GALLERY_KITKAT_INTENT_CALLED) {
//            Uri originalUri = data.getData();
//            final int takeFlags = data.getFlags()
//                    & (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//            activity.getContentResolver().takePersistableUriPermission(originalUri, takeFlags);
//            String id = originalUri.getLastPathSegment().split(":")[1];
//            final String[] imageColumns = { MediaStore.Images.Media.DATA};
//            final String imageOrderBy = null;
//            Uri uri = AppUtils.getUri();
//            Cursor imageCursor = activity.getContentResolver().query(uri,
//                    imageColumns, MediaStore.Images.Media._ID + "=" + id,
//                    null, imageOrderBy);
//            if (imageCursor.moveToFirst()) {
//                selectedImagePath = imageCursor.getString(imageCursor
//                        .getColumnIndex(MediaStore.Images.Media.DATA));
//            }
//            BitmapUtils.createBitmap(selectedImagePath);
//        } else if (requestCode == GALLERY_KITKAT_S4_INTENT_CALLED) {
//            Uri selectedImageUri = data.getData();
//            selectedImagePath = AppUtils.getPath(activity, selectedImageUri);
//            BitmapUtils.createBitmap(selectedImagePath);
//        }
    }

    public static String getPath(Activity activity, Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = activity.managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    public static int calculateInSampleSize(BitmapFactory.Options options) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int reqWidth = 250, reqHeight = 250;
        int inSampleSize = 1;

        if (DrawingManager.getInstance().canvasHeight > 0) {
            float ratio = Math.min(
                    (float) DrawingManager.getInstance().canvasHeight / width,
                    (float) DrawingManager.getInstance().canvasHeight / height);
            reqWidth = Math.round((float) ratio * width);
            reqHeight = Math.round((float) ratio * height);
        }

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap lessResolution(String filePath) {

        BitmapFactory.Options options = new BitmapFactory.Options();

        // First decode with inJustDecodeBounds=true to check dimensions
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(filePath, options);
    }

    private static Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
            selectedImagePath = mediaFile.getAbsolutePath();
        } else {
            return null;
        }

        return mediaFile;
    }
}
