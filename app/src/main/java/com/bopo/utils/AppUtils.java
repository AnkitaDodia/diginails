package com.bopo.utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

public class AppUtils {
	public static final int IMAGE_WIDTH = 300;
	public static final int IMAGE_HEIGHT = 300;
	public static final int CROP_IMAGE = 2001;
	private static ProgressDialog mProgressDialog;

	public static void hideKeyboard(Context context, View view) {
		if (view != null) {
			InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}

	public static void showSimpleProgressDialog(Context context, String title,
			String msg, boolean isCancelable) {
		try {
			if (mProgressDialog == null) {
				mProgressDialog = ProgressDialog.show(context, title, msg);
				mProgressDialog.setCancelable(isCancelable);
			}
			if (!mProgressDialog.isShowing()) {
				mProgressDialog.show();
			}

		} catch (IllegalArgumentException ie) {
			ie.printStackTrace();
		} catch (RuntimeException re) {
			re.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void removeSimpleProgressDialog() {
		try {
			if (mProgressDialog != null) {
				if (mProgressDialog.isShowing()) {
					mProgressDialog.dismiss();
					mProgressDialog = null;
				}
			}
		} catch (IllegalArgumentException ie) {
			ie.printStackTrace();

		} catch (RuntimeException re) {
			re.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static boolean isGPSEnable(Context context) {
		LocationManager locMan = (LocationManager) context
				.getSystemService(Context.LOCATION_SERVICE);
		return locMan.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}

	public static boolean isLocationAccess(Context context) {
		LocationManager locMan = (LocationManager) context
				.getSystemService(Context.LOCATION_SERVICE);
		if (!locMan.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			if (locMan.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
				return true;
			else {
				return false;
			}
		}
		return true;
	}

	public static boolean isNetworkAvailable(Context activity) {
		ConnectivityManager connectivity = (ConnectivityManager) activity
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity == null) {
			return false;
		} else {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public static float convertScreenBased(Context context) {
		WindowManager manager = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = manager.getDefaultDisplay();
		Point point = new Point();
		display.getSize(point);

		// get width and height
		System.out.println(point.x);
		System.out.println(point.y);
		return 0;
	}

	public static float convertPixelToDp(float dp, Context context) {
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return px;
	}

	public static float convertDpToPixels(float px, Context context) {
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float dp = (metrics.densityDpi / 160f);
		dp = dp * 1.5f;
		return dp;
	}

	public static float dpToPx(int dp, Context context) {
		DisplayMetrics displayMetrics = context.getResources()
				.getDisplayMetrics();
		float px = Math.round(dp
				* (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
		return px;
	}

	public static int dpToPy(int dp, Context context) {
		DisplayMetrics displayMetrics = context.getResources()
				.getDisplayMetrics();
		int px = Math.round(dp
				* (displayMetrics.ydpi / DisplayMetrics.DENSITY_DEFAULT));
		return px;
	}

	public static float convertPixelsToDp(float px, Context context) {
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float dp = px / (metrics.densityDpi / 160f);
		return dp;
	}

	public static int pxToDp(int px, Context context) {
		DisplayMetrics displayMetrics = context.getResources()
				.getDisplayMetrics();
		int dp = Math.round(px
				/ (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
		return dp;
	}

	public static int pyToDp(int px, Context context) {
		DisplayMetrics displayMetrics = context.getResources()
				.getDisplayMetrics();
		int dp = Math.round(px
				/ (displayMetrics.ydpi / DisplayMetrics.DENSITY_DEFAULT));
		return dp;
	}

	public static String copyFile(Activity activity, String selectedImagePath) {
		String path = getDirectoryPath(activity);

		File dir = new File(path, "images");
		if (!dir.exists())
			dir.mkdirs();

		path = dir.getAbsolutePath();

		File file = new File(path, selectedImagePath.hashCode() + ".png");
		File source = new File(selectedImagePath);

		try {
			FileUtils.copyFile(source, file);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		if (file.isFile()) {
			// selectedImagePath = file.getAbsolutePath();
			Log.e("new path", file.getAbsolutePath());
			return file.getAbsolutePath();
		}

		Log.e("check", "unable to copy");
		return null;

	}

	public static String getDirectoryPath(Context context) {
		File dir;
		String appname = "AwareApp";
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			dir = new File(Environment.getExternalStorageDirectory(), appname);
			if (!dir.exists())
				dir.mkdir();
		} else {
			dir = context.getDir(appname, Context.MODE_PRIVATE);
		}
		return dir.getAbsolutePath();
	}

	public static String getPath(Activity activity, Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		ContentResolver cr = activity.getContentResolver();

		Cursor cursor = cr.query(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		if (cursor.moveToFirst())
			return cursor.getString(column_index);
		return "";
	}

	public static Uri getUri() {
		String state = Environment.getExternalStorageState();
		if (!state.equalsIgnoreCase(Environment.MEDIA_MOUNTED))
			return MediaStore.Images.Media.INTERNAL_CONTENT_URI;

		return MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
	}

	public static int getImageSampleSize(String selectedImagePath) {
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;

		BitmapFactory.decodeFile(selectedImagePath, o);

		int imageHeight = o.outHeight;
		int imageWidth = o.outWidth;

		int scale = 1;
		// Find the correct scale value. It should be the power of 2.
		final int REQUIRED_SIZE = 150;
		int width_tmp = imageWidth, height_tmp = imageHeight;
		while (true) {
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
			if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
				// if (width_tmp / 2 < IMAGE_WIDTH || height_tmp / 2 <
				// IMAGE_HEIGHT)
				break;
			}
		}

		return scale;
	}

	public static float convertFromDp(Context context, float input) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return ((float) input * scale);
	}

}
