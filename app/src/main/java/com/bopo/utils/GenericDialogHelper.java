package com.bopo.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;

import com.bopo.R;
import com.bopo.activities.BaseActivity;
import com.bopo.dialogs.GeneralDialog;
import com.bopo.dialogs.PaperOptionsDialog;
import com.bopo.dialogs.PrintAlignmentDialog;
import com.bopo.enums.Fragments;
import com.bopo.fragments.FragmentPrintReady;
import com.bopo.print.PrintUtil;
import com.bopo.utils.managers.DesignBitmapManager;

/**
 * Created by admin on 2017-01-28.
 */

public class GenericDialogHelper {
    public static void showPrintFromWeb(final FragmentActivity a) {
        SpannableString content;

        if (DesignBitmapManager.getInstance().getNailDesignId() > 0) {
            content = new SpannableString(
                    Html.fromHtml(
                            "Enter the URL into your PC web browser.<br><br>" +
                                    "<b>DigiNailStudio.com/print/" + DesignBitmapManager.getInstance().getNailDesignId() + "</b><br><br>" +
                                    "Tip: Alternatively, save your design from your device and access Recent Creations from your PC to print."
                    )
            );
            colorSpan(content, 0, 39, a.getResources().getColor(R.color.pink));
        } else {
            content = new SpannableString("You need to save your design in order to be able to print from web.");
        }

        final GeneralDialog generalDialog = GeneralDialog.newInstance(content);
        generalDialog.setOnPositiveClickListener("OK!", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generalDialog.dismiss();
            }
        });

        generalDialog.show(a.getSupportFragmentManager(), "web_print_dialog");
    }

    public static void showUnknownIssue(final FragmentActivity a) {
        SpannableString content = new SpannableString(
                Html.fromHtml(
                        "Can’t solve the problem?<br><br>" +
                                "<b>Please try printing from a printer connected PC</b><br><br>" +
                                "For help please check out the FAQ section at www.diginailstudio.com/faq<br><br>" +
                                "Alternatively, email your question to support@worxtoys.com</b>"
                )
        );

        final GeneralDialog generalDialog = GeneralDialog.newInstance(content);
        generalDialog.setOnPositiveClickListener("OK!", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generalDialog.dismiss();
            }
        });

        colorSpan(content, 26, 73, a.getResources().getColor(R.color.pink));
        content.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                generalDialog.dismiss();
                GenericDialogHelper.showPrintFromWeb(a);
            }
        }, 26, 73, 0);


        colorSpan(content, 120, 146, a.getResources().getColor(R.color.pink));
        content.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                String url = "http://www.diginailstudio.com/faq";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                a.startActivity(i);
            }
        }, 120, 146, 0);

        colorSpan(content, 185, 206, a.getResources().getColor(R.color.pink));
        content.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                String url = "mailto:support@worxtoys.com";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                a.startActivity(i);
            }
        }, 185, 206, 0);

        generalDialog.show(a.getSupportFragmentManager(), "unknown_issue");
    }

    private static void colorSpan(SpannableString span, int start, int end, int color) {
        span.setSpan(
                new ForegroundColorSpan(color),
                start, end, 0);
    }

    public static void showUnauthenticated(final FragmentActivity a) {
        SpannableString content = new SpannableString(
                Html.fromHtml(a.getResources().getString(R.string.login_message))
        );

        final GeneralDialog generalDialog = GeneralDialog.newInstance(content);
        generalDialog.setOnPositiveClickListener(a.getResources().getString(R.string.register), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generalDialog.dismiss();
                ((BaseActivity)a).loadFragment(Fragments.RegisterFragment);
            }
        });
        generalDialog.setOnNeutralClickListener(a.getResources().getString(R.string.login), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generalDialog.dismiss();
                ((BaseActivity)a).loadFragment(Fragments.LoginFragment);
            }
        });

        colorSpan(content, 7, 12, a.getResources().getColor(R.color.pink));
        content.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                generalDialog.dismiss();
                ((BaseActivity)a).loadFragment(Fragments.LoginFragment);
            }
        }, 7, 12, 0);

        colorSpan(content, 16, 24, a.getResources().getColor(R.color.pink));
        content.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                generalDialog.dismiss();
                ((BaseActivity)a).loadFragment(Fragments.RegisterFragment);
            }
        }, 16, 24, 0);

        generalDialog.show(a.getSupportFragmentManager(), "unknown_issue");
    }

    public static void showPaperOptionsFlow(final Context c, final FragmentManager fm, final Fragment targetFragment) {
        final SharedPreferences prefs = c.getSharedPreferences(Consts.PREFS_KEY, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = c.getSharedPreferences(Consts.PREFS_KEY, Context.MODE_PRIVATE).edit();

        final View.OnClickListener paperSizeOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int paperFormat = prefs.getInt(Consts.PARAM_PRINT_MODE, PrintUtil.PAGE_FORMAT_A5_GRID);
                boolean isGrid = paperFormat == PrintUtil.PAGE_FORMAT_A5_GRID || paperFormat == PrintUtil.PAGE_FORMAT_A4_GRID;
                boolean isA5 = false;

                if (v.getId() == R.id.dialog_paper_option_1) {
                    isA5 = true;
                    editor.putInt(
                            Consts.PARAM_PRINT_MODE,
                            isGrid ? PrintUtil.PAGE_FORMAT_A5_GRID : PrintUtil.PAGE_FORMAT_A5_NO_GRID
                    );
                } else if (v.getId() == R.id.dialog_paper_option_2) {
                    editor.putInt(
                            Consts.PARAM_PRINT_MODE,
                            isGrid ? PrintUtil.PAGE_FORMAT_A4_GRID : PrintUtil.PAGE_FORMAT_A4_NO_GRID
                    );
                }

                editor.commit();

                if (targetFragment != null && targetFragment instanceof FragmentPrintReady) {
                    ((FragmentPrintReady)targetFragment).onPrintParamsChanged();
                }

                GenericDialogHelper.showPrintInstructions(
                        c,
                        fm,
                        isA5
                );
            }
        };

        final View.OnClickListener paperTypeOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putInt(
                        Consts.PARAM_PRINT_MODE,
                        v.getId() == R.id.dialog_paper_option_1 ? PrintUtil.PAGE_FORMAT_A5_GRID : PrintUtil.PAGE_FORMAT_A5_NO_GRID
                );
                editor.commit();

                showPaperSizeDialog(c, fm, paperSizeOnClickListener);
            }
        };

        showPaperTypeDialog(c, fm, paperTypeOnClickListener);
    }

    public static void showPaperSizeDialog(final Context c, final FragmentManager fm, final View.OnClickListener onClickListener) {
        PaperOptionsDialog.newInstance(
                c,
                R.string.paper_size_options_title,
                R.drawable.print_mode_a5,
                R.drawable.print_mode_a4,
                onClickListener
        ).show(fm, PaperOptionsDialog.class.getName());
    }

    public static void showPaperTypeDialog(final Context c, final FragmentManager fm, final View.OnClickListener onClickListener) {
        PaperOptionsDialog.newInstance(
                c,
                R.string.please_select_from_2_options,
                R.drawable.paper_format_grid,
                R.drawable.paper_format_no_grid,
                onClickListener
        ).show(fm, PaperOptionsDialog.class.getName());
    }

    public static void showPrintInstructions(final Context c, final FragmentManager fm, final boolean a5Format) {
        Spannable spanText = new SpannableString(c.getResources().getString(a5Format ? R.string.print_help_a5 : R.string.print_help_a4));

        final GeneralDialog generalDialog = GeneralDialog.newInstance(c.getString(R.string.how_to_print), spanText);

        spanText.setSpan(
                new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        PrintAlignmentDialog.newInstance().show(fm, PrintAlignmentDialog.class.getName());
                        generalDialog.dismiss();
                    }
                }, spanText.length() - 31, spanText.length() - 1, 0);
        spanText.setSpan(
                new ForegroundColorSpan(
                        c.getResources().getColor(R.color.pink)),
                spanText.length() - 31, spanText.length() - 1, 0);
        if (a5Format) {
            spanText.setSpan(
                    new ForegroundColorSpan(
                            c.getResources().getColor(R.color.pink)),
                    0, 67, 0);
        }

        generalDialog.show(fm, "select_message");
    }
}
