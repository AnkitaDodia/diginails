package com.bopo.utils;

import java.io.Serializable;
import java.util.regex.Pattern;

public class Consts implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String M_GET = "GET";
	public static final String M_POST = "POST";
	public static final String M_PUT = "PUT";
	public static final String M_DELETE = "DELETE";
	public static final String M_POST_WITH_IMAGE = "POST_IMAGE";

	public static String ROOT_URL = "http://diginailstudio.com/";
	public static String API_URL = ROOT_URL + "dns-api/v1/";

	public static final String BOPO = "bopp";
	public static final String PREF_ACCOUNT = "account";

	public final static String PREFS_KEY = "prefs";
	public final static String PARAM_PRINT_HELP_DIALOG_SHOWN = "print_help_dialog_shown";
	public final static String PARAM_PRINT_MODE = "print_mode";
}