package com.bopo.utils;

import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;

/**
 * Created by Nick Yaro on 28/3/2016.
 */
public class AnimationUtils {
    public static void scaleView(View v, float startScale, float endScale, int duration) {
        scaleView(v, startScale,endScale, duration, new OvershootInterpolator()); //AccelerateInterpolator

    }
    public static void scaleView(View v, float startScale, float endScale, int duration, Interpolator interpolator) {
        Animation anim = new ScaleAnimation(
                startScale, endScale, // Start and end values for the X axis scaling
                startScale, endScale, // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 0.5f); // Pivot point of Y scaling
        anim.setFillAfter(true); // Needed to keep the result of the animation
        anim.setInterpolator(interpolator);
        anim.setDuration(duration);
        v.startAnimation(anim);
    }
}
