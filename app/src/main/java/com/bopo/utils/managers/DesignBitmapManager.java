package com.bopo.utils.managers;

import android.graphics.Bitmap;

/**
 * Created by Nick Yaro on 27/1/2017.
 */

public class DesignBitmapManager {
    private int mNailDesignId;
    private Bitmap mNailDesignBitmap;
    private boolean mIsPendingPrint;

    private static DesignBitmapManager mInstance;

    private DesignBitmapManager() {}

    public static DesignBitmapManager getInstance() {
        if(mInstance == null) {
            mInstance = new DesignBitmapManager();
        }

        return mInstance;
    }

    public int getNailDesignId() {
        return mNailDesignId;
    }

    public void setNailDesignId(int mNailDesignId) {
        this.mNailDesignId = mNailDesignId;
    }

    public Bitmap getNailDesignBitmap() {
        return mNailDesignBitmap;
    }

    public void setNailDesignBitmap(Bitmap mNailDesignBitmap) {
        this.mNailDesignBitmap = mNailDesignBitmap;
    }

    public boolean isPendingPrint() {
        return mIsPendingPrint;
    }

    public void setPendingPrint(boolean pendingPrint) {
        mIsPendingPrint = pendingPrint;
    }

    public void reset() {
        mInstance = new DesignBitmapManager();
    }
}
