package com.bopo.utils.managers;

import android.content.Context;
import android.content.SharedPreferences;

import com.bopo.utils.Consts;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

/**
 * Created by Nick Yaro on 13/2/2017.
 */

public class UserManager {
    private static UserManager mInstance;
    private UserManager() { }

    public static String PARAM_ID = "id";
    public static String PARAM_USERNAME = "username";
    public static String PARAM_TOKEN = "token";
    public static String PARAM_FIRST_NAME = "first_name";
    public static String PARAM_LAST_NAME = "last_name";
    public static String PARAM_EMAIL = "email";
    public static String PARAM_AGE = "age";
    public static String PARAM_REGISTER_DATE = "register_date";

    private WeakReference<Context> weakContext;

    public static UserManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new UserManager();
        }
        mInstance.weakContext = new WeakReference<>(context);
        return mInstance;
    }


    public void setUser(JSONObject object) throws JSONException {
        SharedPreferences.Editor prefsEditor = weakContext.get().getSharedPreferences(Consts.PREF_ACCOUNT, Context.MODE_PRIVATE).edit();

        prefsEditor.putString(PARAM_ID,             object.getString("id"));
        prefsEditor.putString(PARAM_USERNAME,       object.getString("username"));
        prefsEditor.putString(PARAM_TOKEN,          object.getString("token"));
        prefsEditor.putString(PARAM_FIRST_NAME,     object.getString("fname"));
        prefsEditor.putString(PARAM_LAST_NAME,      object.getString("lname"));
        prefsEditor.putString(PARAM_EMAIL,          object.getString("email"));
        prefsEditor.putString(PARAM_AGE,            object.getString("age"));
        prefsEditor.putString(PARAM_REGISTER_DATE,  object.getString("created_on"));
        prefsEditor.commit();
    }

    public void setValue(String param, String value) {
        SharedPreferences.Editor prefsEditor = weakContext.get().getSharedPreferences(Consts.PREF_ACCOUNT, Context.MODE_PRIVATE).edit();
        prefsEditor.putString(param, value);
        prefsEditor.commit();
    }

    public void logout() {
        SharedPreferences.Editor prefsEditor = weakContext.get().getSharedPreferences(Consts.PREF_ACCOUNT, Context.MODE_PRIVATE).edit();
        prefsEditor.clear();
        prefsEditor.commit();
    }

    public String getValue(String param) {
        SharedPreferences prefs = weakContext.get().getSharedPreferences(Consts.PREF_ACCOUNT, Context.MODE_PRIVATE);
        return prefs.getString(param, "");
    }

    public boolean isSignedIn() {
        return !getValue(PARAM_USERNAME).isEmpty() && !getValue(PARAM_TOKEN).isEmpty();
    }
}
