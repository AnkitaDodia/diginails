package com.bopo.utils.managers;

import android.graphics.Bitmap;

import com.bopo.utils.drawing.History;

/**
 * Created by Nick Yaro on 11/2/2017.
 */

public class DrawingManager {
    private static DrawingManager mInstance;
    private DrawingManager() { }

    public static int TOOL_BRUSH = 0;
    public static int TOOL_STICKER = 1;
    public static int TOOL_PATTERN = 2;
    public static int TOOL_PHOTO_CAMERA = 3;
    public static int TOOL_WEB = 4;
    public static int TOOL_TEXT = 5;

    public History history = new History();
    public int canvasHeight, canvasWidth;
    public int paintColor = 0xFFD878F0;
    public StringBuilder editText = new StringBuilder();
    public String fontName;
    public int currentTool = DrawingManager.TOOL_BRUSH;
    public TempBitmap tempBitmap;

    public static DrawingManager getInstance() {
        if (mInstance == null) {
            mInstance = new DrawingManager();
        }
        return mInstance;
    }

    public void reset() {
        mInstance = new DrawingManager();
    }

    public static class TempBitmap {
        public TempBitmap(Bitmap tempBitmap, boolean isPending) {
            this(tempBitmap, isPending, false);
        }

        public TempBitmap(Bitmap bitmap, boolean isPending, boolean isPattern) {
            this.bitmap = bitmap;
            this.isPending = isPending;
            this.isPattern = isPattern;
        }

        public Bitmap bitmap;
        public boolean isPending;
        public boolean isPattern;
    }
}
