package com.bopo.utils.managers;

/**
 * Created by Nick Yaro on 7/3/2017.
 */

public class StringUtils {
    public static String getDesignPrintPath(String str) {
        if (!str.contains("/print-")) {
            return str.replace("/uploads/", "/uploads/print-").replace("/designs/", "/designs/print-");
        }
        return str;
    }
}
