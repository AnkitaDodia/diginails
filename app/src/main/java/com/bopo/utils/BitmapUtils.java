package com.bopo.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;

import com.bopo.utils.managers.DrawingManager;

/**
 * Created by Nick Yaro on 3/3/2016.
 */
public class BitmapUtils {
    public static void createBitmap(String path) {
        Logger.e("path " + path);

        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = AppUtils.getImageSampleSize(path);

        Bitmap photo = BitmapFactory.decodeFile(path, opts);
        DrawingManager.getInstance().tempBitmap = new DrawingManager.TempBitmap(scale(photo), true);
    }

    public static Bitmap scale(Bitmap realImage) {
        int width = 250, height = 250;

        if (DrawingManager.getInstance().canvasHeight > 0) {
            float ratio = Math.min(
                    (float) DrawingManager.getInstance().canvasHeight / realImage.getWidth(),
                    (float) DrawingManager.getInstance().canvasHeight / realImage.getHeight());
            width = Math.round((float) ratio * realImage.getWidth());
            height = Math.round((float) ratio * realImage.getHeight());
        }
        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width, height,
                false);
        return newBitmap;
    }

    public static Bitmap drawTextToBitmap(Context c, String fontName, String text) {
        Bitmap bitmap = null;
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(DrawingManager.getInstance().paintColor);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.setTextSize(110);

        if (fontName != null && !fontName.isEmpty()) {
            Typeface fontFace = Typeface.createFromAsset(c.getAssets(), "fonts/" + fontName);
            paint.setTypeface(fontFace);

            if (text.length() <= 0) {
                bitmap = Bitmap.createBitmap(80, 180, Bitmap.Config.ARGB_8888);
            } else {
                bitmap = Bitmap.createBitmap(text.length() * 80, 180, Bitmap.Config.ARGB_8888);
            }
        }
        Canvas canvas = new Canvas(bitmap);

        // text shadow

        // draw text to the Canvas center
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        int x = (bitmap.getWidth() - bounds.width()) / 2;
        int y = (bitmap.getHeight() + bounds.height()) / 2;

        // Resources resources = context.getResources();
        // float scale = resources.getDisplayMetrics().density;
        // canvas.drawText(gText, x * scale, y * scale, paint);
        canvas.drawText(text, x, y, paint);
        return bitmap;
    }
}
