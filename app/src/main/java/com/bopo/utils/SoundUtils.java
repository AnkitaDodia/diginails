package com.bopo.utils;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.annotation.RawRes;

import com.bopo.R;

/**
 * Created by Nick Yaro on 21/2/2017.
 */

public class SoundUtils {
    public static final int SOUND_CLICK = R.raw.click;

    public static void playSound(Context context, @RawRes int sound) {
        final MediaPlayer mediaPlayer = MediaPlayer.create(context, sound);
        mediaPlayer.start();
    }
}
