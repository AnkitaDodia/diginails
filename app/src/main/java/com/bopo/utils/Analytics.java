package com.bopo.utils;

import com.bopo.BuildConfig;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

/**
 * Created by Nick Yaro on 2/2/2017.
 */

public class Analytics {
    public static final String ACTION_AUTH_SIGN_IN = "Sign-In";
    public static final String ACTION_AUTH_REGISTER = "Registration";
    public static final String ACTION_AUTH_PASSWORD_RESET = "Password Reset";
    public static final String ACTION_MENU_ORDER_ONLINE = "Order Online Link";
    public static final String ACTION_DESIGNER_TOOL = "Designer Tool";
    public static final String ACTION_PRINT_DEVICE = "Print from Device";
    public static final String ACTION_PRINT_WEB = "Print from Web";
    public static final String ACTION_PRINT_HELP = "Print Help";
    public static final String ACTION_PRINT_TROUBLE = "Print Troubleshooting";
    public static final String ACTION_PRINT_RESULT_CORRECT_YES = "Printed Correctly: Yes";
    public static final String ACTION_PRINT_RESULT_CORRECT_NO = "Printed Correctly: No";
    public static final String ACTION_ALIGNMENT_SAVE = "Alignment Settings Saved";
    public static final String ACTION_PAPER_SIZE_SAVE = "Paper Size Settings Saved";
    public static final String ACTION_SPLASH_SKIP = "Splash Video Skip";

    public static final String PARAM_DESIGN_TOOL = "Tool";
    public static final String PARAM_TROUBLE_ITEM = "Tool";
    public static final String PARAM_ALIGNMENT_VALUE = "Alignment Values";
    public static final String PARAM_PAPER_SIZE = "Paper Size";


    public static void log(String event) {
        if (!BuildConfig.DEBUG)
            Answers.getInstance().logCustom(new CustomEvent(event));
    }

    public static void log(String event, String key, String val) {
        if (!BuildConfig.DEBUG)
            Answers.getInstance().logCustom(new CustomEvent(event)
                  .putCustomAttribute(key, val));
    }


}
