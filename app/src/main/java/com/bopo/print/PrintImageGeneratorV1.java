package com.bopo.print;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;

import com.bopo.R;
import com.bopo.intefaces.OnImageGeneratedListener;

/**
 * Created by Nick Yaro on 2/7/2016.
 */
public class PrintImageGeneratorV1 extends PrintImageGenerator {

    public PrintImageGeneratorV1(Context context, Bitmap designBitmap) {
        super(context, designBitmap);
    }

    /*public static final boolean FILL_BACKGROUND_WHITE = true;

    public static final int TOTAL_ROWS = 8;
    public static final int TOTAL_COLUMNS = 10;

    public static final int IMAGE_DENSITY = 300;
    public static final int ROTATE_90_CLOCKWISE = 90;


    public static final int DESIGN_WIDTH = 220;
    public static final int DESIGN_MAX_HEIGHT = 210;

    final int[] colOffsets = new int[] { 83, 324, 564, 804, 1044, 1284, 1524, 1764, 2004, 2243 };
    final int[] rowOffsets = new int[] { 21, 150, 291, 441, 601, 770, 946, 1136 };
    final int[] designHeights = new int[] { 104, 117, 125, 136, 143, 151, 168, 210 };*/

    @Override
    public int getPrintHeight() {
        return 1386;
    }

    @Override
    public int getPrintWidth() {
        return 2547;
    }

    @Override
    public int getDensity() {
        return 300;
    }

    @Override
    public int[] getRowOffsets() {
        return new int[] { 21, 150, 291, 441, 601, 770, 946, 1136 };
    }

    @Override
    public int[] getColOffsets() {
        return new int[] { 83, 324, 564, 804, 1044, 1284, 1524, 1764, 2004, 2243 };
    }

    @Override
    public int[] getDesignHeights() {
        return new int[] { 104, 117, 125, 136, 143, 151, 168, 210 };
    }

    @Override
    public int getDesignWidth() {
        return 220;
    }

    @Override
    public int getDesignWidth(int col) {
        return getDesignWidth();
    }

    @Override
    public int getTotalRows() {
        return 8;
    }

    @Override
    public int getTotalCols() {
        return 10;
    }

    @Override
    public int getDesignMaxHeight() {
        return 210;
    }

    @Override
    public int getRotateDegreesClockwise() {
        return 90;
    }

    @Override
    public int getColWidthPercentage() {
        return 20;
    }

    @Override
    public int getMaskResId() {
        return R.drawable.print_mask1;
    }

    @Override
    public boolean fillBgWhite() {
        return true;
    }
    public boolean isColumnToBePrinted(int[] cols, int colIndex) {
        int colGroup = colIndex - (colIndex % 2); //remove remainder
        if (colGroup > 0) colGroup = colGroup / 2;

        for(int col : cols) {
            if (col == colGroup) {
                return true;
            }
        }
        return false;
    }
}
