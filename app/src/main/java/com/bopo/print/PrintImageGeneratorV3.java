package com.bopo.print;

import android.content.Context;
import android.graphics.Bitmap;

import com.bopo.R;

/**
 * Created by Nick Yaro on 12/4/2016.
 */
public class PrintImageGeneratorV3 extends PrintImageGenerator {

    public PrintImageGeneratorV3(Context context, Bitmap designBitmap) {
        super(context, designBitmap);
    }

    @Override
    public int getPrintHeight() {
        return 1741;
    }

    @Override
    public int getPrintWidth() {
        return 2491;
    }

    @Override
    public int getDensity() {
        return 300;
    }

    @Override
    public int[] getRowOffsets() {
        return new int[] { 40, 168, 298, 440, 588, 740, 906, 1074, 1258, 1453 };
    }

    @Override
    public int getRowOffset(int val) {
        return getRowOffsets()[val] - getSpaceFillRowOffset(val);
    }

    @Override
    public int[] getColOffsets() {
        return new int[] { 20, 260, 500, 740, 980, 1220, 1460, 1700, 1940, 2180 };
    }

    @Override
    public int getColOffset(int val) {
        return getColOffsets()[val] - getSpaceFillColOffset(val);
    }

    @Override
    public int[] getDesignHeights() {
        return new int[] {120, 120, 133, 133, 140, 149, 156, 165, 177, 220 }; //111, 119
    }
    @Override
    public int getDesignHeight(int val) {
        return getDesignHeights()[val] + getSpaceFillRowOffset(val) * 2;
    }

    @Override
    public int getDesignWidth() {
        return getDesignWidth(-1);
    }

    @Override
    public int getDesignWidth(int column) {
        return 230 + getSpaceFillColOffset(column) * 2;
    }

    @Override
    public int getTotalRows() {
        return 10;
    }

    @Override
    public int getTotalCols() {
        return 10;
    }

    @Override
    public int getDesignMaxHeight() {
        return 202;
    }

    @Override
    public int getRotateDegreesClockwise() {
        return 270;
    } //270

    @Override
    public int getColWidthPercentage() {
        return 10;
    }

    @Override
    public boolean fillBgWhite() {
        return true;
    }

    @Override
    public int getMaskResId() {
        return R.drawable.print_mask2;
    }

    @Override
    public int getDefaultColOffset() {
        return 20;
    }

    @Override
    public int getDefaultRowOffset() {
        return 35;
    }

    int getSpaceFillRowOffset(int val) {
        return val > 5 ? 10 : 7;
    }
    int getSpaceFillColOffset(int val) {
        return 7;
    }
}
