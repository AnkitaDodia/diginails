package com.bopo.print;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;

import com.bopo.R;
import com.bopo.intefaces.OnImageGeneratedListener;

/**
 * Created by Nick Yaro on 12/4/2016.
 */
public abstract class PrintImageGenerator {
    Context mContext;
    Bitmap mPrintBitmap, mDesignBitmap, mMask;
    Canvas mCanvas;
    Paint mPaint;
    final boolean mPreMask = true;

    public PrintImageGenerator(Context context, Bitmap designBitmap) {
        mContext = context;
        mPrintBitmap = Bitmap.createBitmap(getPrintWidth(), getPrintHeight(), Bitmap.Config.ARGB_8888);
        mPrintBitmap.setDensity(getDensity());

        mDesignBitmap = designBitmap;

        mDesignBitmap.setDensity(getDensity());
        mDesignBitmap = Bitmap.createScaledBitmap(rotateBitmap(mDesignBitmap, getRotateDegreesClockwise()), getDesignWidth(), getDesignMaxHeight(), true);

        mCanvas = new Canvas(mPrintBitmap);
        mPaint = new Paint();

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = 4; //  4 works good on 512MB (no art), 5 works good on 384MB (no art)

        mMask = BitmapFactory.decodeResource(mContext.getResources(), getMaskResId(), bitmapOptions);
        mMask.setDensity(getDensity());
        mMask = Bitmap.createScaledBitmap(mMask, getPrintWidth(), getPrintHeight(), true);
    }

    public Bitmap generate(int colNum) {
        return generate(new int[] {colNum});
    }

    public void generateAsync(final int[] colNums, final OnImageGeneratedListener onImageGeneratedListener) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                onImageGeneratedListener.onImageGenerated(generate(colNums));
            }
        };
        thread.start();
    }

    private Bitmap generate(int[] colNums) {
        if (fillBgWhite()) {
            mPaint.setColor(Color.WHITE);
            mCanvas.drawRect(new Rect(0, 0, mCanvas.getWidth(), mCanvas.getHeight()), mPaint);
        }

        if (mPreMask) {
            Paint mMaskPaint = new Paint();
            mMaskPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
            mCanvas.drawBitmap(mMask, 0, 0, mMaskPaint);
        }

        for (int row = 0; row < getTotalRows(); row++) {
            Bitmap designScaled = Bitmap.createScaledBitmap(mDesignBitmap, getDesignWidth(), getDesignHeight(row), true);
            for (int column = 0; column < getTotalCols(); column++) {
                if (isColumnToBePrinted(colNums, column)) {
                    mCanvas.drawBitmap(designScaled, getColOffset(column) + getDefaultColOffset(), getRowOffset(row) + getDefaultRowOffset(), mPaint);
                }
            }
        }

        if (!mPreMask) {
            Paint mMaskPaint = new Paint();
            mMaskPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
            mCanvas.drawBitmap(mMask, 0, 0, mMaskPaint);
        }

        return mPrintBitmap;
    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public boolean isColumnToBePrinted(int[] cols, int colGroup) {
        for(int col : cols) {
            if (col == colGroup) {
                return true;
            }
        }
        return false;
    }

    public int getRowOffset(int val) {
        return getRowOffsets()[val];
    }

    public int getColOffset(int val) {
        return getColOffsets()[val];
    }

    public int getDesignHeight(int val) {
        return getDesignHeights()[val];
    }

    public int getDefaultColOffset() {
        return 0;
    }
    public int getDefaultRowOffset() {
        return 0;
    }

    public abstract int getPrintHeight();
    public abstract int getPrintWidth();
    public abstract int getDensity();
    public abstract int[] getRowOffsets();
    public abstract int[] getColOffsets();
    public abstract int[] getDesignHeights();
    public abstract int getDesignWidth();
    public abstract int getDesignWidth(int col);
    public abstract int getTotalRows();
    public abstract int getTotalCols();
    public abstract int getDesignMaxHeight();
    public abstract int getRotateDegreesClockwise();
    public abstract int getColWidthPercentage();
    public abstract int getMaskResId();
    public abstract boolean fillBgWhite();
}
