package com.bopo.print;

        import android.content.Context;
        import android.graphics.Bitmap;

        import com.bopo.R;

/**
 * Created by Nick Yaro on 12/4/2016.
 */
public class PrintImageGeneratorV2 extends PrintImageGenerator {

    public PrintImageGeneratorV2(Context context, Bitmap designBitmap) {
        super(context, designBitmap);
    }

    @Override
    public int getPrintHeight() {
        return 1741;
    }

    @Override
    public int getPrintWidth() {
        return 2491;
    }

    @Override
    public int getDensity() {
        return 300;
    }

    @Override
    public int[] getRowOffsets() {
        return new int[] { 79, 207, 334, 472, 611, 759, 916, 1079, 1253, 1441 };
    }

    @Override
    public int[] getColOffsets() {
        return new int[] {80, 335, 567, 799, 1031, 1264, 1496, 1728, 1964, 2196 };
    }

    @Override
    public int[] getDesignHeights() {
        return new int[] {120, 120, 133, 133, 140, 149, 156, 165, 177, 220 }; //111, 119
    }

    @Override
    public int getDesignWidth() {
        return getDesignWidth(-1);
    }

    @Override
    public int getDesignWidth(int col) {
        return col == 0 ? 240 : 220;
    }

    @Override
    public int getTotalRows() {
        return 10;
    }

    @Override
    public int getTotalCols() {
        return 10;
    }

    @Override
    public int getDesignMaxHeight() {
        return 202;
    }

    @Override
    public int getRotateDegreesClockwise() {
        return 270;
    } //270

    @Override
    public int getColWidthPercentage() {
        return 10;
    }

    @Override
    public boolean fillBgWhite() {
        return true;
    }

    @Override
    public int getMaskResId() {
        return R.drawable.print_mask2;
    }
}
