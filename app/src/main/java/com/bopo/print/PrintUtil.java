package com.bopo.print;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.preference.PreferenceManager;
import android.print.PrintAttributes;
import android.print.PrintManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.bopo.dialogs.PrintAlignmentDialog;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.RectangleReadOnly;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Created by Nick Yaro on 2/7/2016.
 */
public class PrintUtil {
    public final static int PAGE_FORMAT_A5_GRID = 1;
    public final static int PAGE_FORMAT_A4_GRID = 2;
    public final static int PAGE_FORMAT_A4_8_ROW = 3;
    public final static int PAGE_FORMAT_A5_NO_GRID = 4;
    public final static int PAGE_FORMAT_A4_NO_GRID = 5;

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void print(Context context, File fileToPrint, String jobName, int pageFormat) {
        PrintManager printManager = (PrintManager) context.getSystemService(Context.PRINT_SERVICE);

        PrintAttributes.MediaSize mediaSize =
                (pageFormat == PAGE_FORMAT_A5_GRID || pageFormat == PAGE_FORMAT_A5_NO_GRID) ?
                        PrintAttributes.MediaSize.ISO_A5 : PrintAttributes.MediaSize.NA_LETTER;

        if (fileToPrint.exists()) {
            PrintAttributes.Builder builder = new PrintAttributes.Builder();
            builder.setMediaSize(mediaSize);

            printManager.print(jobName, new CustomPrintDocumentAdapter(fileToPrint), builder.build());
        }
    }


    public Document createPdf(Context context, File destinationFile, Bitmap bitmap, int pageFormat) {
        final int A5_WIDTH = 420;
        final int A5_HEIGHT = 595;

        switch (pageFormat) {
            case PAGE_FORMAT_A4_GRID:
            case PAGE_FORMAT_A4_NO_GRID:
                return createPdf(context, destinationFile, bitmap, (int)612.0F, (int)792.0F, A5_HEIGHT, A5_WIDTH);
            case PAGE_FORMAT_A5_GRID:
                return createPdf(context, destinationFile, PrintImageGenerator.rotateBitmap(bitmap, 90), A5_WIDTH, A5_HEIGHT, A5_WIDTH, A5_HEIGHT + 8);
            case PAGE_FORMAT_A5_NO_GRID:
                return createPdf(context, destinationFile, PrintImageGenerator.rotateBitmap(bitmap, 90), A5_WIDTH, A5_HEIGHT, A5_WIDTH, A5_HEIGHT);
            case PAGE_FORMAT_A4_8_ROW:
                return createPdf(context, destinationFile, bitmap, (int)612.0F, (int)792.0F, (int)612.0F, (int)340.0F);
            default:
                break;
        }
        return null;
    }

    public Document createPdf(Context context, File destinationFile, Bitmap bitmap,
                                     int pageWidth, int pageHeight, int imageWidth, int imageHeight) {
        final float oneMmToPostScript = 2.834645669f;
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        bitmap.recycle();

        byte[] byteArray = stream.toByteArray();

        try {
            destinationFile.createNewFile();

            Document document = new Document(new RectangleReadOnly(pageWidth, pageHeight), 0, 0, 5, 0);
            PdfWriter.getInstance(document, new FileOutputStream(destinationFile));

            document.open();
            Image image = Image.getInstance(byteArray);
            byteArray = null;

            int leftMM = sharedPreferences.getInt(PrintAlignmentDialog.PRINT_OFFSET_HORIZONTAL_MM, 0);
            int topMM = sharedPreferences.getInt(PrintAlignmentDialog.PRINT_OFFSET_VERTICAL_MM, 0);
            int leftStretchMM = sharedPreferences.getInt(PrintAlignmentDialog.PRINT_STRETCH_LEFT, 0);
            int rightStretchMM = sharedPreferences.getInt(PrintAlignmentDialog.PRINT_STRETCH_RIGHT, 0);

            int leftStretchPX = Math.round(oneMmToPostScript * leftStretchMM);
            int rightStretchPX = Math.round(oneMmToPostScript * rightStretchMM);
            int xPX = Math.round(oneMmToPostScript * leftMM) - leftStretchPX;
            int yPX = Math.round(oneMmToPostScript * topMM);

            image.scaleAbsoluteWidth(imageWidth + leftStretchPX + rightStretchPX);
            image.scaleAbsoluteHeight(imageHeight); //(image.getHeight() * (image.getScaledWidth() / image.getWidth()));

            image.setAbsolutePosition(xPX, pageHeight - image.getScaledHeight() - yPX);

            document.add(image);
            document.close();

            return document;
        } catch (DocumentException | IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
