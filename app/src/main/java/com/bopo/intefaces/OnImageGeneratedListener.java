package com.bopo.intefaces;

import android.graphics.Bitmap;

/**
 * Created by admin on 2/19/2016.
 */
public interface OnImageGeneratedListener {
    void onImageGenerated(Bitmap b);
}
