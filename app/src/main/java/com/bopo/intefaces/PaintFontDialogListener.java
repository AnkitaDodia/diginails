package com.bopo.intefaces;

public interface PaintFontDialogListener {

	public void onSelectFontCompleted(String fontName);
}
