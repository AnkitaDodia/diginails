package com.bopo.intefaces;

/**
 * Created by Nick Yaro on 25/4/2016.
 */
public interface OnDataLoadedListener {
    void onDataLoaded();
}
