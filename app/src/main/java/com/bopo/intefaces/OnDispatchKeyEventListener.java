package com.bopo.intefaces;

import android.view.KeyEvent;

/**
 * Created by Nick Yaro on 14/2/2017.
 */

public interface OnDispatchKeyEventListener {
    boolean dispatchKeyEvent(KeyEvent KEvent);
}
