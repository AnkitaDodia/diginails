package com.bopo.intefaces;

/**
 * Created by Nick Yaro on 3/3/2016.
 */
public interface OnBackPressedListener {
    boolean onBackPressed();
}
