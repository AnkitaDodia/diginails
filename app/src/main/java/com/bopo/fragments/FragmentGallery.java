package com.bopo.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bopo.R;
import com.bopo.intefaces.OnDataLoadedListener;
import com.bopo.views.AnimationOnTouchListener;
import com.bopo.enums.AnimationType;
import com.bopo.utils.managers.DesignBitmapManager;
import com.bopo.utils.managers.DrawingManager;
import com.bopo.enums.Fragments;
import com.bopo.views.materialshowcaseview.MaterialShowcaseSequence;
import com.bopo.views.materialshowcaseview.ShowcaseConfig;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

/**
 * Created by Nick Yaro on 8/3/2016.
 */
public class FragmentGallery extends BaseFragment {
    GalleryPagerAdapter mGalleryPagerAdapter;
    ViewPager mViewPager;
    ImageView mNewDesignLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_gallery, container, false);

        mGalleryPagerAdapter = new GalleryPagerAdapter(getChildFragmentManager());
        mViewPager = (ViewPager) rootView.findViewById(R.id.gallery_fragment_pager);
        mViewPager.setAdapter(mGalleryPagerAdapter);
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setCurrentItem(1);

        SmartTabLayout viewPagerTab = (SmartTabLayout)rootView.findViewById(R.id.gallery_fragment_pager_indicator);
        mViewPager.setAdapter(mGalleryPagerAdapter);
        viewPagerTab.setViewPager(mViewPager);

        mNewDesignLayout = (ImageView) rootView.findViewById(R.id.gallery_new_design_imageview);

        mNewDesignLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DesignBitmapManager.getInstance().reset();

                DrawingManager.getInstance().reset();
                getBaseActivity().loadFragment(Fragments.DesignerFragment, AnimationType.OUT);
            }
        });

        rootView.findViewById(R.id.gallery_new_design_imageview).setOnTouchListener(new AnimationOnTouchListener(0.9f));

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(500);

        mMaterialShowcaseSequence = new MaterialShowcaseSequence(getActivity(), "showcase_fragment_gallery");
        mMaterialShowcaseSequence.setConfig(config);

        mMaterialShowcaseSequence.addSequenceItem(getToolShowcaseView(getView().findViewById(R.id.gallery_hint_designs), getString(R.string.showcase_select_design)));
        mMaterialShowcaseSequence.addSequenceItem(getToolShowcaseView(getView().findViewById(R.id.gallery_hint_new_design), getString(R.string.showcase_create_design)));
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

        }
    }

    public class GalleryPagerAdapter extends FragmentStatePagerAdapter {
        public GalleryPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            FragmentGalleryDesigns fragment = FragmentGalleryDesigns.newInstance(i + 1);
            if (i == 1) {
                fragment.setOnDataLoadedListener(new OnDataLoadedListener() {
                    @Override
                    public void onDataLoaded() {
                        if (getActivity() == null) { return; }

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mViewPager.setCurrentItem(1);
                                if (mMaterialShowcaseSequence != null)
                                    mMaterialShowcaseSequence.start();
                            }
                        });
                    }
                });
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String tabName = "";
            switch (position) {
                case 0:
                    tabName = getBaseActivity().getString(R.string.most_downloaded);
                    break;
                case 1:
                    tabName = getBaseActivity().getString(R.string.highest_rated);
                    break;
                case 2:
                    tabName = getBaseActivity().getString(R.string.most_recent);
                    break;
            }
            return tabName;
        }
    }

    @Override
    public String getTitle() {
        return getString(R.string.user_design_gallery);
    }
}
