package com.bopo.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bopo.R;
import com.bopo.api.RequestApi;
import com.bopo.dialogs.AccountSettingsDialogFragment;
import com.bopo.dialogs.DialogAvatarUpload;
import com.bopo.enums.Fragments;
import com.bopo.utils.managers.UserManager;
import com.bopo.views.AnimationOnTouchListener;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

/**
 * Created by Nick Yaro on 31/8/2016.
 */
public class FragmentAccount extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_account,
                container, false);

        if (UserManager.getInstance(getActivity()).isSignedIn()) {
            String name = UserManager.getInstance(getActivity()).getValue(UserManager.PARAM_FIRST_NAME);

            ((TextView)rootView.findViewById(R.id.account_username)).setText(name.length() < 9 ? name : name.substring(0, 9) + "...");

            rootView.findViewById(R.id.account_avatar_container).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogAvatarUpload dialogAvatarUpload = DialogAvatarUpload.newInstance();
                    dialogAvatarUpload.setTargetFragment(FragmentAccount.this, -1);
                    dialogAvatarUpload.show(getFragmentManager(), DialogAvatarUpload.class.getName());
                }
            });
            rootView.findViewById(R.id.account_settings).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AccountSettingsDialogFragment.newInstance().show(getFragmentManager(), AccountSettingsDialogFragment.class.getName());
                }
            });
            rootView.findViewById(R.id.account_logout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserManager.getInstance(getActivity()).logout();
                    getBaseActivity().refreshMenuAccount();
                    getBaseActivity().loadFragment(Fragments.GalleryFragment);
                }
            });

            Picasso.with(getActivity())
                    .load(String.format(RequestApi.URL_AVATARS, UserManager.getInstance(getActivity()).getValue(UserManager.PARAM_ID)))
                    .into(((ImageView)rootView.findViewById(R.id.account_avatar)));

            rootView.findViewById(R.id.account_avatar_edit).setOnTouchListener(new AnimationOnTouchListener(0.9f));
            rootView.findViewById(R.id.account_settings).setOnTouchListener(new AnimationOnTouchListener(0.9f));
            rootView.findViewById(R.id.account_logout).setOnTouchListener(new AnimationOnTouchListener(0.9f));

        } else {
            getActivity().onBackPressed();
        }

        return rootView;
    }

    public void onAvatarUploaded() {
        Picasso.with(getActivity())
                .load(String.format(RequestApi.URL_AVATARS, UserManager.getInstance(getActivity()).getValue(UserManager.PARAM_ID)))
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(((ImageView)getView().findViewById(R.id.account_avatar)));

        getBaseActivity().refreshAvatar();
    }

    @Override
    public String getTitle() {
        return getString(R.string.my_account);
    }
}
