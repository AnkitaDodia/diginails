package com.bopo.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bopo.R;
import com.bopo.dialogs.TroubleshootDialogFragment;
import com.bopo.utils.Analytics;
import com.bopo.views.AnimationOnTouchListener;

/**
 * Created by admin on 3/25/2016.
 */
public class FragmentPrintComplete extends BaseFragment {
    public static FragmentPrintComplete newInstance() {
        FragmentPrintComplete fragmentPrintComplete = new FragmentPrintComplete();
        return fragmentPrintComplete;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_print_complete, container,
                false);
        getBaseActivity().setBackButtonVisibility(false);

        rootView.findViewById(R.id.rlBack).setOnTouchListener(new AnimationOnTouchListener(0.9f));
        rootView.findViewById(R.id.printResultYes).setOnTouchListener(new AnimationOnTouchListener(0.9f));
        rootView.findViewById(R.id.printResultNo).setOnTouchListener(new AnimationOnTouchListener(0.9f));

        rootView.findViewById(R.id.rlBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        rootView.findViewById(R.id.printResultYes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
                Analytics.log(Analytics.ACTION_PRINT_RESULT_CORRECT_YES);
            }
        });

        rootView.findViewById(R.id.printResultNo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TroubleshootDialogFragment.newInstance().show(getFragmentManager(), TroubleshootDialogFragment.class.getName());
                onBackPressed();
                Analytics.log(Analytics.ACTION_PRINT_RESULT_CORRECT_NO);
                getView().findViewById(R.id.printResultContainer).setVisibility(View.GONE);
            }
        });

        return rootView;
    }
}
