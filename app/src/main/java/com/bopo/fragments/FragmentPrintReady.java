package com.bopo.fragments;

import java.io.File;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bopo.R;
import com.bopo.activities.BaseActivity;
import com.bopo.dialogs.GeneralDialog;
import com.bopo.dialogs.PrintAlignmentDialog;
import com.bopo.dialogs.TroubleshootDialogFragment;
import com.bopo.intefaces.OnImageGeneratedListener;
import com.bopo.print.PrintImageGenerator;
import com.bopo.print.PrintImageGeneratorV1;
import com.bopo.print.PrintImageGeneratorV2;
import com.bopo.print.PrintImageGeneratorV3;
import com.bopo.print.PrintUtil;

import com.bopo.utils.Analytics;
import com.bopo.utils.Consts;
import com.bopo.utils.GenericDialogHelper;
import com.bopo.utils.managers.UserManager;
import com.bopo.views.AnimationOnTouchListener;
import com.bopo.utils.managers.DesignBitmapManager;


/**
 * Created by admin on 2/19/2016.
 */

public class FragmentPrintReady extends BaseFragment implements OnClickListener, OnImageGeneratedListener {
    View rootView;
    private ImageView printImageView;
    private RelativeLayout rlReadyToPrint, rlHelp, rlTroubleshooting;
    private Bitmap mScaledDownNailDesign;
    private Bitmap mGeneratedBitmap;
    private SharedPreferences mSharedPrefs;
    private PrintImageGenerator mPrintImageGenerator;

    private int printFormat;

    public static FragmentPrintReady newInstance() {
        FragmentPrintReady fragmentPrintReady = new FragmentPrintReady();
        return fragmentPrintReady;
    }

    private void findViews(View rootView) {
        rlHelp = (RelativeLayout) rootView.findViewById(R.id.rlHelp);
        rlReadyToPrint = (RelativeLayout) rootView.findViewById(R.id.rlReadyToPrint);
        rlTroubleshooting = (RelativeLayout) rootView.findViewById(R.id.rlTroubleshooting);

        printImageView = ((ImageView) rootView.findViewById(R.id.ivPrintImage));
    }

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        mScaledDownNailDesign = Bitmap.createScaledBitmap(DesignBitmapManager.getInstance().getNailDesignBitmap(), 220, 210, true);
        mSharedPrefs = getActivity().getSharedPreferences(Consts.PREFS_KEY, Context.MODE_PRIVATE);

        printFormat = mSharedPrefs.getInt(Consts.PARAM_PRINT_MODE, PrintUtil.PAGE_FORMAT_A5_GRID);

        if (!mSharedPrefs.getBoolean(Consts.PARAM_PRINT_HELP_DIALOG_SHOWN, false)) {
            GenericDialogHelper.showPrintInstructions(
                    getActivity(),
                    getFragmentManager(),
                    printFormat == PrintUtil.PAGE_FORMAT_A5_GRID ||  printFormat == PrintUtil.PAGE_FORMAT_A5_NO_GRID
            );
            mSharedPrefs.edit().putBoolean(Consts.PARAM_PRINT_HELP_DIALOG_SHOWN, true).apply();
        }

        DesignBitmapManager.getInstance().setPendingPrint(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_print_ready, container,
                false);
        getBaseActivity().setBackButtonVisibility(false);
        getBaseActivity().setTitleVisibility(false);

        findViews(rootView);

        rootView.findViewById(R.id.imgcol1btn).setOnClickListener(this);
        rootView.findViewById(R.id.imgcol2btn).setOnClickListener(this);
        rootView.findViewById(R.id.imgcol3btn).setOnClickListener(this);
        rootView.findViewById(R.id.imgcol4btn).setOnClickListener(this);
        rootView.findViewById(R.id.imgcol5btn).setOnClickListener(this);
        rootView.findViewById(R.id.imgcol6btn).setOnClickListener(this);
        rootView.findViewById(R.id.imgcol7btn).setOnClickListener(this);
        rootView.findViewById(R.id.imgcol8btn).setOnClickListener(this);
        rootView.findViewById(R.id.imgcol9btn).setOnClickListener(this);
        rootView.findViewById(R.id.imgcol10btn).setOnClickListener(this);

        rlHelp.setOnClickListener(this);
        rlReadyToPrint.setOnClickListener(this);
        rlTroubleshooting.setOnClickListener(this);

        rlHelp.setOnTouchListener(new AnimationOnTouchListener(0.9f));
        rlReadyToPrint.setOnTouchListener(new AnimationOnTouchListener(0.9f));
        rlTroubleshooting.setOnTouchListener(new AnimationOnTouchListener(0.9f));

        initPrinting();

        printImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    float percentage = (event.getX() / (float) v.getWidth()) * 100f;
                    int number = (int) Math.round(Math.ceil(percentage / mPrintImageGenerator.getColWidthPercentage()));
                    generateImage(number - 1);
                }
                return true;
            }
        });

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle b) {
        super.onActivityCreated(b);
        generateImage(0);
    }

    @SuppressLint("NewApi")
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgcol1btn:
                generateImage(0);
                break;

            case R.id.imgcol2btn:
                generateImage(1);
                break;

            case R.id.imgcol3btn:
                generateImage(2);
                break;

            case R.id.imgcol4btn:
                generateImage(3);
                break;

            case R.id.imgcol5btn:
                generateImage(4);
                break;

            case R.id.imgcol6btn:
                generateImage(5);
                break;

            case R.id.imgcol7btn:
                generateImage(6);
                break;

            case R.id.imgcol8btn:
                generateImage(7);
                break;

            case R.id.imgcol9btn:
                generateImage(8);
                break;

            case R.id.imgcol10btn:
                generateImage(9);
                break;

            case R.id.rlReadyToPrint:
                if (Build.VERSION.SDK_INT < 19) {
                    final GeneralDialog generalDialog = GeneralDialog.newInstance(
                            new SpannableString(getString(R.string.error_printing_not_supported))
                    );
                    generalDialog.show(getChildFragmentManager(), "format_dialog");
                } else {
                    final GeneralDialog generalDialog = GeneralDialog.newInstance(
                            new SpannableString(
                                    Html.fromHtml(
                                            String.format(
                                                    getString(R.string.reminder_paper_size),
                                                    printFormat == PrintUtil.PAGE_FORMAT_A5_GRID ? "A5" : "A4"
                                            )
                                    )
                            )
                    );
                    generalDialog.setOnPositiveClickListener(getString(R.string.ok), new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            generateAndPrint();
                        }
                    });

                    generalDialog.show(getChildFragmentManager(), "format_dialog");
                }
                break;

            case R.id.rlHelp:

                GenericDialogHelper.showPrintInstructions(
                        getActivity(),
                        getFragmentManager(),
                        printFormat == PrintUtil.PAGE_FORMAT_A5_GRID ||  printFormat == PrintUtil.PAGE_FORMAT_A5_NO_GRID
                );

                Analytics.log(Analytics.ACTION_PRINT_HELP);
                break;
            case R.id.rlTroubleshooting:
                TroubleshootDialogFragment troubleshootDialogFragment = TroubleshootDialogFragment.newInstance(this);
                troubleshootDialogFragment.setTargetFragment(FragmentPrintReady.this, 0);
                troubleshootDialogFragment.show(getFragmentManager(), TroubleshootDialogFragment.class.getName());
                break;
            default:
                break;

        }
    }

    protected void generateImage(final int row) {
        setProgressVisibility(true);
        printImageView.setImageResource(android.R.color.transparent);
        mGeneratedBitmap = null;

        mPrintImageGenerator.generateAsync(new int[]{row}, this);
    }

    protected void generateAndPrint() {
        Thread t = new Thread() {
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setProgressVisibility(true);
                    }
                });

                try {
                    if (mScaledDownNailDesign != null && mGeneratedBitmap != null) {
                        //Bitmap generatedImage = new PrintImageGeneratorV1(FragmentPrintReady.this.getActivity(), activity.app.printBitmap).generate(row, false);

                        File destinationDir = new File(
                                Environment
                                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                                        + "/Print");

                        if (!destinationDir.exists()) {
                            destinationDir.mkdirs();
                        }

                        long tim = System.currentTimeMillis();
                        String filename = "print-" + UserManager.getInstance(getActivity()).getValue(UserManager.PARAM_ID) + "_bopo-" + tim + ".pdf";

                        PrintUtil printUtil = new PrintUtil();
                        printUtil.createPdf(getActivity(), new File(destinationDir, filename), mGeneratedBitmap, printFormat);
                        printUtil.print(FragmentPrintReady.this.getActivity(), new File(destinationDir, filename), "jobname", printFormat);
                    }
                } catch (Exception e) {
                    //FIXME: not a good idea
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setProgressVisibility(false);

                        mGeneratedBitmap = null;
                        printImageView.setImageBitmap(null);

                        mPrintImageGenerator = null;

                        ((BaseActivity) getActivity()).loadFragment(FragmentPrintComplete.newInstance());
                    }
                });
            }
        };

        t.start();
    }

    @Override
    public void onImageGenerated(final Bitmap b) {
        mGeneratedBitmap = b;
        if (getView() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    printImageView.setImageBitmap(b);
                }
            });
        }
        setProgressVisibility(false);
    }

    public void onPrintParamsChanged() {
        int oldPrintFormat = printFormat;
        printFormat = mSharedPrefs.getInt(Consts.PARAM_PRINT_MODE, PrintUtil.PAGE_FORMAT_A5_GRID);

        if (oldPrintFormat != printFormat && (printFormat == PrintUtil.PAGE_FORMAT_A4_8_ROW || oldPrintFormat == PrintUtil.PAGE_FORMAT_A4_8_ROW)) {
            initPrinting();
            generateImage(0);
        }
    }



    public void initPrinting() {
        int visibility = View.VISIBLE;
        mPrintImageGenerator = null;

        if (printFormat == PrintUtil.PAGE_FORMAT_A4_8_ROW) {
            mPrintImageGenerator = new PrintImageGeneratorV1(FragmentPrintReady.this.getActivity(), DesignBitmapManager.getInstance().getNailDesignBitmap());
            visibility = View.GONE;
        } else if (printFormat == PrintUtil.PAGE_FORMAT_A5_NO_GRID) {
            mPrintImageGenerator = new PrintImageGeneratorV2(FragmentPrintReady.this.getActivity(), DesignBitmapManager.getInstance().getNailDesignBitmap());
        } else {
            mPrintImageGenerator = new PrintImageGeneratorV3(FragmentPrintReady.this.getActivity(), DesignBitmapManager.getInstance().getNailDesignBitmap());
        }

        rootView.findViewById(R.id.imgcol6btn).setVisibility(visibility);
        rootView.findViewById(R.id.imgcol7btn).setVisibility(visibility);
        rootView.findViewById(R.id.imgcol8btn).setVisibility(visibility);
        rootView.findViewById(R.id.imgcol9btn).setVisibility(visibility);
        rootView.findViewById(R.id.imgcol10btn).setVisibility(visibility);


    }
}
