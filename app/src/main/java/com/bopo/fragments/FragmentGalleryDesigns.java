package com.bopo.fragments;

import com.bopo.api.RequestApi;
import java.util.Locale;

/**
 * Created by Nick Yaro on 15/3/2016.
 */
public class FragmentGalleryDesigns extends FragmentBaseDesigns {
    public final static int TYPE_MOST_DOWNLOADED = 1;
    public final static int TYPE_HIGHEST_RATED = 2;
    public final static int TYPE_MOST_RECENT = 3;

    private int mPageType;

    public static FragmentGalleryDesigns newInstance(int pageType) {
        FragmentGalleryDesigns fragment = new FragmentGalleryDesigns();
        fragment.mPageType = pageType;
        return fragment;
    }

    @Override
    public String getUrl(int offset) {
        String url = String.format(Locale.CANADA, RequestApi.GET_DESIGNS_MOST_DOWNLOADED, offset);

        switch (mPageType) {
            case TYPE_MOST_DOWNLOADED:
                url = String.format(Locale.CANADA, RequestApi.GET_DESIGNS_MOST_DOWNLOADED, offset);
                break;
            case TYPE_HIGHEST_RATED:
                url = String.format(Locale.CANADA, RequestApi.GET_DESIGNS_HIGHEST_RATED, offset);
                break;
            case TYPE_MOST_RECENT:
                url = String.format(Locale.CANADA, RequestApi.GET_DESIGNS_MOST_RECENT, offset);
                break;
        }

        return url;
    }

    @Override
    public boolean isInfiniteScroll() {
        return true;
    }

}
