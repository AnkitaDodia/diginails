package com.bopo.fragments;

import java.io.IOException;

import org.json.JSONObject;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.bopo.R;
import com.bopo.api.RequestApi;
import com.bopo.utils.Analytics;
import com.bopo.utils.AppUtils;
import com.bopo.utils.Consts;
import com.bopo.utils.managers.DesignBitmapManager;
import com.bopo.enums.Fragments;
import com.bopo.utils.managers.UserManager;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Response;

public class FragmentRegister extends BaseFragment implements OnClickListener {
	private EditText edtUsername, edtPassword, edtFirstName, edtLastName, edtEmailAddress;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_register, container, false);

		TextView textPrivacyPolicy = (TextView) rootView.findViewById(R.id.textPrivacyPolicy);
		TextView textRegister = (TextView) rootView.findViewById(R.id.textRegister);
		edtUsername = (EditText) rootView.findViewById(R.id.edtUsername);
		edtPassword = (EditText) rootView.findViewById(R.id.edtPassword);
		edtFirstName = (EditText) rootView.findViewById(R.id.edtFirstName);
		edtLastName = (EditText) rootView.findViewById(R.id.edtLastName);
		edtEmailAddress = (EditText) rootView.findViewById(R.id.edtEmailAddres);

		textPrivacyPolicy.setOnClickListener(this);
		textRegister.setOnClickListener(this);
		return rootView;
	}

	public void checkUserName() {
		if (isValid()) {
			setProgressVisibility(true);
			RequestApi.getInstance(getActivity()).get(
					RequestApi.GET_REGISTER_CHECK_USERNAME + edtUsername.getText().toString(),
					usernameCheckCallback
			);
		}
	}

	public void requestRegistration() {
		if (isValid()) {
			RequestApi.getInstance(getActivity()).post(
					RequestApi.POST_REGISTER,
					new FormBody.Builder()
							.add("username", edtUsername.getText().toString())
							.add("pwd", edtPassword.getText().toString())
							.add("fname", edtFirstName.getText().toString())
							.add("lname", edtLastName.getText().toString())
							.add("email", edtEmailAddress.getText().toString())
							.add("age", "1")
							.add("policy_confirm", "1")
							.build(),
					registrationCallback
			);
			Analytics.log(Analytics.ACTION_AUTH_REGISTER);
		}
	}

	private boolean isValid() {
		if (edtUsername.getText().toString().trim().equals("")) {
			edtUsername.setError(getString(R.string.username_error));
			edtUsername.requestFocus();
			return false;
		} else if (edtUsername.getText().toString().contains("@") || edtUsername.getText().toString().contains(".com") || edtUsername.getText().toString().contains(".ca")) {
			edtUsername.setError(getString(R.string.username_error_email));
			edtUsername.requestFocus();
			return false;
		} else if (edtPassword.getText().toString().trim().equals("")) {
			edtPassword.setError(getString(R.string.password_blank_message));
			edtPassword.requestFocus();
			return false;
		} else if (edtFirstName.getText().toString().trim().equals("")) {
			edtFirstName.setError(getString(R.string.firstname_error));
			edtFirstName.requestFocus();
			return false;
		} else if (edtLastName.getText().toString().trim().equals("")) {
			edtLastName.setError(getString(R.string.lastname));
			edtLastName.requestFocus();
			return false;
		} else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(
				edtEmailAddress.getText().toString()).matches()) {
			edtEmailAddress.setError(getString(R.string.email_blank_message));
			edtEmailAddress.requestFocus();
			return false;
		}
		return true;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.textPrivacyPolicy:
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Consts.ROOT_URL + "coppa-policy"));
			startActivity(browserIntent);
			break;
		case R.id.textRegister:
			checkUserName();
			break;
		default:
			break;
		}
	}

	private Callback usernameCheckCallback = new Callback() {
		@Override
		public void onFailure(Call call, IOException e) {
			showPopup(getString(R.string.error_network_generic));
		}

		@Override
		public void onResponse(Call call, Response response) throws IOException {
			try {
				JSONObject object = new JSONObject(response.body().string());
				if (object.has("username") && object.getString("username").equals("available")) {
					requestRegistration();
				} else {
					showPopup("This username already exists!");
				}
			} catch (Exception e) {
				showPopup(getString(R.string.error_network_generic));
			}
		}
	};

	private Callback registrationCallback = new Callback() {
		@Override
		public void onFailure(Call call, IOException e) {
			showPopup(getString(R.string.error_network_generic));
		}

		@Override
		public void onResponse(Call call, Response response) throws IOException {
			setProgressVisibility(false);

			String responseBody = response.body().string();

			if (responseBody.isEmpty() || !response.isSuccessful()) {
				showPopup(getString(R.string.error_wrong_credientials));
				return;
			}

			try {
				UserManager.getInstance(getActivity()).setUser(new JSONObject(responseBody));
			} catch (Exception e) {
				showPopup(getString(R.string.error_network_generic));
				return;
			}

			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					getBaseActivity().refreshMenuAccount();
				}
			});
			AppUtils.hideKeyboard(getActivity(), getView());

			if (DesignBitmapManager.getInstance().isPendingPrint()) {
				getBaseActivity().onBackPressed();
			} else {
				getBaseActivity().loadFragment(Fragments.AccountFragment);
			}
		}
	};
}
