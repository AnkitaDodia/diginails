package com.bopo.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.bopo.activities.BaseActivity;
import com.bopo.R;
import com.bopo.dialogs.GeneralDialog;
import com.bopo.intefaces.OnBackPressedListener;
import com.bopo.views.materialshowcaseview.MaterialShowcaseSequence;
import com.bopo.views.materialshowcaseview.MaterialShowcaseView;

public class BaseFragment extends Fragment implements OnBackPressedListener {
	ProgressDialog mProgressDialog;
	MaterialShowcaseSequence mMaterialShowcaseSequence;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getBaseActivity().setTitle(getTitle());
	}

	protected BaseActivity getBaseActivity() {
		return (BaseActivity)getActivity();
	}

	public void setProgressVisibility(boolean state) {
		setProgressVisibility(state, 0, R.string.please_wait);
	}

	public void setProgressVisibility(boolean state, int titleId, int msgStrId) {
		if (state) {
			if (mProgressDialog == null || !mProgressDialog.isShowing()) {

				String titleStr = "";
				if (titleId > 0) {
					titleStr = getString(titleId);
				}

				String msgStr = getString(R.string.please_wait);
				if (msgStrId > 0) {
					msgStr = getString(msgStrId);
				}

				/*mProgressDialog = new ProgressDialog(this.getActivity());
				mProgressDialog.setTitle(titleStr);
				mProgressDialog.setMessage(msgStr);
				mProgressDialog.setIndeterminate(true);
				mProgressDialog.setCancelable(false);
				mProgressDialog.show();
				mProgressDialog.setContentView(R.layout.progress_dialog);*/

				mProgressDialog = ProgressDialog.show(this.getActivity(), titleStr, msgStr, true, false);
			}
		}
		else {
			if (mProgressDialog != null && mProgressDialog.isShowing()) {
				mProgressDialog.dismiss();
			}
		}
	}

	@Override
	public boolean onBackPressed() {
		if (mMaterialShowcaseSequence != null) {
			mMaterialShowcaseSequence.stop();
		}
		mMaterialShowcaseSequence = null;
		return false;
	}

	protected MaterialShowcaseView getToolShowcaseView(View v, String content) {
		return new MaterialShowcaseView.Builder(getActivity())
				.setTarget(v)
				.setContentText(content)
				.setDismissOnTouch(true)
				.setDismissOnTargetTouch(true)
				.setDismissText(getString(R.string.touch_anywhere_to_continue))
				.setDismissTextColor(getResources().getColor(R.color.grey_light))
				.build();
	}

	public void showPopup(final String message) {
		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				setProgressVisibility(false);
				final GeneralDialog generalDialog = GeneralDialog.newInstance(message);
				generalDialog.show(getBaseActivity().getSupportFragmentManager(), GeneralDialog.class.getName());
			}
		});
	}

	public String getTitle() {
		return null;
	}

}
