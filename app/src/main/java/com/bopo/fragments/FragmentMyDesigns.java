package com.bopo.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;

import com.bopo.R;
import com.bopo.api.RequestApi;
import com.bopo.dialogs.GeneralDialog;
import com.bopo.utils.GenericDialogHelper;
import com.bopo.utils.managers.UserManager;

import java.io.IOException;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by Nick Yaro on 3/3/2017.
 */

public class FragmentMyDesigns extends FragmentBaseDesigns {

    public static FragmentMyDesigns newInstance() {
        FragmentMyDesigns fragment = new FragmentMyDesigns();
        return fragment;
    }

    @Override
    public void populateOrLoadData() {
        if (!UserManager.getInstance(getActivity()).isSignedIn()) {
            GenericDialogHelper.showUnauthenticated(getBaseActivity());
        } else {
            super.populateOrLoadData();
        }
    }

    @Override
    public String getUrl(int offset) {
        return String.format(
                Locale.CANADA,
                RequestApi.GET_USER_DESIGNS,
                UserManager.getInstance(getActivity()).getValue(UserManager.PARAM_ID)
        );
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_recent_creations);
    }

    @Override
    public boolean isInfiniteScroll() {
        return false;
    }

    @Override
    public boolean isOwnDesigns() {
        return true;
    }

    @Override
    public void onDesignDeleteClick(final int position) {
        final GeneralDialog generalDialog = GeneralDialog.newInstance(getString(R.string.are_you_sure_want_to_delete_design));
        generalDialog.setOnPositiveClickListener(getString(R.string.yes), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDesign(position);
            }
        });
        generalDialog.setOnNeutralClickListener(getString(R.string.no), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        generalDialog.show(getFragmentManager(), GeneralDialog.class.getName());
    }

    public void deleteDesign(final int position) {
        setProgressVisibility(true);

        RequestApi.getInstance(getActivity()).delete(
                String.format(RequestApi.DELETE_USER_DESIGNS, mItems.get(position).getId()),
                new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        showPopup(getString(R.string.error_network_generic));
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        setProgressVisibility(false);
                        String responseBody = response.body().string();

                        if (responseBody.isEmpty() || !response.isSuccessful()) {
                            showPopup(getString(R.string.error_network_generic));
                            return;
                        }

                        String errorStr = RequestApi.getError(responseBody);
                        if (!errorStr.isEmpty()) {
                            showPopup(errorStr);
                            return;
                        }

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mItems.remove(position);
                                mAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                }
        );
    }
}
