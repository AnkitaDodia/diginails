package com.bopo.fragments;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.SpannableString;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bopo.api.RequestApi;
import com.bopo.dialogs.GeneralDialog;
import com.bopo.dialogs.SelectBrushSizeDialog;
import com.bopo.intefaces.OnBackPressedListener;
import com.bopo.intefaces.OnDispatchKeyEventListener;
import com.bopo.utils.Analytics;
import com.bopo.utils.GenericDialogHelper;
import com.bopo.utils.Logger;
import com.bopo.utils.MediaUtils;
import com.bopo.utils.SoundUtils;
import com.bopo.utils.managers.DrawingManager;
import com.bopo.utils.managers.UserManager;
import com.bopo.views.AnimationOnTouchListener;
import com.bopo.utils.BitmapUtils;
import com.bopo.utils.managers.DesignBitmapManager;
import com.bopo.enums.DrawingEventType;
import com.bopo.enums.Fragments;
import com.bopo.activities.BaseActivity;
import com.bopo.intefaces.PaintFontDialogListener;
import com.bopo.R;
import com.bopo.dialogs.ChooseFontDialog;
import com.bopo.dialogs.PrintOptionsDialog;
import com.bopo.views.DrawingView;
import com.bopo.views.materialshowcaseview.MaterialShowcaseSequence;
import com.bopo.views.materialshowcaseview.ShowcaseConfig;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class FragmentDesigner extends BaseFragment implements
        OnClickListener, PaintFontDialogListener, OnBackPressedListener, OnDispatchKeyEventListener {
    private ImageButton selectedTool;
    private LinearLayout linSave, linUndo, paint_colors1, paint_colors2;
    private RelativeLayout frmScreenShot;
    private int lastSaveListSize;
    private ImageButton currPaint;
    private DrawingView drawView;
    
    private int lastTempBitmapHashCode;

    public static FragmentDesigner newInstance() {
        FragmentDesigner fragmentDesigner = new FragmentDesigner();
        return fragmentDesigner;
    }

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_designer, container, false);
        drawView = (DrawingView) rootView.findViewById(R.id.imgDraw);
        linSave = (LinearLayout) rootView.findViewById(R.id.linSave);
        linUndo = (LinearLayout) rootView.findViewById(R.id.linUndo);
        frmScreenShot = (RelativeLayout) rootView
                .findViewById(R.id.frmScreenShot);

        selectedTool = (ImageButton) rootView.findViewById(R.id.tool_brush);

        getBaseActivity().setBackButtonVisibility(false);

        paint_colors1 = (LinearLayout) rootView
                .findViewById(R.id.paint_colors1);
        paint_colors2 = (LinearLayout) rootView
                .findViewById(R.id.paint_colors2);

        currPaint = (ImageButton) paint_colors1.getChildAt(0);
        currPaint.setImageDrawable(getResources().getDrawable(R.drawable.paint_pressed));

        lastSaveListSize = DrawingManager.getInstance().history.size();


        OnClickListener paintOnClickListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                SoundUtils.playSound(getActivity(), SoundUtils.SOUND_CLICK);

                if (v instanceof ImageButton) {
                    if (v.getId() != R.id.tool_color) {
                        if (selectedTool != null) {
                            selectedTool.setImageDrawable(null);
                        }
                        selectedTool = (ImageButton) v;
                        selectedTool.setImageDrawable(getResources().getDrawable(R.drawable.paint_pressed));
                    }

                    switch (v.getId()) {
                        case R.id.tool_brush:
                            DrawingManager.getInstance().currentTool = DrawingManager.TOOL_BRUSH;

                            FragmentManager fm = getBaseActivity().getSupportFragmentManager();
                            SelectBrushSizeDialog selectBrushSizeDialog = SelectBrushSizeDialog.newInstance(FragmentDesigner.this, drawView);
                            selectBrushSizeDialog.show(fm, SelectBrushSizeDialog.class.getName());
                            break;
                        case R.id.tool_fill:
                            //TODO: implement
                            break;
                        case R.id.tool_text:
                            DrawingManager.getInstance().currentTool = DrawingManager.TOOL_TEXT;
                            setTextFont();
                            break;
                        case R.id.tool_stickers:
                            DrawingManager.getInstance().currentTool = DrawingManager.TOOL_STICKER;
                            ((BaseActivity) getActivity()).loadFragment(Fragments.StickersFragment);
                            break;
                        case R.id.tool_color:
                            openColorPicker();
                            break;
                        case R.id.tool_stamp:
                            //TODO: implement
                            break;
                        case R.id.tool_patterns:
                            DrawingManager.getInstance().currentTool = DrawingManager.TOOL_PATTERN;
                            ((BaseActivity) getActivity()).loadFragment(FragmentPatterns.newInstance(FragmentDesigner.this));
                            break;
                        case R.id.tool_photo:
                            DrawingManager.getInstance().currentTool = DrawingManager.TOOL_PHOTO_CAMERA;
                            MediaUtils.openGallery(getBaseActivity());
                            break;
                        case R.id.tool_web:
                            DrawingManager.getInstance().currentTool = DrawingManager.TOOL_WEB;
                            ((BaseActivity) getActivity()).loadFragment(Fragments.PictureFromWebFragment);
                            break;
                        case R.id.tool_camera:
                            DrawingManager.getInstance().currentTool = DrawingManager.TOOL_PHOTO_CAMERA;
                            MediaUtils.openCamera(getBaseActivity());
                            break;
                    }

                    Analytics.log(Analytics.ACTION_DESIGNER_TOOL, Analytics.PARAM_DESIGN_TOOL, v.getContentDescription().toString());
                }
            }
        };


        rootView.findViewById(R.id.tool_brush).setOnClickListener(paintOnClickListener);
        rootView.findViewById(R.id.tool_fill).setOnClickListener(paintOnClickListener);
        rootView.findViewById(R.id.tool_text).setOnClickListener(paintOnClickListener);
        rootView.findViewById(R.id.tool_stickers).setOnClickListener(paintOnClickListener);
        rootView.findViewById(R.id.tool_color).setOnClickListener(paintOnClickListener);

        rootView.findViewById(R.id.tool_stamp).setOnClickListener(paintOnClickListener);
        rootView.findViewById(R.id.tool_patterns).setOnClickListener(paintOnClickListener);
        rootView.findViewById(R.id.tool_photo).setOnClickListener(paintOnClickListener);
        rootView.findViewById(R.id.tool_web).setOnClickListener(paintOnClickListener);
        rootView.findViewById(R.id.tool_camera).setOnClickListener(paintOnClickListener);

        float animationFactor = 0.75f;

        rootView.findViewById(R.id.tool_brush).setOnTouchListener(new AnimationOnTouchListener(animationFactor));
        rootView.findViewById(R.id.tool_fill).setOnTouchListener(new AnimationOnTouchListener(animationFactor));
        rootView.findViewById(R.id.tool_text).setOnTouchListener(new AnimationOnTouchListener(animationFactor));
        rootView.findViewById(R.id.tool_stickers).setOnTouchListener(new AnimationOnTouchListener(animationFactor));
        rootView.findViewById(R.id.tool_color).setOnTouchListener(new AnimationOnTouchListener(animationFactor));

        rootView.findViewById(R.id.tool_stamp).setOnTouchListener(new AnimationOnTouchListener(animationFactor));
        rootView.findViewById(R.id.tool_patterns).setOnTouchListener(new AnimationOnTouchListener(animationFactor));
        rootView.findViewById(R.id.tool_photo).setOnTouchListener(new AnimationOnTouchListener(animationFactor));
        rootView.findViewById(R.id.tool_web).setOnTouchListener(new AnimationOnTouchListener(animationFactor));
        rootView.findViewById(R.id.tool_camera).setOnTouchListener(new AnimationOnTouchListener(animationFactor));

        linSave.setOnTouchListener(new AnimationOnTouchListener(1.25f));
        linUndo.setOnTouchListener(new AnimationOnTouchListener(1.25f));

        linSave.setOnClickListener(this);
        linUndo.setOnClickListener(this);

        //Bitmap mask1 = BitmapFactory.decodeResource(getResources(), R.drawable.nail_tran);

        drawView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        getBaseActivity().setBackButtonVisibility(false);

        int drawingHeight = Math.round(getResources().getDimension(R.dimen.paint_canvas_image_height) * 0.885f);
        int drawingWidth = Math.round(getResources().getDimension(R.dimen.paint_canvas_image_width) * 0.78f);

        DrawingManager.getInstance().canvasHeight = drawingHeight;
        DrawingManager.getInstance().canvasWidth = drawingWidth;

        drawView.getLayoutParams().height = drawingHeight;
        drawView.getLayoutParams().width = drawingWidth;


        getBaseActivity().setBackButtonVisibility(false);

        applyPendingImage();

        if (DrawingManager.getInstance().currentTool == DrawingManager.TOOL_BRUSH) {
            if (DesignBitmapManager.getInstance().getNailDesignBitmap() != null) {
                DesignBitmapManager.getInstance().setNailDesignBitmap(
                        BitmapUtils.scale(DesignBitmapManager.getInstance().getNailDesignBitmap())
                );
                drawView.addStaticBitmap(DesignBitmapManager.getInstance().getNailDesignBitmap());
            }
        }

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(500);

        mMaterialShowcaseSequence = new MaterialShowcaseSequence(getActivity(), "showcase_fragment_paint_10");
        mMaterialShowcaseSequence.setConfig(config);

        mMaterialShowcaseSequence.addSequenceItem(getToolShowcaseView(getView().findViewById(R.id.paint_colors1), getString(R.string.showcase_paint)));
        mMaterialShowcaseSequence.addSequenceItem(getToolShowcaseView(getView().findViewById(R.id.tool_stickers), getString(R.string.showcase_stickers)));
        mMaterialShowcaseSequence.addSequenceItem(getToolShowcaseView(getView().findViewById(R.id.tool_photo), getString(R.string.showcase_photo)));
        mMaterialShowcaseSequence.addSequenceItem(getToolShowcaseView(getView().findViewById(R.id.tool_web), getString(R.string.showcase_web)));
        mMaterialShowcaseSequence.addSequenceItem(getToolShowcaseView(getView().findViewById(R.id.tool_color), getString(R.string.showcase_color)));
        mMaterialShowcaseSequence.addSequenceItem(getToolShowcaseView(getView().findViewById(R.id.linUndo), getString(R.string.showcase_undo)));
        mMaterialShowcaseSequence.addSequenceItem(getToolShowcaseView(getView().findViewById(R.id.linSave), getString(R.string.showcase_save)));

        mMaterialShowcaseSequence.start();
    }

    public void setTextFont() {
        if (DrawingManager.getInstance().currentTool == DrawingManager.TOOL_TEXT) {
            Logger.e("");
            FragmentManager fm = getChildFragmentManager();
            ChooseFontDialog selecteMessageDialog = ChooseFontDialog.newInstance(this);
            selecteMessageDialog.show(fm, "select_message");
            paint_colors1.setVisibility(View.VISIBLE);
            paint_colors2.setVisibility(View.VISIBLE);
        }

    }


    @Override
    public void onResume() {
        super.onResume();

        applyPendingImage();

        if (UserManager.getInstance(getActivity()).isSignedIn() &&
                        DesignBitmapManager.getInstance().isPendingPrint()
                ) {
            promptIsDesignPublicAndSave();
            DesignBitmapManager.getInstance().setPendingPrint(false);
        }

        getBaseActivity().findViewById(R.id.title).setVisibility(View.GONE);

        updateColorHint();
    }

    @Override
    public void onPause() {
        super.onPause();
        getBaseActivity().findViewById(R.id.title).setVisibility(View.VISIBLE);
    }

    public void applyPendingImage() {
        if (
                DrawingManager.getInstance().tempBitmap != null &&
                DrawingManager.getInstance().tempBitmap.bitmap != null &&
                DrawingManager.getInstance().tempBitmap.isPending) {
            DrawingManager.getInstance().tempBitmap.isPending = false;

            if (DrawingManager.getInstance().tempBitmap.isPattern) {
                drawView.addPattern(DrawingManager.getInstance().tempBitmap.bitmap);
            } else {
                drawView.addMovableBitmap(DrawingManager.getInstance().tempBitmap.bitmap);
            }
        }
    }

    public void openColorPicker() {
        ColorPickerDialogBuilder
                .with(getActivity())
                .setTitle(getBaseActivity().getString(R.string.pick_a_color))
                .initialColor(DrawingManager.getInstance().paintColor)
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(10)
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setPositiveButton(getString(R.string.ok), new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                        String color = "#" + (Integer.toHexString(selectedColor).toUpperCase());
                        drawView.setColor(color);
                        updateColorHint();
                    }
                })
                .build()
                .show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linSave:
                if (UserManager.getInstance(getActivity()).isSignedIn()) {
                    if (DrawingManager.getInstance().history.size() == 1 && DrawingManager.getInstance().history.get(0).type == DrawingEventType.BITMAP_STATIC) {
                        PrintOptionsDialog.newInstance().show(getChildFragmentManager(), "select_message");
                    } else {
                        promptIsDesignPublicAndSave();
                        lastSaveListSize = DrawingManager.getInstance().history.size();
                    }
                } else {
                    GenericDialogHelper.showUnauthenticated(getBaseActivity());
                    DesignBitmapManager.getInstance().setNailDesignBitmap(getPrintImage());
                    DesignBitmapManager.getInstance().setPendingPrint(true);
                }
                break;
            case R.id.linUndo:
                drawView.onClickUndo();
                break;
            default:
                break;
        }
    }

    public Bitmap getDrawingBitmapWithMask() {
        Canvas canvas = new Canvas();
        Paint paint = new Paint();

        drawView.setDrawingCacheEnabled(true);

        Bitmap mainImage = drawView.getDrawingCache();
        Bitmap mask = Bitmap.createScaledBitmap(
                BitmapFactory.decodeResource(getResources(), R.drawable.nail),
                mainImage.getWidth(),
                mainImage.getHeight(),
                true
        );

        Bitmap result = Bitmap.createBitmap(mainImage.getWidth(), mainImage.getHeight(), Bitmap.Config.ARGB_8888);

        canvas.setBitmap(result);
        paint.setFilterBitmap(false);

        canvas.drawBitmap(mainImage, 0, 0, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawBitmap(mask, 0, 0, paint);
        paint.setXfermode(null);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        result.compress(Bitmap.CompressFormat.PNG, 100, bytes);

        return result;
    }

    public Bitmap getPrintImage() {
        Canvas canvas = new Canvas();
        drawView.setDrawingCacheEnabled(true);
        Bitmap mainImage = drawView.getDrawingCache();
        Bitmap mask1 = BitmapFactory.decodeResource(getResources(), R.drawable.nail);
        Bitmap mask = Bitmap.createScaledBitmap(mask1, mainImage.getWidth(), mainImage.getHeight(), true);

        Bitmap result = Bitmap.createBitmap(mainImage.getWidth(),
                mainImage.getHeight(), Bitmap.Config.ARGB_8888);

        canvas.setBitmap(result);
        Paint paint = new Paint();
        paint.setFilterBitmap(false);

        canvas.drawBitmap(mainImage, 0, 0, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawBitmap(mask, 0, 0, paint);
        paint.setXfermode(null);

        return result;
    }

    public Bitmap getNailImage() {
        Canvas canvas1 = new Canvas();
        frmScreenShot.setDrawingCacheEnabled(true);

        Bitmap mainImage1 = frmScreenShot.getDrawingCache();
        Bitmap nailWithFinger = BitmapFactory.decodeResource(getResources(), R.drawable.nail_with_finger);
        nailWithFinger = Bitmap.createScaledBitmap(nailWithFinger, mainImage1.getWidth(), mainImage1.getHeight(), true);
        Bitmap result1 = Bitmap.createBitmap(mainImage1.getWidth(), mainImage1.getHeight(), Bitmap.Config.ARGB_8888);

        canvas1.setBitmap(result1);
        Paint paint1 = new Paint();
        paint1.setFilterBitmap(false);

        canvas1.drawBitmap(mainImage1, 0, 0, paint1);
        paint1.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas1.drawBitmap(nailWithFinger, 0, 0, paint1);
        paint1.setXfermode(null);

        return result1;
    }

    @Override
    public void onSelectFontCompleted(String fontName) {
        DrawingManager.getInstance().fontName = fontName;
        DrawingManager.getInstance().editText = new StringBuilder(getString(R.string.placeholder_your_text));
        Bitmap tempBitmap = BitmapUtils.drawTextToBitmap(
                getActivity(),
                DrawingManager.getInstance().fontName,
                DrawingManager.getInstance().editText.toString()
        );
        drawView.addTextBitmap(tempBitmap);
        InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,
                InputMethodManager.HIDE_IMPLICIT_ONLY);

        openColorPicker();
    }

    @Override
    public boolean onBackPressed() {
        super.onBackPressed();

        if (DrawingManager.getInstance().history.size() != lastSaveListSize && DrawingManager.getInstance().history.size() > 1) {
            GeneralDialog generalDialog = GeneralDialog.newInstance(getString(R.string.exit_without_saving_changes));
            generalDialog.setOnPositiveClickListener(getString(R.string.ok), new OnClickListener() {
                @Override
                public void onClick(View v) {
                    DrawingManager.getInstance().history.clear();
                    lastSaveListSize = DrawingManager.getInstance().history.size();
                    DesignBitmapManager.getInstance().reset();
                    DrawingManager.getInstance().reset();

                    getBaseActivity().onBackPressed();
                }
            });
            generalDialog.setOnNeutralClickListener(getString(R.string.cancel), new OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
            generalDialog.show(getActivity().getSupportFragmentManager(), GeneralDialog.class.getName());
            return true;
        }

        DrawingManager.getInstance().history.clear();
        DesignBitmapManager.getInstance().reset();

        return false;
    }


    public void updateColorHint() {
        Bitmap color = Bitmap.createBitmap(30, 30, Bitmap.Config.ARGB_8888);
        color.eraseColor(DrawingManager.getInstance().paintColor);

        ((ImageView) getView().findViewById(R.id.tool_color_preview)).setImageBitmap(color);
    }

    private void flattenDrawing() {
        Bitmap b = getDrawingBitmapWithMask();

        DrawingManager.getInstance().history.clear();

        drawView.addStaticBitmap(b);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent KEvent) {
        int keyAction = KEvent.getAction();

        if (DrawingManager.getInstance().currentTool == DrawingManager.TOOL_TEXT) {
            if (KEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                InputMethodManager imm = (InputMethodManager) getBaseActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(drawView.getWindowToken(), 0);
                return true;
            } else if (keyAction == KeyEvent.ACTION_DOWN) {
                if (DrawingManager.getInstance().editText.toString().equals(getString(R.string.placeholder_your_text))) {
                    DrawingManager.getInstance().editText = new StringBuilder();
                }

                if (KEvent.getKeyCode() == KeyEvent.KEYCODE_DEL) {
                    String ts = DrawingManager.getInstance().editText.toString();
                    if (ts.length() > 0) {
                        String tv = DrawingManager.getInstance().editText.substring(0, ts.length() - 1);
                        DrawingManager.getInstance().editText = new StringBuilder();
                        DrawingManager.getInstance().editText.append(tv);
                    } else {
                        DrawingManager.getInstance().editText = new StringBuilder();
                        DrawingManager.getInstance().editText.append("");
                    }
                } else {
                    KEvent.getUnicodeChar(KEvent.getDisplayLabel());
                    int keyunicode = KEvent.getUnicodeChar(KEvent.getMetaState());
                    if (keyunicode != 0) {
                        DrawingManager.getInstance().editText.append(String.valueOf((char) keyunicode));
                    }
                }
                Bitmap tempBitmap = BitmapUtils.drawTextToBitmap(
                        getBaseActivity(),
                        DrawingManager.getInstance().fontName,
                        DrawingManager.getInstance().editText.toString()
                );
                drawView.updateTextBitmap(tempBitmap);
                return true;
            }
        }
        
        return false;
    }

    // ------------------------ Saving and networking ------------------------

    public void promptIsDesignPublicAndSave() {
        GeneralDialog generalDialog = GeneralDialog.newInstance(getString(R.string.do_you_want_to_publish));
        generalDialog.setOnPositiveClickListener(getString(R.string.yes), new OnClickListener() {
            @Override
            public void onClick(View v) {
                saveDesign(true);
            }
        });
        generalDialog.setOnNeutralClickListener(getString(R.string.no), new OnClickListener() {
            @Override
            public void onClick(View v) {
                saveDesign(false);
            }
        });
        generalDialog.show(getFragmentManager(), GeneralDialog.class.getName());
    }

    private void saveDesign(boolean isPublic) {
        setProgressVisibility(true);

        try {
            Bitmap printImage = getPrintImage();

            RequestApi.getInstance(getActivity()).postDesignN(
                    printImage,
                    isPublic,
                    new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            setProgressVisibility(false);

                            final GeneralDialog generalDialog1 = GeneralDialog.newInstance(
                                    new SpannableString(
                                            Html.fromHtml(
                                                    getString(R.string.save_design_fail)
                                            )
                                    )
                            );
                            generalDialog1.setOnNeutralClickListener(getBaseActivity().getString(R.string.retry), new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    generalDialog1.dismiss();
                                    promptIsDesignPublicAndSave();
                                }
                            });
                            generalDialog1.setOnPositiveClickListener(getString(R.string.print_without_saving), new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ((BaseActivity) getActivity()).loadFragment(FragmentPrintReady.newInstance());
                                }
                            });

                            generalDialog1.show(getChildFragmentManager(), "format_dialog");

                            DesignBitmapManager.getInstance().setNailDesignId(0);
                            DesignBitmapManager.getInstance().setNailDesignBitmap(getPrintImage());
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            setProgressVisibility(false);
                            int designId = 0;

                            try {
                                JSONObject obj = new JSONObject(response.body().string());
                                designId = Integer.parseInt(obj.optString("id"));
                            } catch (JSONException | NumberFormatException e) {
                                e.printStackTrace();
                            }

                            DesignBitmapManager.getInstance().setNailDesignId(designId);
                            DesignBitmapManager.getInstance().setNailDesignBitmap(getPrintImage());
                            PrintOptionsDialog.newInstance().show(getChildFragmentManager(), PrintOptionsDialog.class.getName());
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}