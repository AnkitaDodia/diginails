package com.bopo.fragments;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.bopo.api.RequestApi;
import com.bopo.utils.BitmapUtils;
import com.bopo.utils.Logger;
import com.bopo.utils.managers.DrawingManager;
import com.bopo.R;
import com.bopo.adapters.PatternsAdapter;
import com.bopo.views.GridViewScrollable;
import com.bopo.bean.PatternItem;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class FragmentPatterns extends BaseFragment {
	View rootView;
	GridViewScrollable gridStickers;
	List<PatternItem> listPatterns;
	FragmentDesigner fragmentDesigner;

	public static FragmentPatterns newInstance(FragmentDesigner fragmentDesigner) {
		FragmentPatterns fragmentPatterns = new FragmentPatterns();
		fragmentPatterns.fragmentDesigner = fragmentDesigner;
		return fragmentPatterns;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_patterns, container,
				false);
		gridStickers = (GridViewScrollable) rootView
				.findViewById(R.id.gridStickers);
		getBaseActivity().setBackButtonVisibility(true);

		gridStickers.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				try {
					String url = listPatterns.get(position).patternsImage;
					Picasso.with(getActivity()).load(url).into(target);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		getPatterns();
		return rootView;
	}

	Target target = new Target() {

		@Override
		public void onPrepareLoad(Drawable arg0) {
			Logger.e("onPrepareLoad");

		}

		@Override
		public void onBitmapLoaded(final Bitmap bmap, LoadedFrom arg1) {
			Bitmap b = BitmapUtils.scale(bmap);

			DrawingManager.getInstance().tempBitmap = new DrawingManager.TempBitmap(b, true, true);
			getBaseActivity().removeFragment();

			if (fragmentDesigner != null) {
				fragmentDesigner.openColorPicker();
			}
		}

		@Override
		public void onBitmapFailed(Drawable arg0) {
			Logger.e("onBitmapFailed");

		}
	};

	public static Bitmap scale(Bitmap realImage, float maxImageSize,
			boolean filter) {
		float ratio = Math.min((float) maxImageSize / realImage.getWidth(),
				(float) maxImageSize / realImage.getHeight());
		int width = Math.round((float) ratio * realImage.getWidth());
		int height = Math.round((float) ratio * realImage.getHeight());

		Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width, height,
				filter);
		return newBitmap;
	}

	public Bitmap mergeBitmap(Bitmap fr, Bitmap sc) {

		Bitmap comboBitmap;

		int width, height;

		width = fr.getWidth();
		height = fr.getHeight() + fr.getHeight() - 50;

		comboBitmap = Bitmap.createBitmap(width, height,
				Bitmap.Config.ARGB_8888);

		Canvas comboImage = new Canvas(comboBitmap);

		comboImage.drawBitmap(fr, 0f, 0f, null);
		comboImage.drawBitmap(sc, 0f, fr.getHeight() - 50, null);
		return comboBitmap;

	}

	public void getPatterns() {
		setProgressVisibility(true);
		RequestApi.getInstance(getActivity()).get(
				RequestApi.GET_PATTERNS,
				new Callback() {
					@Override
					public void onFailure(Call call, IOException e) {
						showPopup(getString(R.string.error_network_generic));
					}
					@Override
					public void onResponse(Call call, Response response) throws IOException {
						setProgressVisibility(false);
						String responseBody = response.body().string();
						JSONObject jsonObject;

						if (responseBody.isEmpty() || !response.isSuccessful()) {
							showPopup(getString(R.string.error_network_generic));
							return;
						}

						try {
							jsonObject = new JSONObject(responseBody);
							if (jsonObject.has("error")) {
								showPopup(jsonObject.getString("error"));
								return;
							}

							listPatterns = new ArrayList<>();
							JSONArray jsnArray = jsonObject.getJSONArray("patterns");
							for (int i = 0; i < jsnArray.length(); i++) {
								JSONObject objData = jsnArray.getJSONObject(i);
								PatternItem st = new PatternItem();
								st.patternsID = objData.getString("id");
								st.patternsName = objData.getString("name");
								st.patternsImage = objData.getString("img_path");
								st.patternsThumb = objData.getString("thumb_path");
								listPatterns.add(st);

							}
						} catch (JSONException e) {
							showPopup(getString(R.string.error_network_generic));
							return;
						}

						if (listPatterns.size() > 0) {
							final PatternsAdapter patternsBaseAdapter = new PatternsAdapter(
									getBaseActivity(), listPatterns);
							getActivity().runOnUiThread(new Runnable() {
								@Override
								public void run() {
									gridStickers.setAdapter(patternsBaseAdapter);
								}
							});
						} else {
							showPopup(getString(R.string.error_network_generic));
						}
					}
				}
		);
	}
}
