package com.bopo.fragments;

import java.io.IOException;

import org.json.JSONObject;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.bopo.R;
import com.bopo.api.RequestApi;
import com.bopo.utils.Analytics;
import com.bopo.utils.AppUtils;
import com.bopo.utils.Consts;
import com.bopo.utils.managers.UserManager;
import com.bopo.views.AnimationOnTouchListener;
import com.bopo.utils.managers.DesignBitmapManager;
import com.bopo.enums.Fragments;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class FragmentLogin extends BaseFragment implements OnClickListener {
	private EditText etUserName, etPassword;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_login, container, false);
		getBaseActivity().setBackButtonVisibility(true);

		TextView textGo, textForgotPassword, textRegistration, textPrivacyPolicy;

		etUserName = (EditText) rootView.findViewById(R.id.etUserName);
		etPassword = (EditText) rootView.findViewById(R.id.etPassword);
		textForgotPassword = (TextView) rootView.findViewById(R.id.textForgotPassword);
		textRegistration = (TextView) rootView.findViewById(R.id.textRegistration);
		textPrivacyPolicy = (TextView) rootView.findViewById(R.id.textPrivacyPolicy);

		textGo = (TextView) rootView.findViewById(R.id.textGo);
		textGo.setOnClickListener(this);
		textForgotPassword.setOnClickListener(this);
		textRegistration.setOnClickListener(this);
		textPrivacyPolicy.setOnClickListener(this);

		textRegistration.setOnTouchListener(new AnimationOnTouchListener(1.25f));
		textGo.setOnTouchListener(new AnimationOnTouchListener(1.25f));

		return rootView;
	}

	public void requestLogin() {
		if (isValid()) {
			setProgressVisibility(true);
			RequestApi.getInstance(getActivity()).post(
					RequestApi.POST_LOGIN,
					new RequestApi.RequestBuilder()
							.put("username", etUserName.getText().toString())
							.put("pwd", etPassword.getText().toString())
							.build(),
					new Callback() {
						@Override
						public void onFailure(Call call, IOException e) {
							showPopup(getString(R.string.error_network_generic));
						}
						@Override
						public void onResponse(Call call, Response response) throws IOException {
							setProgressVisibility(false);
							String responseBody = response.body().string();

							if (responseBody.isEmpty() || !response.isSuccessful()) {
								showPopup(getString(R.string.error_wrong_credientials));
								return;
							}

							try {
								UserManager.getInstance(getActivity()).setUser(new JSONObject(responseBody));
							} catch (Exception e) {
								showPopup(getString(R.string.error_network_generic));
								return;
							}

							getActivity().runOnUiThread(new Runnable() {
								@Override
								public void run() {
									getBaseActivity().refreshMenuAccount();
								}
							});
							AppUtils.hideKeyboard(getActivity(), getView());

							if (DesignBitmapManager.getInstance().isPendingPrint()) {
								getBaseActivity().onBackPressed();
							} else {
								getBaseActivity().loadFragment(Fragments.AccountFragment);
							}

						}
					}
			);
			Analytics.log(Analytics.ACTION_AUTH_SIGN_IN);
		}
	}

	private boolean isValid() {
		if (etUserName.getText().toString().trim().equals("")) {
			etUserName.setError(getString(R.string.username_error));
			etUserName.requestFocus();
			return false;
		} else if (etPassword.getText().toString().trim().equals("")) {
			etPassword.setError(getString(R.string.password_blank_message));
			etPassword.requestFocus();
			return false;
		}
		return true;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.textGo:
			requestLogin();
			break;
		case R.id.textForgotPassword:
			getBaseActivity().loadFragment(Fragments.ForgotPasswordFragment);
			break;
		case R.id.textRegistration:
			getBaseActivity().loadFragment(Fragments.RegisterFragment);
			break;
		case R.id.textPrivacyPolicy:
			Intent browserIntent = new Intent(Intent.ACTION_VIEW,
					Uri.parse(Consts.ROOT_URL + "coppa-policy"));
			startActivity(browserIntent);
			break;

		default:
			break;
		}
	}
}
