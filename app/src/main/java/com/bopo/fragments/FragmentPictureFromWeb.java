package com.bopo.fragments;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bopo.R;
import com.bopo.dialogs.PermissionDialog;
import com.bopo.dialogs.PermissionDialogListener;
import com.bopo.intefaces.OnBackPressedListener;
import com.bopo.utils.AppUtils;
import com.bopo.utils.managers.DrawingManager;
import com.bopo.views.CropImageView;
import com.bopo.views.CustomWebview;

public class FragmentPictureFromWeb extends BaseFragment implements
		PermissionDialogListener, OnBackPressedListener {
	View rootView;

	LinearLayout linselect, linaction, lincancel, lincrop;
	TextView textWeb, txtGO;
	private Uri mImageCaptureUri;
	CustomWebview webview;
	EditText edtUrl;
	private CropImageView mCropView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_web_picture,
				container, false);
		mCropView = (CropImageView) rootView.findViewById(R.id.cropImageView);
		linselect = (LinearLayout) rootView.findViewById(R.id.linselect);
		linaction = (LinearLayout) rootView.findViewById(R.id.linaction);
		lincancel = (LinearLayout) rootView.findViewById(R.id.lincancel);
		lincrop = (LinearLayout) rootView.findViewById(R.id.lincrop);
		textWeb = (TextView) rootView.findViewById(R.id.textWeb);
		txtGO = (TextView) rootView.findViewById(R.id.txtGO);
		edtUrl = (EditText) rootView.findViewById(R.id.edtUrl);
		mCropView.setVisibility(View.GONE);

		edtUrl.setText("images.google.com");

		txtGO.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String s = edtUrl.getText().toString().trim();
				if (s.startsWith("http")) {
					webview.loadUrl(s);
				} else {
					webview.loadUrl("http://" + s);
				}

			}
		});

		webview = (CustomWebview) rootView.findViewById(R.id.webViewDesign);

		webview.getSettings().setJavaScriptEnabled(true);

		webview.getSettings().setBuiltInZoomControls(true);

		webview.getSettings().setLoadWithOverviewMode(true);
		webview.getSettings().setUseWideViewPort(true);
		webview.loadUrl("https://images.google.com");

		webview.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
			}
		});

		InsideWebViewClient webViewClient = new InsideWebViewClient() {
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				if (	url.contains("po" + "rn") ||
						url.contains("se" + "x") ||
						url.contains("fu" + "ck") ||
						url.contains("na" + "ked") ||
						url.contains("pu" + "ssy") ||
						url.contains("xx" + "x")) {
					webview.loadUrl("https://images.google.com");
				}

			}
		};

		webview.setWebViewClient(webViewClient);

		linselect.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				webview.setDrawingCacheEnabled(true);
				Bitmap img = webview.getDrawingCache();
				img = img.copy(img.getConfig(), true);
				mCropView.setVisibility(View.VISIBLE);
				mCropView.setImageBitmap(img);
				mCropView.setCropMode(CropImageView.CropMode.RATIO_FREE);
				new Handler().postDelayed(new Runnable() {
					public void run() {
						showCrop();
					}
				}, 500);

			}
		});
		lincancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				hideCrop();
			}
		});
		lincrop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				FragmentManager fm = getChildFragmentManager();
				PermissionDialog selecteMessageDialog = PermissionDialog.newInstance(
						FragmentPictureFromWeb.this);
				selecteMessageDialog.show(fm, "select_message");

			}
		});
		hideCrop();
		return rootView;
	}

	public void showCrop() {
		webview.setVisibility(View.GONE);
		linselect.setVisibility(View.GONE);
		linaction.setVisibility(View.VISIBLE);

	}

	public void hideCrop() {
		webview.setVisibility(View.VISIBLE);
		linselect.setVisibility(View.VISIBLE);
		linaction.setVisibility(View.GONE);
		mCropView.setVisibility(View.GONE);
	}

	@Override
	public boolean onBackPressed() {
		if (canGoBack()) {
			goBack();
			return true;
		}
		return false;
	}

	private class InsideWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;

		}

	}

	public boolean canGoBack() {
		return this.webview != null && this.webview.canGoBack();
	}

	public void goBack() {
		if (this.webview != null) {
			this.webview.goBack();
		}
	}

	@Override
	public void onAgree() {
		DrawingManager.getInstance().tempBitmap = new DrawingManager.TempBitmap(mCropView.getCroppedBitmap(), true);
		getBaseActivity().removeFragment();
	}
}
