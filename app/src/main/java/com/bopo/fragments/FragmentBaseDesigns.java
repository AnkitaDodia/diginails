package com.bopo.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bopo.R;
import com.bopo.activities.BaseActivity;
import com.bopo.adapters.GalleryArrayAdapter;
import com.bopo.api.RequestApi;
import com.bopo.bean.GalleryDesignItem;
import com.bopo.intefaces.OnDataLoadedListener;
import com.bopo.utils.Consts;
import com.bopo.views.GalleryTabScrollListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by Nick Yaro on 3/3/2017.
 */

public abstract class FragmentBaseDesigns extends BaseFragment {
    protected List<GalleryDesignItem> mItems = new ArrayList<>();
    private OnDataLoadedListener mOnDataLoadedListener;

    protected ArrayAdapter<GalleryDesignItem> mAdapter;

    GridView mGridView;
    ProgressBar mProgressBar;
    TextView mErrorTextView;

    public abstract String getUrl(int offset);
    public abstract boolean isInfiniteScroll();
    public boolean isOwnDesigns() { return false; }
    public void onDesignDeleteClick(int position) {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_gallery_tab, container, false);

        mGridView = (GridView) rootView.findViewById(R.id.gallery_grid_view);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.gallery_progress);
        mErrorTextView = (TextView) rootView.findViewById(R.id.gallery_loading_error);

        mErrorTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });

        if (isInfiniteScroll()) {
            mGridView.setOnScrollListener(new GalleryTabScrollListener(this));
        }

        getBaseActivity().setBackButtonVisibility(false);

        populateOrLoadData();

        return rootView;
    }

    public void loadData() {
        mItems.clear();
        loadData(0);
    }

    public void loadData(int offset) {
        mGridView.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
        mErrorTextView.setVisibility(View.GONE);

        RequestApi.getInstance(getActivity()).get(getUrl(offset), getCallback());
    }

    public void populateOrLoadData() {
        if (mItems != null && mItems.size() > 0) {
            refreshListView();
        } else {
            loadData();
        }
    }

    public ArrayAdapter<GalleryDesignItem> createAdapter() {
        return new GalleryArrayAdapter((BaseActivity) getActivity(), FragmentBaseDesigns.this, mItems, isOwnDesigns());
    }

    public OnDataLoadedListener getOnDataLoadedListener() {
        return mOnDataLoadedListener;
    }
    public void setOnDataLoadedListener(OnDataLoadedListener onDataLoadedListener) {
        mOnDataLoadedListener = onDataLoadedListener;
    }

    public Callback getCallback() {
        return new Callback() {
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String responseBody = response.body().string();
                    responseBody = responseBody.replace("{\"user-designs\"", "{\"designs\"");

                    JSONObject obj = new JSONObject(responseBody);
                    JSONArray arr = obj.getJSONArray("designs");

                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject design = (JSONObject) arr.get(i);

                        try {
                            GalleryDesignItem galleryDesignItem = new GalleryDesignItem(
                                    Integer.parseInt(design.getString("id")),
                                    design.getString("img_path"),
                                    design.getString("downloads").equals("null") ? 0 : Integer.parseInt(design.getString("downloads")),
                                    design.getString("design_rating").equals("null") ? 0 : Double.parseDouble(design.getString("design_rating")),
                                    Integer.parseInt(design.getString("users_id")),
                                    design.getString("username"),
                                    Integer.parseInt(design.getString("public")) == 1
                            );
                            mItems.add(galleryDesignItem);

                            if (i < 50) { //preload half of the list
                                String imagePath = Consts.ROOT_URL + "resize.php?img=" + galleryDesignItem.getPrintImgPath() + "&width=200&h=360&zc=1";
                                Picasso.with(getActivity()).load(imagePath).fetch();
                            }
                        } catch (NumberFormatException e) {
                        }
                    }

                    if (getOnDataLoadedListener() != null) {
                        getOnDataLoadedListener().onDataLoaded();
                    }

                } catch (IOException e) {
                    onFailure(null, e);
                } catch (Exception e) {
                    onFailure(null, null);
                }

                refreshListView();
            }

            @Override
            public void onFailure(Call call, IOException e) {
                if (getActivity() == null) { return; }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mGridView.setVisibility(View.GONE);
                        mProgressBar.setVisibility(View.GONE);
                        mErrorTextView.setVisibility(View.VISIBLE);
                    }
                });
            }
        };
    }

    public void refreshListView() {
        if (!isAdded()) {
            return;
        }

        if (mItems.size() > 0) {
            if (getActivity() == null) { return; }

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mGridView.setVisibility(View.VISIBLE);
                    mProgressBar.setVisibility(View.GONE);

                    if (mAdapter == null) {
                        mAdapter = createAdapter();
                        mGridView.setAdapter(mAdapter);
                    } else {
                        mGridView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                    }

                }
            });
        } else {
            getCallback().onFailure(null, null);
        }
    }

}
