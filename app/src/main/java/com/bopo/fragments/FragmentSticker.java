package com.bopo.fragments;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.bopo.R;
import com.bopo.adapters.StickersBaseAdapter;
import com.bopo.api.RequestApi;
import com.bopo.utils.managers.DrawingManager;
import com.bopo.views.GridViewScrollable;
import com.bopo.bean.StickerItem;
import com.bopo.dialogs.SelectSubStickerDialogFragment;
import com.bopo.dialogs.SelectSubStickerListener;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class FragmentSticker extends BaseFragment implements SelectSubStickerListener {
	GridViewScrollable gridStickers;
	List<StickerItem> listStickerItems;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater
				.inflate(R.layout.fragment_sticker, container, false);
		gridStickers = (GridViewScrollable) rootView
				.findViewById(R.id.gridStickers);
		getBaseActivity().setBackButtonVisibility(true);

		gridStickers.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String stickerName = listStickerItems.get(position).name
						.toString().trim();
				String stickerID = listStickerItems.get(position).id
						.toString().trim();
				// sendSubStickerRequest(StrckerID);
				FragmentManager fm = getChildFragmentManager();
				SelectSubStickerDialogFragment selecteUserDialog = SelectSubStickerDialogFragment.newInstance(
						FragmentSticker.this, stickerID, stickerName);
				selecteUserDialog.show(fm, "select_user");

			}
		});
		getStickers();
		return rootView;
	}

	@Override
	public void onSelectSubSticker(Bitmap bmp) {
		DrawingManager.getInstance().tempBitmap = new DrawingManager.TempBitmap(bmp, true);
		getBaseActivity().removeFragment();
	}

	public void getStickers() {
		setProgressVisibility(true);
		RequestApi.getInstance(getActivity()).get(
				RequestApi.GET_STICKER_CATEGORIES,
				new Callback() {
					@Override
					public void onFailure(Call call, IOException e) {
						showPopup(getString(R.string.error_network_generic));
					}
					@Override
					public void onResponse(Call call, Response response) throws IOException {
						setProgressVisibility(false);

						String responseBody = response.body().string();
						JSONObject jsonObject;

						if (responseBody.isEmpty() || !response.isSuccessful()) {
							showPopup(getString(R.string.error_network_generic));
							return;
						}

						try {
							jsonObject = new JSONObject(responseBody);
							if (jsonObject.has("error")) {
								showPopup(jsonObject.getString("error"));
								return;
							}

							listStickerItems = new ArrayList<>();
							JSONArray jsonArray = jsonObject.getJSONArray("sticker_categories");
							for (int i = 0; i < jsonArray.length(); i++) {
								JSONObject objData = jsonArray.getJSONObject(i);
								StickerItem st = new StickerItem();
								st.id = objData.getString("id");
								st.name = objData.getString("name");
								st.stickerThumb = objData
										.getString("sticker_thumb_path");
								listStickerItems.add(st);

							}


						} catch (JSONException e) {
							showPopup(getString(R.string.error_network_generic));
							return;
						}

						if (listStickerItems.size() > 0) {
							final StickersBaseAdapter stickersBaseAdapter = new StickersBaseAdapter(
									getBaseActivity(), listStickerItems);
							getActivity().runOnUiThread(new Runnable() {
								@Override
								public void run() {
									gridStickers.setAdapter(stickersBaseAdapter);
								}
							});
						} else {
							showPopup(getString(R.string.error_network_generic));
						}
					}
				}
		);
	}

}
