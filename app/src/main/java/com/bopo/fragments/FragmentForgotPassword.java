package com.bopo.fragments;

import java.io.IOException;
import java.util.Locale;

import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bopo.R;
import com.bopo.api.RequestApi;
import com.bopo.dialogs.GeneralDialog;
import com.bopo.utils.Analytics;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class FragmentForgotPassword extends BaseFragment implements OnClickListener {
	View rootView;
	TextView textForgot;
	EditText edtEmailAddres;
	ImageView imgHelp;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.forgot_password_fragment,
				container, false);
		textForgot = (TextView) rootView.findViewById(R.id.textForgot);
		edtEmailAddres = (EditText) rootView.findViewById(R.id.edtEmailAddres);
		imgHelp = (ImageView) rootView.findViewById(R.id.imgHelp);
		textForgot.setOnClickListener(this);
		imgHelp.setOnClickListener(this);

		return rootView;
	}

	public void requestForgotPass() {
		if (isValid()) {
			Analytics.log(Analytics.ACTION_AUTH_PASSWORD_RESET);
			setProgressVisibility(true);

			RequestApi.getInstance(getActivity()).get(
					String.format(Locale.CANADA, RequestApi.GET_RESET_PASSWORD, edtEmailAddres.getText().toString()),
					new Callback() {
				@Override
				public void onFailure(Call call, IOException e) {
					showPopup(getString(R.string.error_network_generic));
				}

				@Override
				public void onResponse(Call call, Response response) throws IOException {
					try {
						JSONObject obj = new JSONObject(response.body().string());
						JSONObject errorObj = obj.optJSONObject("error");

						if (errorObj != null && !errorObj.optString("text").isEmpty()) {
							showPopup(errorObj.optString("text"));
						} else {
							showPopup("We've sent an email with a link to reset your password.\nIf you don't see it, please wait a few minutes and be sure to check your junk/spam folder.");
						}
					} catch (Exception e) {
						showPopup(getString(R.string.error_network_generic));
					}
				}
			});
		}
	}

	private boolean isValid() {
		if (edtEmailAddres.getText().toString().trim().equals("")) {
			edtEmailAddres.setError(getString(R.string.email_blank_message));
			edtEmailAddres.requestFocus();
			return false;
		} else if (!edtEmailAddres.getText().toString().trim().equals("")) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.textForgot:
			requestForgotPass();
			break;
		case R.id.imgHelp:
			FragmentManager fm = getChildFragmentManager();
			GeneralDialog selecteMessageDialog = GeneralDialog.newInstance(
					getActivity().getResources().getString(
							R.string.forgotmessage));
			selecteMessageDialog.show(fm, "select_message");
			break;
		default:
			break;
		}
	}

}
