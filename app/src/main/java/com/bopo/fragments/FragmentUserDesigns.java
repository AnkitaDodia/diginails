package com.bopo.fragments;

import com.bopo.R;
import com.bopo.api.RequestApi;

import java.util.Locale;

/**
 * Created by Nick Yaro on 3/3/2017.
 */

public class FragmentUserDesigns extends FragmentBaseDesigns {
    private int mUserId;
    private String mUserName;

    public static FragmentUserDesigns newInstance(int userId, String userName) {
        FragmentUserDesigns fragment = new FragmentUserDesigns();
        fragment.mUserId = userId;
        fragment.mUserName = userName;
        return fragment;
    }

    @Override
    public String getUrl(int offset) {
        return String.format(
                Locale.CANADA,
                RequestApi.GET_USER_DESIGNS_PUBLIC,
                mUserId
        );
    }

    @Override
    public String getTitle() {
        return String.format(getString(R.string.users_designs), mUserName);
    }

    @Override
    public boolean isInfiniteScroll() {
        return false;
    }

}
