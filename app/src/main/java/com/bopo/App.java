package com.bopo;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

public class App extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		if (!BuildConfig.DEBUG) {
			Fabric.with(this, new Crashlytics());
		}

		Picasso.Builder builder = new Picasso.Builder(this);
		builder.downloader(new OkHttpDownloader(this, Integer.MAX_VALUE));
		Picasso built = builder.build();
		Picasso.setSingletonInstance(built);
	}
}
