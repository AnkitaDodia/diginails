package com.bopo.fonts;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.bopo.R;

public class KelsonBoldFontTextView extends CustomFontTextView {
	String fontFaceName;

	public KelsonBoldFontTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
	}

	public KelsonBoldFontTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}

	public KelsonBoldFontTextView(Context context) {
		super(context);
		init(null);
	}
	
	private void init(AttributeSet attrs) {
		if (attrs != null) {
			 TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);
			 String fontFace = "Kelson_Bold.otf";
			 setFontFace(fontFace);
			 a.recycle();
		}
	}

	public String getFontFaceName() {
		return fontFaceName;
	}
}