package com.bopo.fonts;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.bopo.R;

public class CustomFontTextView extends TextView {
	String fontFaceName;

	public CustomFontTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
	}
	
	public CustomFontTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}
	
	public CustomFontTextView(Context context) {
		super(context);
		init(null);
	}
	
	private void init(AttributeSet attrs) {
		if (attrs != null) {
			 TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);
			 String fontFace = a.getString(R.styleable.CustomFontTextView_fontFace);
			setFontFace(fontFace);
			 a.recycle();
		}
	}

	protected void setFontFace(String fontFace) {
		if (fontFace != null) {
			fontFaceName = fontFace;
			Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + fontFace);
			setTypeface(myTypeface);
		}
	}

	public String getFontFaceName() {
		return fontFaceName;
	}
}