package com.bopo.views;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.PagerTabStrip;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Nick Yaro on 16/3/2016.
 */
public class CustomPagerTabStrip extends PagerTabStrip {
    public CustomPagerTabStrip(Context context) {
        super(context);
        this.setDrawFullUnderline(false);
    }
    public CustomPagerTabStrip(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setDrawFullUnderline(false);
    }
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Kelson_Regular.otf");
        for (int i=0; i<this.getChildCount(); i++) {
            View v = this.getChildAt(i);
            if (v instanceof TextView) {
                ((TextView)v).setTypeface(tf);
                ((TextView)v).setTextSize(20);
            }
        }
    }
}
