package com.bopo.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import com.bopo.R;
import com.bopo.bean.DrawModel;
import com.bopo.enums.DrawingEventType;
import com.bopo.utils.BitmapUtils;
import com.bopo.bean.ImageEntity;
import com.bopo.utils.drawing.MultiTouchController;
import com.bopo.utils.drawing.MultiTouchController.MultiTouchObjectCanvas;
import com.bopo.utils.drawing.MultiTouchController.PointInfo;
import com.bopo.utils.drawing.MultiTouchController.PositionAndScale;
import com.bopo.utils.drawing.MultiTouchEntity;
import com.bopo.utils.managers.DrawingManager;

public class DrawingView extends View implements MultiTouchObjectCanvas<MultiTouchEntity> {
	private Canvas mDrawCanvas;
    private Paint mDrawPaint, mPatternPaint;
	private Path mDrawPath;
    private Bitmap canvasBitmap;

	private int paintAlpha = 255;
	private int mBrushSize;
	private boolean erase;

    private MultiTouchController<MultiTouchEntity> multiTouchController =
            new MultiTouchController<MultiTouchEntity>(this);

    private PointInfo currTouchPoint = new PointInfo();

    private static final int UI_MODE_ROTATE = 1, UI_MODE_ANISOTROPIC_SCALE = 2;
    private int mUIMode = UI_MODE_ROTATE;

	public DrawingView(Context context) {
		this(context, null);
	}

	public DrawingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public DrawingView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {
		setBackgroundColor(Color.TRANSPARENT);

		mBrushSize = getResources().getInteger(R.integer.brush_size_2);

		mDrawPaint = new Paint();
		mDrawPaint.setColor(DrawingManager.getInstance().paintColor);
		mDrawPaint.setAntiAlias(true);
		mDrawPaint.setStrokeWidth(mBrushSize);
		mDrawPaint.setStyle(Paint.Style.STROKE);
		mDrawPaint.setStrokeJoin(Paint.Join.ROUND);
		mDrawPaint.setStrokeCap(Paint.Cap.ROUND);

		mDrawPath = new Path();
		mPatternPaint = new Paint();
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldW, int oldH) {
		super.onSizeChanged(w, h, oldW, oldH);
		canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		mDrawCanvas = new Canvas(canvasBitmap);
	}

    public void onClickUndo() {
        if (DrawingManager.getInstance().history.size() > 0) {
			if (
							DrawingManager.getInstance().history.size() == 1 &&
							DrawingManager.getInstance().history.get(0) != null &&
							DrawingManager.getInstance().history.get(0).type == DrawingEventType.BITMAP_STATIC) {
				return;
			}

            DrawingManager.getInstance().history.removeLast();
            invalidate();
        }
    }

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawColor(Color.WHITE);

		try {
			for (int i = 0; i < DrawingManager.getInstance().history.size(); i++) {
				switch (DrawingManager.getInstance().history.get(i).type) {
					case DRAW:
						mDrawPaint.setColor(DrawingManager.getInstance().history.get(i).color);
						mDrawPaint.setStrokeWidth(DrawingManager.getInstance().history.get(i).brushSize);
						canvas.drawPath(DrawingManager.getInstance().history.get(i).drawPath, mDrawPaint);
						break;
					case BITMAP_STATIC:
						Bitmap btr = DrawingManager.getInstance().history.get(i).bitmap;
						canvas.drawBitmap(btr, 0, 0, mDrawPaint);
						break;
					case BITMAP_MOVABLE:
					case TEXT:
						DrawingManager.getInstance().history.get(i).mte.draw(canvas);
						break;
					case PATTERN:
						Bitmap resultBitmap = DrawingManager.getInstance().history.get(i).bitmap;

						ColorFilter filter = new LightingColorFilter(DrawingManager.getInstance().history.get(i).color, 1);
						mPatternPaint.setColorFilter(filter);

						float ratio = canvas.getHeight() / (float)resultBitmap.getHeight();

						canvas.drawBitmap(
								resultBitmap,
								null,
								new Rect(0, 0, resultBitmap.getWidth() * Math.round(ratio), canvas.getHeight()),
								mPatternPaint
						);
						break;
				}
			}

			mDrawPaint.setColor(DrawingManager.getInstance().paintColor);
			mDrawPaint.setStrokeWidth(mBrushSize);
			canvas.drawPath(mDrawPath, mDrawPaint);

		} catch (OutOfMemoryError e) {
            DrawingManager.getInstance().history.clear();
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (DrawingManager.getInstance().currentTool == DrawingManager.TOOL_BRUSH) {
            float touchX = event.getX();
            float touchY = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mDrawPath.moveTo(touchX, touchY);
                    break;
                case MotionEvent.ACTION_MOVE:
                    mDrawPath.lineTo(touchX, touchY);
                    break;
                case MotionEvent.ACTION_UP:
                    mDrawPath.lineTo(touchX, touchY);
                    mDrawCanvas.drawPath(mDrawPath, mDrawPaint);

                    DrawModel dr = new DrawModel(DrawingEventType.DRAW);
                    dr.color = DrawingManager.getInstance().paintColor;
                    dr.brushSize = mBrushSize;
                    dr.drawPath = mDrawPath;
                    DrawingManager.getInstance().history.add(dr);

                    mDrawPath = new Path();
                    mDrawPath.reset();
                    break;
                default:
                    return false;
            }

            // redraw
            invalidate();
            return true;
        } else if (
						DrawingManager.getInstance().currentTool == DrawingManager.TOOL_STICKER ||
						DrawingManager.getInstance().currentTool == DrawingManager.TOOL_PHOTO_CAMERA ||
						DrawingManager.getInstance().currentTool == DrawingManager.TOOL_WEB ||
						DrawingManager.getInstance().currentTool == DrawingManager.TOOL_TEXT) {
            return multiTouchController.onTouchEvent(event);
        }
		return false;
    }

	public void addPattern(Bitmap b) {
		if (b == null) return;

		DrawModel dr = new DrawModel(DrawingEventType.PATTERN);
		dr.color = DrawingManager.getInstance().paintColor;
		dr.bitmap = b.copy(b.getConfig(), true);
		DrawingManager.getInstance().history.add(dr);
	}

	public void addStaticBitmap(Bitmap editBitmap) {
		if (editBitmap == null) { return; }

		DrawModel dr = new DrawModel(DrawingEventType.BITMAP_STATIC);
		dr.color = DrawingManager.getInstance().paintColor;
		dr.bitmap = editBitmap.copy(editBitmap.getConfig(), true);
		DrawingManager.getInstance().history.add(dr);
	}

	public void addMovableBitmap(Bitmap b) {
		if (b == null) return;

		DrawModel dr = new DrawModel(DrawingEventType.BITMAP_MOVABLE);
		dr.mte = new ImageEntity(b, getContext().getResources());

		float cx = 200;
		float cy = 200;
		DrawingManager.getInstance().history.add(dr);
		DrawingManager.getInstance().history.get(DrawingManager.getInstance().history.size() - 1).mte.load(getContext(), cx, cy);
		invalidate();
	}

	public void addTextBitmap(Bitmap bitmap) {
		DrawModel dr = new DrawModel(DrawingEventType.TEXT);
		dr.textValue = DrawingManager.getInstance().editText.toString();
		dr.mte = new ImageEntity(bitmap, getContext().getResources());

		float cx = mDrawCanvas.getWidth() / 2;
		float cy = mDrawCanvas.getHeight() / 5;
		DrawingManager.getInstance().history.add(dr);
		DrawingManager.getInstance().history.get(DrawingManager.getInstance().history.size() - 1).mte.load(getContext(), cx, cy);
		invalidate();
	}

	public void updateTextBitmap(Bitmap bitmap) {
		if (bitmap == null) return;

		for (int i = 0; i < DrawingManager.getInstance().history.size(); i++) {
			if (DrawingManager.getInstance().history.get(i).type == DrawingEventType.TEXT) {
				DrawModel dr = DrawingManager.getInstance().history.get(i);
				dr.textValue = DrawingManager.getInstance().editText.toString();
				dr.mte.update(bitmap);
				DrawingManager.getInstance().history.set(i, dr);
				float cx = mDrawCanvas.getWidth() / 2;
				float cy = mDrawCanvas.getHeight() / 5;
				DrawingManager.getInstance().history.get(DrawingManager.getInstance().history.size() - 1).mte
						.load(getContext(), cx, cy);
			}
		}
		invalidate();
	}

	public void updateTextColor() {
		for (int i = 0; i < DrawingManager.getInstance().history.size(); i++) {
			if (DrawingManager.getInstance().history.get(i).type == DrawingEventType.TEXT) {
				DrawModel dr = DrawingManager.getInstance().history.get(i);
				Bitmap tempBitmap = BitmapUtils.drawTextToBitmap(getContext(), DrawingManager.getInstance().fontName, dr.textValue);
				dr.mte.update(tempBitmap);
				DrawingManager.getInstance().history.set(i, dr);
				float cx = mDrawCanvas.getWidth() / 2;
				float cy = mDrawCanvas.getHeight() / 5;
				DrawingManager.getInstance().history.get(DrawingManager.getInstance().history.size() - 1).mte
						.load(getContext(), cx, cy);
			}
		}
		invalidate();
	}

	public void setColor(String newColor) {
		DrawingManager.getInstance().paintColor = Color.parseColor(newColor);
		mDrawPaint.setColor(DrawingManager.getInstance().paintColor);
		//mDrawPaint.setShader(null);

		if (DrawingManager.getInstance().currentTool == DrawingManager.TOOL_PATTERN) {
			addPattern(DrawingManager.getInstance().tempBitmap.bitmap);
		} else if (DrawingManager.getInstance().currentTool == DrawingManager.TOOL_TEXT) {
			updateTextColor();
		}
		invalidate();
	}

	public void setBrushSize(int newSize) {
		int pixelAmount = (int) TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, newSize, getResources()
						.getDisplayMetrics());
		mBrushSize = newSize;
		mDrawPaint.setStrokeWidth(mBrushSize);
	}

	/**
	 * Get the image that is under the single-touch point, or return null
	 * (canceling the drag op) if none
	 */
	public MultiTouchEntity getDraggableObjectAtPoint(PointInfo pt) {
		float x = pt.getX(), y = pt.getY();
		int n = DrawingManager.getInstance().history.size();
		for (int i = n - 1; i >= 0; i--) {
			ImageEntity im = (ImageEntity) DrawingManager.getInstance().history.get(i).mte;
			if (im.containsPoint(x, y))
				return im;
		}
		return null;
	}

	/**
	 * Select an object for dragging. Called whenever an object is found to be
	 * under the point (non-null is returned by getDraggableObjectAtPoint()) and
	 * a drag operation is starting. Called with null when drag op ends.
	 */
	public void selectObject(MultiTouchEntity img, PointInfo touchPoint) {
		currTouchPoint.set(touchPoint);
		if (img != null) {
			// Move image to the top of the stack when selected
			mDrawPaint.setColor(Color.TRANSPARENT);
		} else {
			// Called with img == null when drag stops.
		}
		/*
		 * if (app.mainBitmap != null) { for (int i = 0; i < DrawingManager.getInstance().history.size();
		 * i++) { if (DrawingManager.getInstance().history.get(i).type == TEXT &&
		 * DrawingManager.getInstance().history.get(i).textCounter == app.textCounter) { InputMethodManager
		 * imm = (InputMethodManager) getContext()
		 * .getSystemService(Context.INPUT_METHOD_SERVICE);
		 * imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,
		 * InputMethodManager.HIDE_IMPLICIT_ONLY); } } }
		 */
		invalidate();
	}

	/**
	 * Get the current position and scale of the selected image. Called whenever
	 * a drag starts or is reset.
	 */
	public void getPositionAndScale(MultiTouchEntity img,
			PositionAndScale objPosAndScaleOut) {
		// FIXME affine-izem (and fix the fact that the anisotropic_scale part
		// requires averaging the two scale factors)
		objPosAndScaleOut.set(img.getCenterX(), img.getCenterY(),
				(mUIMode & UI_MODE_ANISOTROPIC_SCALE) == 0,
				(img.getScaleX() + img.getScaleY()) / 2,
				(mUIMode & UI_MODE_ANISOTROPIC_SCALE) != 0, img.getScaleX(),
				img.getScaleY(), (mUIMode & UI_MODE_ROTATE) != 0,
				img.getAngle());
	}

	/** Set the position and scale of the dragged/stretched image. */
	public boolean setPositionAndScale(MultiTouchEntity img,
			PositionAndScale newImgPosAndScale, PointInfo touchPoint) {
		currTouchPoint.set(touchPoint);
		boolean ok = ((ImageEntity) img).setPos(newImgPosAndScale);
		if (ok)
			invalidate();
		return ok;
	}

	public boolean pointInObjectGrabArea(PointInfo pt, MultiTouchEntity img) {
		return false;
	}




	//TODO: Custom brush
	//http://stackoverflow.com/questions/8428874/how-to-make-custom-brush-for-canvas-in-android
	/*
	    private Bitmap mBitmapBrush;
    private Vector2 mBitmapBrushDimensions;

    private List<Vector2> mPositions = new ArrayList<Vector2>(100);

    private static final class Vector2 {
        public Vector2(float x, float y) {
            this.x = x;
            this.y = y;
        }

        public final float x;
        public final float y;
    }

    public CanvasBrushDrawing(Context context) {
        super(context);

// load your brush here
        mBitmapBrush = BitmapFactory.decodeResource(context.getResources(), R.drawable.splatter_brush);
        mBitmapBrushDimensions = new Vector2(mBitmapBrush.getWidth(), mBitmapBrush.getHeight());

        setBackgroundColor(0xffffffff);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for (Vector2 pos : mPositions) {
            canvas.drawBitmap(mBitmapBrush, pos.x, pos.y, null);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int action = event.getAction();
        switch (action) {
        case MotionEvent.ACTION_MOVE:
            final float posX = event.getX();
            final float posY = event.getY();
            mPositions.add(new Vector2(posX - mBitmapBrushDimensions.x / 2, posY - mBitmapBrushDimensions.y / 2));
            invalidate();
        }

        return true;
    }
	 */

    /*
    private void flatten() {
        DrawModel dr = new DrawModel();
        dr.type = DrawingEventType.BITMAP_STATIC;
        dr.color = DrawingManager.getInstance().paintColor;
        dr.bitmap = canvasBitmap.copy(Bitmap.Config.RGB_565, true);

        //mDrawCanvas.drawColor(Color.WHITE);
        //DrawingManager.getInstance().history.subList(0, i).clear();

        for (int i = 0; i < DrawingManager.getInstance().history.size(); i++) {
            if (DrawingManager.getInstance().history.get(i).type != DrawingEventType.BITMAP_STATIC) {
                DrawingManager.getInstance().history.remove(i);
            }
        }
        DrawingManager.getInstance().history.add(dr);
        invalidate();
    }
    */

	/*public void flatten() {
        setDrawingCacheEnabled(true);
		Bitmap canvasBitmapCopy = this.getDrawingCache();

        DrawingManager.getInstance().history.clear();
        app.printBitmap = canvasBitmapCopy;
        app.bitmap = canvasBitmapCopy;

		DrawModel dr = new DrawModel();
		dr.type = DrawingEventType.BITMAP_STATIC;
		dr.color = DrawingManager.getInstance().paintColor;
		dr.bitmap = canvasBitmapCopy;



		DrawingManager.getInstance().history.add(dr);
		invalidate();
	}*/

	/*
		public void setErase(boolean isErase) {
		erase = isErase;
		if (erase)
			mDrawPaint
					.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
		else
			mDrawPaint.setXfermode(null);
	}
	 */
}
