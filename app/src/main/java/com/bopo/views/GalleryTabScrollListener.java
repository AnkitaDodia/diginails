package com.bopo.views;

import com.bopo.fragments.FragmentBaseDesigns;
import com.bopo.fragments.FragmentGalleryDesigns;

/**
 * Created by Nick Yaro on 22/4/2016.
 */
public class GalleryTabScrollListener extends InfiniteScrollListener {
    FragmentBaseDesigns mFragmentGalleryDesigns;

    public GalleryTabScrollListener(FragmentBaseDesigns fragmentGalleryDesigns) {
        super(24);
        mFragmentGalleryDesigns = fragmentGalleryDesigns;
    }

    @Override
    public boolean onLoadMore(int page, int totalItemsCount) {
        mFragmentGalleryDesigns.loadData(page - 1);
        mFragmentGalleryDesigns.refreshListView();
        return true;
    }
}
