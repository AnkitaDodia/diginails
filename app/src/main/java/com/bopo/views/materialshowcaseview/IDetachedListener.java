package com.bopo.views.materialshowcaseview;


public interface IDetachedListener {
    void onShowcaseDetached(MaterialShowcaseView showcaseView, boolean wasDismissed);
}
