package com.bopo.views;

import android.view.MotionEvent;
import android.view.View;

import com.bopo.utils.AnimationUtils;

/**
 * Created by Nick Yaro on 28/3/2016.
 */
public class AnimationOnTouchListener implements View.OnTouchListener {
    public static float DEFAULT_SCALE = 0.9f;
    public static int DEFAULT_DURATION = 150;

    public float mScaleTo;
    public int mDuration;
    public boolean mPressedState;

    public AnimationOnTouchListener() {
        this(DEFAULT_SCALE, DEFAULT_DURATION);
    }

    public AnimationOnTouchListener(float scaleTo) {
        this(scaleTo, DEFAULT_DURATION);
    }

    public AnimationOnTouchListener(float scaleTo, int duration) {
        mScaleTo = scaleTo;
        mDuration = duration;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            AnimationUtils.scaleView(v, 1f, mScaleTo, mDuration);
            mPressedState = true;
        } else if (mPressedState && (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)) {
            AnimationUtils.scaleView(v, mScaleTo, 1f, mDuration);
            mPressedState = false;
        }
        return false;
    }
}
